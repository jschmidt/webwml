#use wml::debian::template title="Дарения за проекта Дебиан" MAINPAGE="true"
#use wml::debian::translation-check translation="77730b1058b58e76cb29910f6acf353ec5bf9847"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Дебиан Франция</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Дебиан</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Благодарим на всички дарители на Дебиан за помощта с оборудване и услуги: <a href="https://db.debian.org/machines.cgi">спонсори на хостинг и хардуер</a>, <a href="mirror/sponsors">спонсори на огледални сървъри</a>, <a href="partners/">партньори в разработки и услуги</a>.<p>
</aside>

<p>
Даренията се управляват от <a href="$(HOME)/devel/leader">Лидера на проекта</a> (DPL) и позволяват на Дебиан да използва <a href="https://db.debian.org/machines.cgi">сървъри</a>, <a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">друг хардуер</a>, домейни, сертификати за SSL, да подпомага <a href="https://www.debconf.org">годишната конференция на Дебиан</a>, <a href="https://wiki.debian.org/MiniDebConf">мини-конференции</a>, <a href="https://wiki.debian.org/Sprints">събирания на разработчици</a>, да присъства на събития и други.
</p>

<p id="default">
Различни <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">организации</a> управляват активи от името на Дебиан и получават дарения за Дебиан. Тук ще намерите информация за различните начини изпращане на дарения към проекта Дебиан. Най-лесният начин да направите дарение е чрез PayPal до <a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>, организация с идеална цел, която държи активи на проекта Дебиан.
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">Дарение чрез PayPal</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>Организация</th>
<th>Начини за даряване</th>
<th>Забележки</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a> (регулярни дарения),
 <a href="#spi-cheque">чек</a>,
 <a href="#spi-other">други</a>
</td>
<td>САЩ, организация с идеална цел, позволява приспадане на данъци</td>
</tr>
<tr>
<td><a href="#debianfrance">Дебиан Франция</a></td>
<td>
 <a href="#debianfrance-bank">банков превод</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>Франция, организация с идеална цел, позволява приспадане на данъци</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">банков превод</a>,
 <a href="#debianch-other">други</a>
</td>
<td>Швейцария, организация с идеална цел</td>
</tr>
<tr>
<td><a href="#debian">Дебиан</a></td>
<td>
 <a href="#debian-equipment">оборудване</a>,
 <a href="#debian-time">време</a>,
 <a href="#debian-other">други</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
е организация с идеална цел, базирана в Съединените американски щати. Основана
е през 1997 от членове на проекта Дебиан в помощ на организациите, занимаващи
се със свободен софтуер/хардуер.
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
През <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">страницата на SPI</a> в PayPal можете да правите единични или периодични дарения. За периодично дарение поставете отметка в полето, <em>Make thhis a monthly donation</em>.
</p>

<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
През <a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">страницата на SPI</a> на сайта на Click &amp; Pledge можете да правите единични или периодични дарения. За да направите периодично дарение изберете колко често искате да дарявате отдясно (<em>Repeat payment</em>), превъртете надолу до <em>Debian Project Donation</em>, въведете желаната сума, натиснете <em>Add to cart</em> и следвайте инструкциите.
</p>

<h3 id="spi-cheque">Чек</h3>

<p>
Възможни са дарения с чек или паричен превод в <abbr title="американски долари">USD</abbr> и <abbr title="канадски долари">CAD</abbr>. Запишете „Debian“ в полето за забележки и изпратете до SPI на адреса указан на <a href="https://www.spi-inc.org/donations/">страницата за дарения на SPI</a>.
</p>

<h3 id="spi-other">Други</h3>

<p>
Възможни са и дарения чрез банков превод и други начини. В някои части на света може да е по-лесно да се направи дарение до някоя от партньорските организации на Software in the Public Interest. Посетете <a href="https://www.spi-inc.org/donations/">страницата за дарения на SPI</a> за повече информация.
</p>

<h2 id="debianfrance">Дебиан Франция</h2>

<p>
Асоциацията <a href="https://france.debian.net/">Дебиан Франция</a> е организация, регистрирана във Франция според <q>закона от 1901</q>, с цел подпомагане на проекта Дебиан във Франция.
</p>

<h3 id="debianfrance-bank">Банков превод</h3>

<p>
Информацията за банковите сметки е достъпна на
<a href="https://france.debian.net/soutenir/#compte">страницата за дарения на Debian France</a>. За издаване на фактура изпратете поща до
<a href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h3 id="debianfrance-paypal">PayPal</h3>

<p>
Дарения могат да бъдат изпращани чрез <a href="https://france.debian.net/galette/plugins/paypal/form">страницата на Debian France в PayPal</a>. Даренията могат да бъдат направлявани конкретно към Debian France или към проекта Дебиан.
</p>

<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">Debian.ch</a> е основана за да представлява проекта Дебиан в Швейцария и Лихтенщайн.
</p>

<h3 id="debianch-bank">Банков превод</h3>

<p>
Информацията за банковите сметки за дарения чрез банков превод от швейцарски и международни банки е достъпна на <a href="https://debian.ch/">сайта на debian.ch</a>.
</p>

<h3 id="debianch-other">Други</h3>

<p>
За информация за други начини да направите дарение се обърнете към адреса за дарения на <a href="https://debian.ch/">сайта на debian.ch</a>.
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Дебиан</h2>

<p>
Дебиан може да приема дарения на <a href="#debian-equipment">оборудване</a>, но не и <a href="#debian-other">други</a> дарения.
</p>

<h3 id="debian-equipment">Оборудване и услуги</h3>

<p>
Дебиан разчита на даренията на оборудване и услуги от частни лица, организации, университети и т.н. за да поддържа връзките си със света.
</p>

<p>
Ако вашата организация има неизползвани сървъри или отделни компоненти (твърди дискове, контролери за SCSI, мрежови карти и т.н.), моля, помислете за дарение към Дебиан. За повече информация се свържете с <a href="mailto:hardware-donations@debian.org">делегата за даренията на оборудване</a>.
</p>

<p>
Дебиан поддържа <a href="https://wiki.debian.org/Hardware/Wanted">списък с желано оборудване</a> за различни услуги и групи в рамките на проекта.
</p>

<h3 id="debian-time">Време</h3>

<p>
Можете да <a href="$(HOME)/intro/help">помогнете на Дебиан</a> като отделите време за работа по проекта.
</p>

<h3 id="debian-other">Други</h3>

<p>
В момента Дебиан не може да приема дарения в електронни валути, но варианти се търсят.
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>
