#use wml::debian::translation-check translation="1ee140ecea4124e2849e2bc5c2de4b63b838e5c5" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder blev opdaget i cURL, et URL-overførselsbibliotek:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8169">CVE-2020-8169</a>

    <p>Marek Szlagor rapporterede at libcurl kunne narres til at foranstille en 
    del af adgangskoden til værtsnavnet før det blev slået op, potentielt 
    førende til en lækage af den delvise adgangskode over netværket og til 
    DNS-serveren/-serverne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8177">CVE-2020-8177</a>

    <p>sn rapporterede at curl kunne narres af en ondsindet server til at 
    overskrive en lokal fil ved hjælp af valgmulighederne -J 
    (--remote-header-name) og -i (--include) på den samme 
    kommandolinje.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8231">CVE-2020-8231</a>

    <p>Marc Aldorasi rapporterede at libcurl kunne anvende den forkerte 
    forbindelse når en applikation, som anvender libcurls multi-API, opsætter 
    valgmuligheden CURLOPT_CONNECT_ONLY, hvilke kunne føre til 
    informationslækager.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>

    <p>Varnavas Papaioannou rapporterede at en ondsindet server kunne anvende 
    PASV-svaret til at narre curl til for at forbinde sig tilbage til en 
    vilkårlig IP-adresse og port, potentielt førende til at curl udtrak 
    oplysninger om tjenester, som ellers er private og ikke 
    offentliggjorte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8285">CVE-2020-8285</a>

    <p>xnynx rapporterede at libcurl kunne løbe tør for plads på stakken, når 
    der funktionaliteten til FTP-wildcardmatching blev anvendt 
    (CURLOPT_CHUNK_BGN_FUNCTION).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8286">CVE-2020-8286</a>

    <p>Der blev rapporteret at libcurl ikke kontrollede om et OCSP-svar faktisk 
    svarede til certifikatet, som det var hensigten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22876">CVE-2021-22876</a>

    <p>Viktor Szakats rapporterede at libcurl ikke fjernede brugerens 
    loginoplysninger fra URL'en, når det automatisk udfyldte Referer
    HTTP-forespørgselsheaderfeltet i udgående HTTP-forespørgsler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22890">CVE-2021-22890</a>

    <p>Mingtao Yang rapporterede at ved anvendelse af en HTTPS-proxy og TLS 1.3, 
    kunne libcurl forveksle sessiontickets ankommende fra HTTPS-proxy'en som var 
    de i stedet ankommet fra den fjerne server.  Dermed kunne en HTTPS-proxy 
    være i stand til at narre libcurl til at anvende den forkerte sessionticket 
    for værten, og dermed omgå server-TLS-certifikatkontrollen.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 7.64.0-4+deb10u2.</p>

<p>Vi anbefaler at du opgraderer dine curl-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende curl, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4881.data"
