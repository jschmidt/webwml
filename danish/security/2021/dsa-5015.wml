#use wml::debian::translation-check translation="65a9c27818db1c1e093dd5651ca3c518685e3224" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Andrew Bartlett opdagede at Samba, SMB/CIFS-fil-, print- og loginserver til 
Unix, kunne omsætte domænebrugere til lokale brugere på en uønsket måde.  Dermed 
kunne en bruger i et AD-domæne potentielt få rootadgang på domænemedlemmer.</p>

<p>Et nyt parameter <q>min domain uid</q> (standardværdi 1000), er tilføjet for 
at angive den minimale uid, som er tilladt ved omsætning af en lokal konto til 
en domænekonto.</p>

<p>Yderligere oplysninger og omgåelser, finder man i opstrøms bulletin
<a href="https://www.samba.org/samba/security/CVE-2020-25717.html">
https://www.samba.org/samba/security/CVE-2020-25717.html</a></p>

<p>I den gamle stabile distribution (buster), er dette problem rettet
i version 2:4.9.5+dfsg-5+deb10u2.  Desuden afhjælper denne opdatering
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25722">\
CVE-2020-25722</a>.  Desværre er de krævede ændringer til at rette 
yderligere CVE'er, der påvirker Samba som en AD-kompatibel domaincontroller, for 
omfattende til at kunne tilbageføres.  Folk der anvender Samba som en 
AD-kompatibel domaincontroller, opfordres til at migrere til Debian bullseye.  
Gældende fra dette tidspunkt, er opsætninger med en AD-domaincontroller ikke 
længere understøttet i den gamle stabile udgave af Debian.</p>

<p>Vi anbefaler at du opgraderer dine samba-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende samba, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5015.data"
