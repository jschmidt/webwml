#use wml::debian::translation-check translation="9c44c7905cd7bbe6df67ff227e8eaec739c984bb" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>

<p>Man opdagede at rettelsen til løsning af 
<a href="https://security-tracker.debian.org/tracker/CVE-2021-44228">\
CVE-2021-44228</a> i Apache Log4j, et Logging Framework for Java, var 
ufuldstændig i visse ikke-standard-opsætninger.  Dermed kunne angribere med 
kontrol over Thread Context Map-inddata (MDC), når logningsopsætningen 
anvender et ikke-standard-Pattern Layout med enten en Context Lookup (for 
eksempel, $${ctx:loginId}) eller et Thread Context Map-mønster (%X, %mdc, eller 
%MDC) til at fabrikere ondsindede inddata ved hjælp af et JNDI Lookup-mønster, 
medførende et lammelsesangreb (DOS).</p>

<p>I den gamle stabile distribution (buster), er dette problem rettet
i version 2.16.0-1~deb10u1.</p>

<p>I den stabile distribution (bullseye), er dette problem rettet i
version 2.16.0-1~deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine apache-log4j2-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende apache-log4j2, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5022.data"
