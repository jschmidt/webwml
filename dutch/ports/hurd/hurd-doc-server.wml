#use wml::debian::template title="Debian GNU/Hurd --- Documentatie" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="f4f6753d2f1e1d5bb9708ce8b3f7dde77940b870"

<h1>Debian GNU/Hurd</h1>

<p>Dirk Ritter stuurde me de volgende tekst, die de uitvoer bevat van de optie <tt>--help</tt> van elk Hurd serverprogramma. Dit kan een goed startpunt zijn voor verdere documentatie over de serverprogramma's, vooral die welke nuttig zijn voor de gebruiker, zoals ext2fs, ufs, isofs, ftpfs, crash, enz.
</p>

<p>
Sindsdien werd de tekst bijgewerkt met e-mailfragmenten uit de Hurd mailinglijsten. Met dank aan Martin von Loewis

<h2>Voorlopige beschrijving van de GNU/Hurd gebruikersinterface</h2>

<p>
Momenteel is er bijna niets, maar dit is misschien beter dan helemaal niets, dus stuur klachten, correcties en aanvullingen naar
<a href="mailto:dirk@gnumatic.s.bawue.de">Dirk Ritter</a>,
&lt;dirk@gnumatic.s.bawue.de&gt;. Houd er rekening mee dat mijn programmeervaardigheden <em>zeer</em> beperkt zijn. Dus u mag niet van mij verwachten dat ik begrip heb van datgene wat te maken heeft met het ontwerp en de implementatie van besturingssystemen.</p>

<table border="2"
       summary="Index of HURD servers and translators:">

<caption><em>Index van HURD-servers en vertalers (translators):</em></caption>

<tr>
  <th><a href="#auth" name="TOC_auth" type="text/html">
      De authenticatieserver</a></th>
  <th>&quot;<code>auth</code>&quot;</th>
</tr>
<tr>
  <th><a href="#crash" name="TOC_crash" type="text/html">
      De crashserver</a></th>
  <th>&quot;<code>crash</code>&quot;</th>
</tr>
<tr>
  <th><a href="#exec" name="TOC_exec" type="text/html">
      De uitvoeringsserver</a></th>
  <th>&quot;<code>exec</code>&quot;</th>
</tr>
<tr>
  <th><a href="#ext2fs" name="TOC_ext2fs" type="text/html">
      De ext2fs-server</a></th>
  <th>&quot;<code>ext2fs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#fifo" name="TOC_fifo" type="text/html">
      De fifo-vertaler</a></th>
  <th>&quot;<code>fifo</code>&quot;</th>
</tr>
<tr>
  <th><a href="#firmlink" name="TOC_firmlink" type="text/html">
      De firmlink-vertaler</a></th>
  <th>&quot;<code>firmlink</code>&quot;</th>
</tr>
<tr>
  <th><a href="#ftpfs" name="TOC_ftpfs" type="text/html">
      De ftp-bestandssysteemvertaler</a></th>
  <th>&quot;<code>ftpfs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#fwd" name="TOC_fwd" type="text/html">
      De fwd-server</a></th>
  <th>&quot;<code>fwd</code>&quot;</th>
</tr>
<tr>
  <th><a href="#hostmux" name="TOC_hostmux" type="text/html">
      De hostmux-server</a></th>
  <th>&quot;<code>hostmux</code>&quot;</th>
</tr>
<tr>
  <th><a href="#ifsock" name="TOC_ifsock" type="text/html">
      De ifsock-server</a></th>
  <th>&quot;<code>ifsock</code>&quot;</th>
</tr>
<tr>
  <th><a href="#init" name="TOC_init" type="text/html">
      De init-server</a></th>
  <th>&quot;<code>init</code>&quot;</th>
</tr>
<tr>
  <th><a href="#isofs" name="TOC_isofs" type="text/html">
      De iso-bestandssysteemserver</a></th>
  <th>&quot;<code>isofs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#magic" name="TOC_magic" type="text/html">
      De magic-server</a></th>
  <th>&quot;<code>magic</code>&quot;</th>
</tr>
<tr>
  <th><a href="#new-fifo" name="TOC_new-fifo" type="text/html">
      De nieuwe fifo-server</a></th>
  <th>&quot;<code>new-fifo</code>&quot;</th>
</tr>
<tr>
  <th><a href="#nfs" name="TOC_nfs" type="text/html">
      De nfs-server</a></th>
  <th>&quot;<code>nfs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#null" name="TOC_null" type="text/html">
      De null-server</a></th>
  <th>&quot;<code>null</code>&quot;</th>
</tr>
<tr>
  <th><a href="#pfinet" name="TOC_pfinet" type="text/html">
      De pfinet-server</a></th>
  <th>&quot;<code>pfinet</code>&quot;</th>
</tr>
<tr>
  <th><a href="#pflocal" name="TOC_pflocal" type="text/html">
      De pflocal-server</a></th>
  <th>&quot;<code>pflocal</code>&quot;</th>
</tr>
<tr>
  <th><a href="#proc" name="TOC_proc" type="text/html">
      De processerver</a></th>
  <th>&quot;<code>proc</code>&quot;</th>
</tr>
<tr>
  <th><a href="#storeio" name="TOC_storeio" type="text/html">
      De opslagvertaler</a></th>
  <th>&quot;<code>storeio</code>&quot;</th>
</tr>
<tr>
  <th><a href="#symlink" name="TOC_symlink" type="text/html">
      De symbolische koppelingvertaler</a></th>
  <th>&quot;<code>symlink</code>&quot;</th>
</tr>
<tr>
  <th><a href="#term" name="TOC_term" type="text/html">
      De terminalserver</a></th>
  <th>&quot;<code>term</code>&quot;</th>
</tr>
<tr>
  <th><a href="#ufs" name="TOC_ufs" type="text/html">
      De ufs-server</a></th>
  <th>&quot;<code>ufs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#usermux" name="TOC_usermux" type="text/html">
      De usermux-server</a></th>
  <th>&quot;<code>usermux</code>&quot;</th>
</tr>

# Uncomment and fill the blanks...
#<tr>
#  <th><a href="#" name="TOC_" type="text/html">
#      De server</a></th>
#  <th>&quot;<code></code>&quot;</th>
#<tr>
</table>


<h2 class="center"><a href="#TOC_auth" name="auth" type="text/html">
De authenticatieserver - &quot;<code>auth</code>&quot;</a></h2>

<p>
Geeft referenties door wanneer twee servers die elkaar niet vertrouwen met elkaar communiceren. In zekere zin stelt elke authenticatieserver een vertrouwensdomein in. (Martin von Loewis, 10 okt 1996)</p>

<P>
Een van de interessante eigenschappen is dat het mogelijk is voor processen om zich gelijktijdig voor te doen als meerdere identiteiten en ook om dynamisch identiteiten te verkrijgen of op te geven.</p>

<p>
Het uitvoeren van &quot;<code>/hurd/auth --help</code>&quot; geeft:
<br>
<pre>
Gebruik: auth [OPTIE...]

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Bugs melden aan bug-hurd@gnu.org.
</pre>


<h2 class="center"><a href="#TOC_crash" name="crash" type="text/html">
De crashserver - &quot;<code>crash</code>&quot;</a></h2>

<p>
De crashserver wordt actief wanneer een taak een fataal foutsignaal krijgt, bijvoorbeeld omdat de geheugengrenzen worden overschreden (segmentatiefout). De crashserver heeft drie werkwijzen: de procesgroep (pgrp) van de aanstoot gevende taak opschorten of vernietigen of een core-bestand dumpen. Dit laatste is nog niet geïmplementeerd. Bekijk de crashserver als een airbag.</p>
<p>
U kunt de werkingsmodus natuurlijk instellen met <code>settrans</code>, maar ook met <code>fsysopts</code> tijdens runtime. Dit geldt voor de standaard voor het hele systeem en vereist meestal systeembeheerdersrechten. Een gebruiker kan een andere standaard selecteren met de omgevingsvariabele <code>CRASHSERVER</code>. U stelt deze variabele in op een inode waaraan de <code>crash</code>-server is gekoppeld. Op een standaard Debian GNU/Linux-systeem hebben deze drie standaardwerkwijzen overeenkomstige vertalers in <code>/servers/crash-*</code>.</p>

<pre>

 Deze opties specificeren de regeling voor een gecrasht proces:
  -s, --suspend              Het proces opschorten
  -k, --kill                 Het proces vernietigen
  -c, --core-file            Een core-bestand dumpen
      --dump-core
</pre>


<h2 class="center"><a href="#TOC_exec" name="exec" type="text/html">
De uitvoeringsserver - &quot;<code>exec</code>&quot;</a></h2>

<p>
Exec beheert de creatie van een nieuw procesimage vanuit het imagebestand.</p>

<P>
Eigenlijk biedt deze server ondersteuning om een uitvoerbaar procesimage te maken uit elk imagebestand dat wordt herkend door de BFD-bibliotheek (dit omvat a.out, ELF en vele andere). Met gzip gecomprimeerde uitvoerbare bestanden worden ook herkend (handig voor opstartdiskettes).</p>

<p>
Het uitvoeren van &quot;<code>/hurd/exec --help</code>&quot; geeft:
<br>
<pre>
Gebruik: exec [OPTIE...]
Standaard uitvoeringsserver van de Hurd

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_ext2fs" name="ext2fs" type="text/html">
De ext2-bestandssysteemserver - &quot;<code>ext2fs</code>&quot;</a></h2>

<p>
Deze server beheert bestandssystemen van het type ext2.
Deze doet hetzelfde als <code>ext2fs.static</code>, alleen is <code>ext2fs.static</code> een statisch gekoppeld uitvoerbaar bestand.

<p>
Het uitvoeren van &quot;<code>/hurd/ext2fs --help</code>&quot; geeft:
<br>
<pre>
Gebruik: ext2fs [OPTIE...] APPARAAT...

Als noch --interleave noch --layer is opgegeven, worden meerdere APPARATEN samengevoegd.

  -E, --no-exec              Geen uitvoering van bestanden toestaan op dit
                             bestandssysteem
  -I, --interleave=BLOKKEN   Interleave van reeksen met een lengte van BLOKKEN
  -L, --layer                Meerdere apparaten stapelen voor redundantie
  -n, --no-sync              Gegevens niet automatisch naar schijf
                             synchroniseren
  -r, --readonly             Nooit naar schijf schrijven of openen voor
                             schrijven toestaan
  -s, --sync[=INTERVAL]      Als INTERVAL werd opgegeven, alle gegevens die
                             niet daadwerkelijk naar schijf zijn geschreven,
                             elke INTERVAL seconden synchroniseren, anders in
                             synchrone modus werken (de standaardinstelling is
                             om elke 30 seconden te synchroniseren)
  -S, --no-suid              Geen set-uid of set-gid uitvoering toestaan
  -T, --store-type=TYPE      Elk APPARAAT benoemt een opslag van het type TYPE
  -w, --writable             Normaal lees-/schrijfgedrag gebruiken

 Opstartopties:
      --bootflags=VLAGGEN
  -C, --directory=MAP
      --device-master-port=POORT
      --exec-server-task=POORT
      --host-priv-port=POORT

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Als noch --interleave noch --layer is opgegeven, worden meerdere APPARATEN samengevoegd.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_fifo" name="fifo" type="text/html">
De fifo-server - &quot;<code>fifo</code>&quot;</a></h2>

<p>
De fifo-vertaler implementeert benoemde pijpen.

<p>
Het uitvoeren van &quot;<code>/hurd/fifo --help</code>&quot; geeft:
<br>
<pre>
Gebruik: fifo [OPTIE...]
Vertaler voor fifo's

  -d, --dgram                Leesbewerkingen reflecteren structuurgrenzen van
                             schrijfbewerking
  -m, --multiple-readers     Meerdere gelijktijdige lezers toestaan
  -n, --noblock              Niet blokkeren bij openen
  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_firmlink" name="firmlink" type="text/html">
De firmlink-server - &quot;<code>firmlink</code>&quot;</a></h2>

<p>
Een vertaler voor firmlinks (hechte koppelingen).

<p>
Het uitvoeren van &quot;<code>/hurd/firmlink --help</code>&quot; geeft:
<br>
<pre>
Gebruik: firmlink [OPTIE...] DOEL
Een vertaler voor firmlinks (hechte koppelingen)

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Een hechte koppeling houdt het midden tussen een symbolische koppeling en een harde koppeling;

Net als een symbolische koppeling is deze 'bij naam' en bevat deze geen echte verwijzing naar het doel. De opzoeking levert echter een node op die bovenliggende opzoekingen omleidt, zodat pogingen om de cwd te vinden die via de koppeling gaat, de naam van de koppeling weergeeft en niet de naam van het doel. Het doel waarnaar de hechte koppeling verwijst, wordt opgezocht in de naamruimte van de vertaler, niet in die van de client.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_ftpfs" name="ftpfs" type="text/html">
De ftp-bestandssysteemserver - &quot;<code>ftpfs</code>&quot;</a></h2>

<p>
Een server voor ftp-bestandssystemen.

<p>
Het uitvoeren van &quot;<code>/hurd/ftpfs --help</code>&quot; geeft:
<br>
<pre>
Gebruik: ftpfs [OPTIE...] EXTERN_BS [SERVER]
Hurd ftp-bestandssysteemvertaler

  -D, --debug[=BESTAND]         Debug-uitvoer afdrukken naar BESTAND

 Parameters:
      --bulk-stat-period=SECS   Periode voor het detecteren van
                                bulkstatistieken (standaard 10)
      --bulk-stat-threshold=SECS  Aantal statistieken binnen de bulk-stat-period
                                dat een bulk-statistiek activeert (standaard 5)
      --name-timeout=SECS    Tijd dat mapnamen in de cache worden opgeslagen
                             (standaard 300)
      --node-cache-size=AANTAL  Aantal recent gebruikte bestandssysteemnodes
                                dat in de cache wordt opgeslagen (standaard 50)
      --stat-timeout=SECS    Tijd dat statusinformatie in de cache wordt
                             opgeslagen (standaard 120)

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Als SERVER niet is opgegeven, wordt geprobeerd deze uit EXTERN_BS te halen, waarbij de notatie `SERVER:BS' wordt gebruikt. SERVER kan een computernaam zijn, in welk geval anonieme ftp wordt gebruikt, of kan een gebruiker en wachtwoord bevatten zoals `GEBRUIKER:WACHTWOORD@COMPUTER' (het `:WACHTWOORD'-gedeelte is optioneel).

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_fwd" name="fwd" type="text/html">
De fwd-server - &quot;<code>fwd</code>&quot;</a></h2>

<p>
Wanneer deze benaderd wordt, stuurt de fwd-vertaler verzoeken door naar een andere server. Dit wordt gebruikt in de fifo- en de symbolische koppelingserver. De gedachte is om geen miljoen servers te hebben voor dergelijke triviale dingen; fwd wordt gebruikt om te coördineren dat één server meerdere verschillende nodes op passende wijze afhandelt.

<p>
Het uitvoeren van &quot;<code>/hurd/fwd --help</code>&quot; geeft:
<br>
<pre>
Gebruik: /hurd/fwd SERVER [VERTALER_NAAM [VERTALER_ARG...]]
</pre>



<h2 class="center"><a href="#TOC_hostmux" name="hostmux" type="text/html">
De hostmux-server - &quot;<code>hostmux</code>&quot;</a></h2>

<p>
Ik heb geen idee waar deze server goed voor is.

<small>
(Een server om computers op te zoeken?)
</small>

<p>
Het uitvoeren van &quot;<code>/hurd/hostmux --help</code>&quot; geeft:
<br>
<pre>
Gebruik: hostmux [OPTIE...] VERTALER [ARG...]
Een vertaler voor het aanroepen van computerspecifieke vertalers

  -H, --host-pattern=PAT     De tekenreeks die in de vertalerspecificatie moet
                             worden vervangen door de computernaam; als deze
                             leeg is of niet voorkomt, wordt de computernaam
                             toegevoegd als extra argument.
                             (standaard `${host}')
  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Deze vertaler verschijnt als een map waarin computernamen kunnen worden opgezocht en zal VERTALER starten om elke resulterende node van dienst te zijn.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_ifsock" name="ifsock" type="text/html">
De ifsock-server - &quot;<code>ifsock</code>&quot;</a></h2>

<p>
<code>ifsock</code> behandelt alleen <code>S_IFSOCK</code> bestandssysteemnodes voor bestandssystemen die dit niet zelf doen, en fungeert als een haak waaraan Unix-domeinsocketadressen kunnen worden opgehangen. pfinet en pflocal implementeren de socket-API. (Thomas Bushnell, 10 okt 1996)

<p>
Het uitvoeren van &quot;<code>/hurd/ifsock --help</code>&quot; eeft:
<br>
<pre>
/hurd/ifsock: Moet als een vertaler gestart worden
</pre>



<h2 class="center"><a href="#TOC_init" name="init" type="text/html">
De initialisatieserver - &quot;<code>init</code>&quot;</a></h2>

<p>
Een server voor systeemopstartprocedures en basisruntimeconfiguraties.

<p>
Het uitvoeren van &quot;<code>/hurd/init --help</code>&quot; geeft:
<br>
<pre>
Gebruik: init [OPTIE...]
De hurd kernservers en de functioneringstoestand van het systeem starten en onderhouden

  -d, --debug
  -f, --fake-boot            Deze hurd is niet opgestart op de ruwe machine
  -n, --init-name
  -q, --query                Naar de naam van de te starten servers vragen
  -s, --single-user          Systeem opstarten in modus één gebruiker
  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_isofs" name="isofs" type="text/html">
De iso-bestandssysteemserver - &quot;<code>isofs</code>&quot;</a></h2>

<p>
Een server voor bestandssystemen van het type iso, vaak gebruikt op compact disks.

<p>
Het uitvoeren van &quot;<code>/hurd/isofs --help</code>&quot; geeft:
<br>
<pre>
Gebruik: isofs [OPTIE...] APPARAAT...

Als noch --interleave noch --layer is opgegeven, worden meerdere APPARATEN samengevoegd.

  -E, --no-exec              Geen uitvoering van bestanden toestaan op dit
                             bestandssysteem
  -I, --interleave=BLOKKEN   Interleave van reeksen met een lengte van BLOKKEN
  -L, --layer                Meerdere apparaten stapelen voor redundantie
  -n, --no-sync              Gegevens niet automatisch naar schijf
                             synchroniseren
  -r, --readonly             Nooit naar schijf schrijven of openen voor
                             schrijven toestaan
  -s, --sync[=INTERVAL]      Als INTERVAL werd opgegeven, alle gegevens die
                             niet daadwerkelijk naar schijf zijn geschreven,
                             elke INTERVAL seconden synchroniseren, anders in
                             synchrone modus werken (de standaardinstelling is
                             om elke 30 seconden te synchroniseren)
  -S, --no-suid              Geen set-uid of set-gid uitvoering toestaan
  -T, --store-type=TYPE      Elk APPARAAT benoemt een opslag van het type TYPE
  -w, --writable             Normaal lees-/schrijfgedrag gebruiken

 Opstartopties:
      --bootflags=VLAGGEN
  -C, --directory=MAP
      --device-master-port=POORT
      --exec-server-task=POORT
      --host-priv-port=POORT

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Als noch --interleave noch --layer is opgegeven, worden meerdere APPARATEN samengevoegd.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_magic" name="magic" type="text/html">
De magic-server - &quot;<code>magic</code>&quot;</a></h2>

<p>
Een vertaler die het magische resultaat van opnieuw proberen <code>MAGIC</code> oplevert.

Normale eindgebruikers hoeven er waarschijnlijk niet veel vanaf te weten omdat het bijvoorbeeld gebruikt wordt om terminal-I/O te vergemakkelijken. Programmeurs kunnen hun voordeel doen met de volgende informatie die Thomas Bushnell gaf:

<blockquote>
<p>
De sleutel hier is om te weten wat een &quot;magisch resultaat van opnieuw proberen&quot; is. Wat u moet doen is kijken naar de <code>dir_lookup</code> <abbr>RPC</abbr> die is gedocumenteerd in <code>&lt;hurd/fs.defs&gt;</code> en <code>&lt;hurd/ hurd_types.defs&gt;</code>.

<br>
Magische opzoekresultaten zijn in principe voor gevallen waarin de opzoeking informatie nodig heeft die deel uitmaakt van de toestand van het aanroepende proces en geen deel uitmaakt van de normale opzoekprocedure voor namen. Deze &quot;doorgeven naar de toestand van de aanroeper&quot; dingen moeten allemaal speciaal geïmplementeerd worden in de C-bibliotheek (zie <code>libc/hurd/hurdlookup.c</code>), en moeten de bekende gevallen dekken om de functionaliteit te bieden die veel andere systemen bieden.
</p>
</blockquote>

<p>
Hij was ook zo vriendelijk om een bepaald gedrag uit te leggen waar normale eindgebruikers meestal verbaasd over zijn als ze het voor het eerst tegenkomen:

<blockquote>
<p>
Niet in staat zijn tot &quot;<kbd>ls /dev/fd</kbd>&quot; is omdat de vertaler niet weet welke bestandsindicatoren u open hebt staan, dus kan het u niet vertellen welke er zijn. Maar het gedrag ervan is precies hetzelfde als op andere systemen.
</p>
</blockquote>

<p>
Het uitvoeren van &quot;<code>/hurd/magic --help</code>&quot; geeft:
<br>
<pre>
Gebruik: magic [OPTIE...] MAGIC
Een vertaler die het magische resultaat van opnieuw proberen <code>MAGIC</code> oplevert.

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_new-fifo" name="new-fifo" type="text/html">
De nieuwe fifo-server - &quot;<code>new-fifo</code>&quot;</a></h2>

<p>
Alternatieve server voor benoemde pijpen.

<p>
Het uitvoeren van &quot;<code>/hurd/new-fifo --help</code>&quot; geeft:
<br>
<pre>
Gebruik: new-fifo [OPTIE...]

  -d, --dgram                Leesbewerkingen reflecteren structuurgrenzen van
                             schrijfbewerking
  -n, --noblock              Niet blokkeren bij openen
  -m, --multiple-readers     Meerdere gelijktijdige lezers toestaan
  -s, --server               In servermodus werken
  -S, --standalone           Niet proberen een fifo-server te gebruiken
  -U, --use-server=NAAM      Server NAAM proberen te gebruiken
  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_nfs" name="nfs" type="text/html">
De netwerkbestandssysteemserver - &quot;<code>nfs</code>&quot;</a></h2>

<p>
Netwerkbestandssysteemondersteuning voor het netwerkbestandssysteem van Sun.

<p>
Het uitvoeren van &quot;<code>/hurd/nfs --help</code>&quot; geeft:
<br>
<pre>
Gebruik: nfs [OPTIE...] EXTERN_BS [COMPUTER]
Hurd nfs-vertaler

  -h, --hard                 Bestandssysteemaanvragen opnieuw proberen tot ze
                             slagen
  -s, --soft[=AANTAL]        Bestandssysteemaanvragen zullen uiteindelijk
                             mislukken, na AANTAL pogingen indien opgegeven,
                             anders na 3
  -R, --read-size=BYTES, --rsize=BYTES
                             Max pakketgrootte voor lezen (standaard 8192)
  -W, --write-size=BYTES, --wsize=BYTES
                             Max pakketgrootte voor schrijven (standaard 8192)

 Verlooptijden:
      --cache-timeout=SEC    Verlooptijd voor bestandsgegevens in de cache
                             (standaard 3)
      --init-transmit-timeout=SEC
      --max-transmit-timeout=SEC
      --name-cache-neg-timeout=SEC
                             Verlooptijd voor negatieve mapcache-items
                             (standaard 3)
      --name-cache-timeout=SEC   Verlooptijd voor positieve mapcache-items
                             (standaard 3)
      --stat-timeout=SEC     Verlooptijd voor statusinformatie in de cache
                             (standaard 3)

 Serverspecificatie:
      --default-mount-port=POORT   Poort voor aankoppelserver, als er geen
                             automatisch kan worden gevonden
      --default-nfs-port=POORT   Poort voor nfs-bewerkingen, als er geen
                             automatisch kan worden gevonden
      --mount-port=POORT     Poort voor aankoppelserver
      --mount-program=ID[.VERS]
      --nfs-port=POORT       Poort voor nfs-bewerkingen
      --nfs-program=ID[.VERS]
      --pmap-port=SVC|POORT

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Als COMPUTER niet is opgegeven, wordt geprobeerd deze uit EXTERN_BS te halen met behulp van de notatie `COMPUTER:BS' of `BS@COMPUTER'.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_null" name="null" type="text/html">
De gootsteen - &quot;<code>null</code>&quot;</a></h2>

<p>
Een server voor veel vrije ruimte en ontelbare aantallen nullen, implementeert
<code>/dev/null</code> en <code>/dev/zero</code>.

<p>
Het uitvoeren van &quot;<code>/hurd/null --help</code>&quot; geeft:
<br>
<pre>
Gebruik: null [OPTIE...]
Eindeloze gootsteen en nulbron

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_pfinet" name="pfinet" type="text/html">
De TCP/IP-server - &quot;<code>pfinet</code>&quot;</a></h2>

<p>
Een server voor TCP/IP, die de protocolfamilie (IPv4) PF_INET implementeert. De server die de IPv6-protocolfamilie zou implementeren, zou in het huidige schema pfinet6 heten.</p>
<p>
Deze opzetten is helemaal niet moeilijk. Deze wordt altijd geplaatst in
<code>/servers/socket/2</code>, want dat is waar glibc ernaar zal zoeken.
Zorg er dus voor dat u hem op deze manier installeert:
<code>settrans /servers/socket/2 /hurd/pfinet -6 /servers/socket/26 --interface=/dev/eth0 OPTIES</code> en
<code>settrans /servers/socket/26 /hurd/pfinet -4 /servers/socket/2 --interface=/dev/eth0 OPTIES</code>,
waarbij <code>OPTIES</code>
het IP-adres, het netmasker en de gateway (indien aanwezig) specificeren. Momenteel wordt slechts één netwerkinterface ondersteund. Later kunt u met dezelfde opdracht ook andere interfaces instellen, zoals <code>eth1</code> enzovoort.</p>
<p>
Als u geen netwerkkaart heeft, moet u op zijn minst de loopback-interface instellen, zodat localhost werkt (belangrijk voor de printerwachtrij en andere nuttige dingen). Doe dit met het bovenstaande commando, maar specificeer geen interface of <code>OPTIES</code>. Een eenvoudige <code>settrans /servers/socket/1 /hurd/pfinet</code> is voldoende.</p>
<p>
Het uitvoeren van &quot;<code>/hurd/pfinet --help</code>&quot; geeft:
<br>
<pre>
Swansea University Computer Society TCP/IP voor NET3.019 IP-protocollen: ICMP, UDP, TCP
Gebruik: pfinet [OPTIE...]
Interfacespecifieke opties vóór de eerste interfacespecificatie zijn van toepassing op de eerstvolgende interface; anders zijn ze van toepassing op de eerder gespecificeerde interface.

  -i, --interface=APPARAAT   Te gebruiken netwerkinterface

 Deze gelden voor een opgegeven interface:
  -a, --address=ADRES        Het netwerkadres instellen
  -g, --gateway=ADRES        De standaardgateway instellen
  -m, --netmask=MASK         Het netmasker instellen
  -s, --shutdown             De interface uitschakelen

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_pflocal" name="pflocal" type="text/html">
De pflocal-server - &quot;<code>pflocal</code>&quot;</a></h2>

<p>
Implementeert UNIX-domeinsockets. Nodig voor bijvoorbeeld pijpen.

<p>
Het uitvoeren van &quot;<code>/hurd/pflocal --help</code>&quot; geeft:
<br>
<pre>
Gebruik: /hurd/pflocal
</pre>

<h2 class="center"><a href="#TOC_proc" name="proc" type="text/html">
De processerver - &quot;<code>proc</code>&quot;</a></h2>

<p>
De proc server wijst PID's en processtructuren toe aan taken en beheert alle zaken op procesniveau, zoals wachten, fork-bits en ondersteuning voor de C-bibliotheek.

<p>
Het uitvoeren van &quot;<code>/hurd/proc --help</code>&quot; geeft:
<br>
<pre>
Gebruik: proc [OPTIE...]
Hurd processerver

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_storeio" name="storeio" type="text/html">
De opslagvertaler - &quot;<code>storeio</code>&quot;</a></h2>

<p>
Een vertaler voor apparaten en andere opslag.

<p>
Het uitvoeren van &quot;<code>/hurd/storeio --help</code>&quot; geeft:
<br>
<pre>
Gebruik: storeio [OPTIE...] APPARAAT...
Vertaler voor apparaten en andere opslag

  -I, --interleave=BLOKKEN   Interleave van reeksen met een lengte van BLOKKEN
  -L, --layer                Meerdere apparaten stapelen voor redundantie
  -n, --rdev=ID              Het stat rdev-nummer voor deze node; kan een enkel
                             geheel getal zijn of de vorm MAJOR,MINOR hebben
  -r, --readonly             Schrijven niet toestaan
  -T, --store-type=TYPE      Elk APPARAAT benoemt een opslag van het type TYPE
  -w, --writable             Schrijven niet toestaan
  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Als noch --interleave noch --layer is opgegeven, worden meerdere APPARATEN samengevoegd.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_symlink" name="symlink" type="text/html">
De symbolische koppelingserver - &quot;<code>symlink</code>&quot;</a></h2>

<p>
Een server voor symbolische koppelingen voor bestandssystemen die dit niet zelf ondersteunen.

<p>
Het uitvoeren van &quot;<code>/hurd/symlink --help</code>&quot; geeft:
<br>
<pre>
?
</pre>
(Er was geen uitvoer? Vreemd...)


<h2 class="center"><a href="#TOC_term" name="term" type="text/html">
De terminalserver - &quot;<code>term</code>&quot;</a></h2>

<p>
Implementeert een POSIX-terminal.

<p>
Het uitvoeren van &quot;<code>/hurd/term --help</code>&quot; geeft:
<br>
<pre>
Gebruik: term ttynaam type arg
</pre>



<h2 class="center"><a href="#TOC_ufs" name="ufs" type="text/html">
De ufs-server - &quot;<code>ufs</code>&quot;</a></h2>

<p>
Een server voor bestandssystemen van het type ufs. Deze doet hetzelfde als <code>ufs.static</code>, alleen is <code>ufs.static</code> een statisch gekoppeld uitvoerbaar bestand.

<p>
Het uitvoeren van &quot;<code>/hurd/ufs --help</code>&quot; geeft:
<br>
<pre>
Gebruik: ufs [OPTIE...] APPARAAT...

Als noch --interleave noch --layer is opgegeven, worden meerdere APPARATEN samengevoegd.

  -C, --compat=FMT           FMT kan GNU, 4.4 of 4.2 zijn en bepaalt welke
                             bestandssysteemextensies naar de schijf worden
                             geschreven (standaard is GNU)
  -E, --no-exec              Geen uitvoering van bestanden toestaan op dit
                             bestandssysteem
  -I, --interleave=BLOKken   Interleave van reeksen met een lengte van BLOKKEN
  -L, --layer                Meerdere apparaten stapelen voor redundantie
  -n, --no-sync              Gegevens niet automatisch naar schijf
                             synchroniseren
  -r, --readonly             Nooit naar schijf schrijven of openen voor
                             schrijven toestaan
  -s, --sync[=INTERVAL]      Als INTERVAL werd opgegeven, alle gegevens die
                             niet daadwerkelijk naar schijf zijn geschreven,
                             elke INTERVAL seconden synchroniseren, anders in
                             synchrone modus werken (de standaardinstelling is
                             om elke 30 seconden te synchroniseren)
  -S, --no-suid              Geen set-uid of set-gid uitvoering toestaan
  -T, --store-type=TYPE      Elk APPARAAT benoemt een opslag van het type TYPE
  -w, --writable             Normaal lees-/schrijfgedrag gebruiken

 Opstartopties:
      --bootflags=VLAGGEN
      --device-master-port=PoORT
      --directory=MAP
      --exec-server-task=POORT
      --host-priv-port=POORT

  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven
  -V, --version              Programmaversie afdrukken

Verplichte of optionele argumenten voor lange opties zijn ook verplicht of optioneel voor overeenkomstige korte opties.

Als noch --interleave noch --layer is opgegeven, worden meerdere APPARATEN samengevoegd.

Bugs melden aan bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_usermux" name="usermux" type="text/html">
De usermux-vertaler - &quot;<code>usermux</code>&quot;</a></h2>

<p>
Een vertaler voor het aanroepen van gebruikersspecifieke vertalers.

<p>
Het uitvoeren van &quot;<code>/hurd/usermux --help</code>&quot; geeft:
<br>
<pre>
Gebruik: usermux [OPTIE...] [VERTALER [ARG...]]
Een vertaler voor het aanroepen van gebruikersspecifieke vertalers.

  -C, --clear-patterns       Alle patronen terug op leeg instellen; deze optie
                             kan dan gevolgd worden door opties om specifieke
                             patronen in te stellen
      --home-pattern=PAT     De tekenreeks die in de vertalerspecificatie moet
                             worden vervangen door de persoonlijke map van de
                             gebruiker (standaard `${home}')
      --uid-pattern=PAT      De tekenreeks die in de vertalerspecificatie moet
                             worden vervangen door de uid (standaard `${uid}')
      --user-pattern=PAT     De tekenreeks die in de vertalerspecificatie moet
                             worden vervangen door de gebruikersnaam (standaard
                             `${user}')
  -?, --help                 Deze hulplijst weergeven
      --usage                Een kort gebruiksbericht weergeven

Deze vertaler verschijnt als een map waarin gebruikersnamen kunnen worden opgezocht en zal VERTALER starten om elke resulterende node van dienst te zijn. Als er geen patroon voorkomt in de vertalerspecificatie, wordt de persoonlijke map van de gebruiker toegevoegd; VERTALER is standaard ingesteld op /hurd/symlink.

Bugs melden aan bug-hurd@gnu.org.
</pre>



# Uncomment and fill the blanks...
#<h2 class="center"><a href="#TOC_" name="" type="text/html">
#The  server - &quot;<code></code>&quot;</a></h2>
#
#<p>
#A server for .
#
#<p>
#Running &quot;<code>/hurd/ --help</code>&quot; gives:
#<br>
#<pre>
#
#</pre>
