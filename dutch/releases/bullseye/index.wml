#use wml::debian::template title="Debian &ldquo;bullseye&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e"

<p>Debian <current_release_bullseye> werd uitgebracht op
<a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 werd oorspronkelijk uitgebracht op <:=spokendate('2021-08-14'):>."
/>
De release bevatte verschillende ingrijpende wijzigingen,
beschreven in ons
<a href="$(HOME)/News/2021/20210814">persbericht</a> en de
<a href="releasenotes">notities bij de release</a>.</p>

<p><strong>Debian 11 werd vervangen door
<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Er worden geen beveiligingsupdates meer uitgebracht sinds <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### Deze paragraaf is indicatief; kijk ze na voor publicatie!
#<p><strong>Bullseye geniet evenwel van langetermijnondersteuning (Long Term Support - LTS) tot
#eind xxxxx 20xx. De LTS is beperkt tot i386, amd64, armel, armhf en arm64.
#Alle andere architecturen worden niet langer ondersteund in bullseye.
#Raadpleeg voor meer informatie de <a
#href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
#</strong></p>

<p>Raadpleeg de <a href="debian-installer/">installatie-informatie</a>-pagina
en de <a href="installmanual">Installatiehandleiding</a> over het verkrijgen
en installeren van Debian. Zie de instructies in de
<a href="releasenotes">notities bij de release</a> om van een oudere Debian
release op te waarderen.</p>

### Wat volgt activeren wanneer de LTS-periode begint.
#<p>Ondersteunde architecturen tijdens de langetermijnondersteuning:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Ondersteunde computerarchitecturen bij de initiële release van bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd <a href="../reportingbugs">andere problemen rapporteren</a>.</p>

<p>Tot slot, maar niet onbelangrijk, een overzicht van de
<a href="credits">mensen</a> die deze release mogelijk maakten.</p>

