#use wml::debian::template title="Information for Debian Consultants" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#policy">Policy for Debian's Consultants Page</a></li>
  <li><a href="#change">Additions, Modifications, and Removals of Entries</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Consultants can communicate with other Debian consultants via the <a href="https://lists.debian.org/debian-consultants/">debian-consultants mailing list</a>. The list is not moderated; posting is allowed by anyone. The mailing list also provides a public archive.</p>
</aside>

<h2><a id="policy">Policy for Debian's Consultants Page</a></h2>

<p>If you want to be listed as a consultant at the Debian website,
then please note the following rules:</p>

<ul>
  <li><strong>Mandatory contact information:</strong><br />
    You must provide a working email address and answer emails sent to you
    within four weeks at least. In order to prevent abuse, any requests (additions,
    removals, or changes) have to be sent from that same address. To avoid spam,
    you can request to have your mail address scrambled (e.g.
    <em>debian -dot- consulting -at- example -dot- com</em>). Please note this
    explicitly in your submission. Alternatively, you can  
    request that your email address does not appear on the website at all
    (although we need a working email address for list maintenance).
    If you want your address to be hidden on the list, you can instead provide
    the URL of a contact form on your website for us to link to.
    <em>Contact</em> is meant to be used for this.
  </li>
  <li><strong>Your website:</strong><br />
    If you provide a link to a website, that website must have a
    mention of your Debian consulting services. Providing the direct link
    instead of just the homepage is not mandatory if the information is
    reasonably accessible, but much appreciated.
  </li>
  <li><strong>Multiple cities/regions/countries:</strong><br />
    You have to choose the country (only one) that you want to be
    listed under. Additional cities, regions or countries should be
    listed either as part of the additional info or on your own website.
  </li>
  <li><strong>Rules for Debian developers:</strong><br />
    You are not allowed to use your official <em>@debian.org</em> email address
    for the consultant listing. The same goes for your website: you may
    not use an official <em>*.debian.org domain</em> as described
    in the <a href="$(HOME)/devel/dmup">DMUP</a> (Debian Machine Usage Policies).
  </li>
</ul>

<p>If the above criteria are no longer met at some future point, the
consultant should receive a warning message that they are about to be
removed from the list, unless they fulfill all criteria again.
There should be a grace period of four weeks.</p>

<p>Some parts (or all) of the consultant's information can be removed
if they don't comply with this policy anymore, or at the discretion of
the list maintainers.</p>

<h2><a id="change">Additions, Modifications, and Removals of Entries</a></h2>

<p>If you wish to be added to the list of consultants, please send an email to
   <a href="mailto:consultants@debian.org">consultants@debian.org</a>,
   in English, providing any of the following pieces of information that you
   would like to be listed (email address is mandatory, everything else is
   optional at your discretion):</p>

<ul>
  <li>Country for which you want to be listed</li>
  <li>Name</li>
  <li>Company</li>
  <li>Address</li>
  <li>Phone</li>
  <li>Fax</li>
  <li>Contact</li>
  <li>Email</li>
  <li>URL</li>
  <li>Rates</li>
  <li>Additional information</li>
</ul>

<p>A request for an update of the consultant's information should be sent
to <a href="mailto:consultants@debian.org">consultants@debian.org</a>
via email, preferably from the address mentioned on the <a
href="https://www.debian.org/consultants/">consultants page</a>.</p>


# translators can, but don't have to, translate the rest - yet
# BEGIN future version
# fill out the following submission form:</p>
# 
# <form method=post action="https://cgi.debian.org/cgi-bin/submit_consultant.pl">
# 
# <p>
# <input type="radio" name="submissiontype" value="new" checked>
# New consultant listing submission
# <br />
# <input type="radio" name="submissiontype" value="update">
# Update of an existing consultant listing
# <br />
# <input type="radio" name="submissiontype" value="remove">
# Removal of an existing consultant listing
# </p>
# 
# <p>Name:<br />
# <input type="text" name="name" size="50"></p>
# 
# <p>Company:<br />
# <input type="text" name="company" size="50"></p>
# 
# <p>Address:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
# 
# <p>Phone:<br />
# <input type="text" name="phone" size="50"></p>
# 
# <p>Fax:<br />
# <input type="text" name="fax" size="50"></p>
# 
# <p>E-mail:<br />
# <input type="text" name="email" size="50"></p>
# 
# <p>URL:<br />
# <input type="text" name="url" size="50"></p>
# 
# <p>Rates:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
# 
# <p>Additional information, if any (<em>in English</em>):<br />
# <textarea name="comment" cols=40 rows=7></textarea></p>
# 
# </form>
# 
# <p>If you are unable to submit the above for any reason whatsoever,
# please send it via e-mail <em>in English</em> to
# END future version
