#use wml::debian::template title="Introduction to Debian" MAINPAGE="true" 
#use wml::debian::recent_list

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian is a Community</h2>
      <p>Thousands of volunteers around the world work together on the Debian operating system, prioritizing Free and Open Source Software. Meet the Debian project.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">People:</a>
          Who we are and what we do
        </li>
        <li>
          <a href="philosophy">Our Philosophy:</a>
          Why we do it and how we do it
        </li>
        <li>
          <a href="../devel/join/">Get involved:</a>
          How to become a Debian contributor
        </li>
        <li>
          <a href="help">Contribute:</a>
          How you can help Debian
        </li>
        <li>
          <a href="../social_contract">Debian Social Contract:</a>
          Our moral agenda
        </li>
        <li>
          <a href="diversity">Everyone is welcome:</a>
          Debian's Diversity Statement
        </li>
        <li>
          <a href="../code_of_conduct">For Participants:</a>
          Debian's Code of conduct
        </li>
        <li>
          <a href="../partners/">Partners:</a>
          Companies and organizations assisting the Debian project
        </li>
        <li>
          <a href="../donations">Donations:</a>
          How to sponsor the Debian project
        </li>
        <li>
          <a href="../legal/">Legal Issues:</a>
          Licenses, trademarks, privacy policy, patent policy, etc.
        </li>
        <li>
          <a href="../contact">Contact:</a>
          How to get in touch with us
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian is an Operating System</h2>
      <p>Debian is a free operating system, developed and maintained by the Debian project. A free Linux distribution with thousands of applications to meet our users' needs.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Download:</a>
          Where to get Debian
        </li>
        <li>
          <a href="why_debian">Why Debian:</a>
          Reasons to choose Debian
        </li>
        <li>
          <a href="../support">Support:</a>
          Where to find help
        </li>
        <li>
          <a href="../security">Security:</a>
          Last update <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Software:</a>
          Search and browse the long list of Debian packages
        </li>
        <li>
          <a href="../doc">Documentation:</a>
          Installation guide, FAQ, HOWTOs, Wiki, and more
        </li>
        <li>
          <a href="../bugs">Bug Tracking System (BTS):</a>
          How to report a bug, BTS documentation
        </li>
        <li>
          <a href="https://lists.debian.org/">Mailing Lists:</a>
          Collection of Debian lists for users, developers, etc.
        </li>
        <li>
          <a href="../blends">Pure Blends:</a>
          Meta packages for specific needs
        </li>
        <li>
          <a href="../devel">Developers' Corner:</a>
          Information primarily of interest to Debian developers
        </li>
        <li>
          <a href="../ports">Ports/Architectures:</a>
          Debian support for various CPU architectures
        </li>
        <li>
          <a href="search">Search:</a>
          Information on how to use the Debian search engine
        </li>
        <li>
          <a href="cn">Languages:</a>
          Language settings for the Debian website
        </li>
      </ul>
    </div>
  </div>

</div>

