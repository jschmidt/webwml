<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4008">CVE-2016-4008</a>

    <p>infinite loop while parsing DER certificates
    The _asn1_extract_der_octet function in lib/decoding.c in GNU Libtasn1
    before 4.8, when used without the ASN1_DECODE_FLAG_STRICT_DER flag,
    allows remote attackers to cause a denial of service
    (infinite recursion) via a crafted certificate.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.13-2+deb7u3.</p>

<p>We recommend that you upgrade your libtasn1-3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-495.data"
# $Id: $
