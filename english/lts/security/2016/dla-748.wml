<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Scott Tenaglia discovered a heap-based buffer overflow in libupnp4, a
portable SDK for UPnP Devices. That can lead to denial of service or
remote code execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.0~svn20100507-1.2+deb7u1.</p>

<p>We recommend that you upgrade your libupnp4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-748.data"
# $Id: $
