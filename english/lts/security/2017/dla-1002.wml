<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Sebastian Krahmer from SUSE discovered that smb4k, a Samba (SMB) share
advanced browser, contains a logic flaw in which the mount helper binary
does not properly verify the mount command it is being asked to run.</p>

<p>This allows local users to call any other binary as root.</p>

<p>The issue is resolved by backporting version 1.2.1-2 from Debian 9
<q>Stretch</q>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.2.1-2~deb7u1.</p>

<p>We recommend that you upgrade your smb4k packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1002.data"
# $Id: $
