<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in qemu, a fast processor
emulator. The Common Vulnerabilities and Exposures project identifies
the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2615">CVE-2017-2615</a>

    <p>The Cirrus CLGD 54xx VGA Emulator in qemu is vulnerable to an
    out-of-bounds access issue. It could occur while copying VGA data
    via bitblt copy in backward mode.</p>

    <p>A privileged user inside guest could use this flaw to crash the
    Qemu process resulting in DoS OR potentially execute arbitrary
    code on the host with privileges of Qemu process on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2620">CVE-2017-2620</a>

    <p>The Cirrus CLGD 54xx VGA Emulator in qemu is vulnerable to an
    out-of-bounds access issue. It could occur while copying VGA data
    in cirrus_bitblt_cputovideo.</p>

    <p>A privileged user inside guest could use this flaw to crash the
    Qemu process resulting in DoS OR potentially execute arbitrary
    code on the host with privileges of Qemu process on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5898">CVE-2017-5898</a>

    <p>The CCID Card device emulator support is vulnerable to an integer
    overflow flaw. It could occur while passing message via
    command/responses packets to and from the host.</p>

    <p>A privileged user inside guest could use this flaw to crash the
    Qemu process on host resulting in DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5973">CVE-2017-5973</a>

    <p>The USB xHCI controller emulator support in qemu is vulnerable
    to an infinite loop issue. It could occur while processing control
    transfer descriptors' sequence in xhci_kick_epctx.</p>

    <p>A privileged user inside guest could use this flaw to crash the
    Qemu process resulting in DoS.</p></li>

</ul>

<p>This update also updates the fix <a href="https://security-tracker.debian.org/tracker/CVE-2016-9921">CVE-2016-9921</a> 
since it was too strict and broke certain guests.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u20.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-845.data"
# $Id: $
