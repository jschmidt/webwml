<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Secunia Research has discovered multiple vulnerabilities in GnuTLS
libtasn1, which can be exploited by malicious people to compromise
a vulnerable system.</p>

<p>Two errors in the "asn1_find_node()" function (lib/parser_aux.c)
can be exploited to cause a stacked-based buffer overflow.</p>

<p>Successful exploitation of the vulnerabilities allows execution
of arbitrary code but requires tricking a user into processing
a specially crafted assignments file by e.g. asn1Coding utility.</p>


<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
2.13-2+deb7u4.</p>

<p>We recommend that you upgrade your libtasn1-3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-950.data"
# $Id: $
