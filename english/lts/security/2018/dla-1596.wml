<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there can be a denial of service (DoS)
vulnerability in squid3 due to a memory leak in SNMP query rejection
code when SNMP is enabled. In environments where per-process memory
restrictions are not enforced strictly, a remote attacker to consume
all memory available to the Squid process, causing it to crash.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.4.8-6+deb8u6.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1596.data"
# $Id: $
