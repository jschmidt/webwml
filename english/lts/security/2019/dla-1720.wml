<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that liblivemedia, the LIVE555 RTSP server library,
is vulnerable to an invalid memory access when processing the
Authorization header field. Remote attackers could leverage this
vulnerability to possibly trigger code execution or denial of service
(OOB access and application crash) via a crafted HTTP header.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2014.01.13-1+deb8u3.</p>

<p>We recommend that you upgrade your liblivemedia packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1720.data"
# $Id: $
