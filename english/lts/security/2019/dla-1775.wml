<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Colin Snover discovered a denial-of-service vulnerability in phpBB3, a
full-featured web forum. Previous versions allowed users to run searches
that might result in long execution times and load on larger boards when
using the fulltext native search engine. To combat this, further
restrictions were introduced on search queries.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.0.12-5+deb8u3.</p>

<p>We recommend that you upgrade your phpbb3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1775.data"
# $Id: $
