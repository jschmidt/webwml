<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The Oniguruma regular expressions library, notably used in PHP
mbstring, is vulnerable to stack exhaustion.  A crafted regular
expression can crash the process.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.9.5-3.2+deb8u3.</p>

<p>We recommend that you upgrade your libonig packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1918.data"
# $Id: $
