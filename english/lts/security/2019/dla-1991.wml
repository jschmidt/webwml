<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In libssh2, SSH_MSG_DISCONNECT logic in packet.c has an integer
overflow in a bounds check, enabling an attacker to specify an
arbitrary (out-of-bounds) offset for a subsequent memory read. A
crafted SSH server may be able to disclose sensitive information or
cause a denial of service condition on the client system when a user
connects to the server</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.4.3-4.1+deb8u6.</p>

<p>We recommend that you upgrade your libssh2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1991.data"
# $Id: $
