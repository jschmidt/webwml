<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in tiff, a Tag Image File Format library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17546">CVE-2019-17546</a>

      <p>The RGBA interface contains an integer overflow that might lead
      to heap buffer overflow write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6128">CVE-2019-6128</a>

      <p>A memory leak exists due to missing cleanup code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18661">CVE-2018-18661</a>

      <p>In case of exhausted memory there is a null pointer dereference
      in tiff2bw.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12900">CVE-2018-12900</a>

      <p>Fix for heap-based buffer overflow, that could be used to crash an
      application or even to execute arbitrary code (with the permission
      of the user running this application).</p>

  <p><a href="https://security-tracker.debian.org/tracker/CVE-2017-17095">CVE-2017-17095</a></p>

      <p>A crafted tiff file could lead to a heap buffer overflow in pal2rgb.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.0.3-12.3+deb8u10.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2009.data"
# $Id: $
