<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue in bsdiff, a tool to generate/apply a patch between two binary
files, has been found.</p>

<p>Using a crafted patch file an integer signedness error in bspatch could be
used for a heap based buffer overflow and possibly execution of arbitrary
code.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.3-15+deb8u1.</p>

<p>We recommend that you upgrade your bsdiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2010.data"
# $Id: $
