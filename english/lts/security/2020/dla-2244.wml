<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an escaping issue in
<tt>libphp-phpmailer</tt>, an email generation utility class for the PHP
programming language.</p>

<p>The <tt>Content-Type</tt> and <tt>Content-Disposition</tt> headers could have
permitted file attachments that bypassed attachment filters which
match on filename extensions. For more information, please see the
<a href="https://github.com/PHPMailer/PHPMailer/security/advisories/GHSA-f7hx-fqxw-rvvj">upstream announcement</a></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13625">CVE-2020-13625</a>

    <p>PHPMailer before 6.1.6 contains an output escaping bug when the name of
    a file attachment contains a double quote character. This can result in the
    file type being misinterpreted by the receiver or any mail relay processing
    the message.</p></li>
</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.2.9+dfsg-2+deb8u6.</p>

<p>We recommend that you upgrade your libphp-phpmailer packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2244.data"
# $Id: $
