<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in Wordpress, a web
blogging tool. They allowed remote attackers to perform
various Cross-Side Scripting (XSS) attacks, create open
redirects, escalate privileges, and bypass authorization
access.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4046">CVE-2020-4046</a>

    <p>In affected versions of WordPress, users with low
    privileges (like contributors and authors) can use the
    embed block in a certain way to inject unfiltered HTML
    in the block editor. When affected posts are viewed by a
    higher privileged user, this could lead to script
    execution in the editor/wp-admin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4047">CVE-2020-4047</a>

    <p>In affected versions of WordPress, authenticated users with
    upload permissions (like authors) are able to inject
    JavaScript into some media file attachment pages in a certain
    way. This can lead to script execution in the context of a
    higher privileged user when the file is viewed by them.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4048">CVE-2020-4048</a>

    <p>In affected versions of WordPress, due to an issue in
    wp_validate_redirect() and URL sanitization, an arbitrary
    external link can be crafted leading to unintended/open
    redirect when clicked.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4049">CVE-2020-4049</a>

    <p>In affected versions of WordPress, when uploading themes, the
    name of the theme folder can be crafted in a way that could
    lead to JavaScript execution in /wp-admin on the themes page.
    This does require an admin to upload the theme, and is low
    severity self-XSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4050">CVE-2020-4050</a>

    <p>In affected versions of WordPress, misuse of the
    `set-screen-option` filter's return value allows arbitrary
    user meta fields to be saved. It does require an admin to
    install a plugin that would misuse the filter. Once installed,
    it can be leveraged by low privileged users.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.1.31+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2269.data"
# $Id: $
