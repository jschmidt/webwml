<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>lemonldap-ng community fixed a vulnerability in the Nginx default
configuration files (<a href="https://security-tracker.debian.org/tracker/CVE-2020-24660">CVE-2020-24660</a>).
Debian package does not install any default site, but documentation
provided insecure examples in Nginx configuration before this version.
If you use lemonldap-ng handler with Nginx, you should verify your
configuration files.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.9.7-3+deb9u4.</p>

<p>We recommend that you upgrade your lemonldap-ng packages.</p>

<p>For the detailed security status of lemonldap-ng please refer to
its security tracker page at:
<a  rel="nofollow" href="https://security-tracker.debian.org/tracker/lemonldap-ng">https://security-tracker.debian.org/tracker/lemonldap-ng</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a  rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2367.data"
# $Id: $
