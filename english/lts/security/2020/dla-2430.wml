<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Vaisha Bernard discovered that Blueman, a graphical bluetooth manager
performed insufficient validation on a D-Bus interface, which could
result in denial of service or privilege escalation.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.0.4-1+deb9u1.</p>

<p>We recommend that you upgrade your blueman packages.</p>

<p>For the detailed security status of blueman please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/blueman">https://security-tracker.debian.org/tracker/blueman</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2430.data"
# $Id: $
