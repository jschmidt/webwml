<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>golang-go.crypto was recently updated with a fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>. This in
turn requires all packages that use the affected code to be recompiled in order
to pick up the security fix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>

    <p>An issue was discovered in supplementary Go cryptography libraries, aka
    golang-googlecode-go-crypto. If more than 256 GiB of keystream is
    generated, or if the counter otherwise grows greater than 32 bits, the amd64
    implementation will first generate incorrect output, and then cycle back to
    previously generated keystream. Repeated keystream bytes can lead to loss of
    confidentiality in encryption applications, or to predictability in CSPRNG
    applications.</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
2.21-2+deb9u1.</p>

<p>We recommend that you upgrade your snapd packages.</p>

<p>For the detailed security status of snapd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/snapd">https://security-tracker.debian.org/tracker/snapd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2527.data"
# $Id: $
