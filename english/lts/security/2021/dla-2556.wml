<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been corrected in unbound, a
validating, recursive, caching DNS resolver. Support for the unbound DNS server
has been resumed, the sources can be found in the unbound1.9 source package.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12662">CVE-2020-12662</a>

    <p>Unbound has Insufficient Control of Network Message
    Volume, aka an <q>NXNSAttack</q> issue. This is triggered by random
    subdomains in the NSDNAME in NS records.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12663">CVE-2020-12663</a>

    <p>Unbound has an infinite loop via malformed DNS answers received from
    upstream servers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28935">CVE-2020-28935</a>

    <p>Unbound contains a local vulnerability that would allow for a local symlink
    attack. When writing the PID file Unbound creates the file if it is not
    there, or opens an existing file for writing. In case the file was already
    present, it would follow symlinks if the file happened to be a symlink
    instead of a regular file.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.9.0-2+deb10u2~deb9u1.</p>

<p>We recommend that you upgrade your unbound1.9 packages.</p>

<p>For the detailed security status of unbound1.9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/unbound1.9">https://security-tracker.debian.org/tracker/unbound1.9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2556.data"
# $Id: $
