<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Four security issues have been discovered in cgal. A code execution vulnerability exists
in the Nef polygon-parsing functionality of CGAL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28601">CVE-2020-28601</a>

    <p>An oob read vulnerability exists in Nef_2/PM_io_parser.h PM_io_parser::read_vertex()
    Face_of[] OOB read. An attacker can provide malicious input to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28636">CVE-2020-28636</a>

    <p>An oob read vulnerability exists in Nef_S2/SNC_io_parser.h SNC_io_parser::read_sloop()
    slh->twin() An attacker can provide malicious input to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35628">CVE-2020-35628</a>

    <p>An oob read vulnerability exists in Nef_S2/SNC_io_parser.h SNC_io_parser::read_sloop()
    slh->incident_sface. An attacker can provide malicious input to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35636">CVE-2020-35636</a>

    <p>An oob read vulnerability exists in Nef_S2/SNC_io_parser.h SNC_io_parser::read_sface()
    sfh->volume(). An attacker can provide malicious input to trigger this vulnerability.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.9-1+deb9u1.</p>

<p>We recommend that you upgrade your cgal packages.</p>

<p>For the detailed security status of cgal please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cgal">https://security-tracker.debian.org/tracker/cgal</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2649.data"
# $Id: $
