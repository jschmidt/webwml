<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The ieee-data package, which provides the OUI and IAB listings of
identifiers assigned by IEEE Standards Association, ships a script
(update-ieee-data) which queries ieee.org to download the most recent
dataset and save it to /var/lib/ieee-data/.</p>

<p>This script broke for stretch at the end of 2018 when the URL changed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
20160613.1+deb9u1.</p>

<p>We recommend that you upgrade your ieee-data packages.</p>

<p>For the detailed security status of ieee-data please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ieee-data">https://security-tracker.debian.org/tracker/ieee-data</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2703.data"
# $Id: $
