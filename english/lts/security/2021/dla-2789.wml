<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Richard Weinberger reported that unsquashfs in squashfs-tools, the tools
to create and extract Squashfs filesystems, does not check for duplicate
filenames within a directory. An attacker can take advantage of this flaw
for writing to arbitrary files to the filesystem if a malformed Squashfs
image is processed.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1:4.3-3+deb9u3.</p>

<p>We recommend that you upgrade your squashfs-tools packages.</p>

<p>For the detailed security status of squashfs-tools please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/squashfs-tools">https://security-tracker.debian.org/tracker/squashfs-tools</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2789.data"
# $Id: $
