<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in Salt, a powerful
remote execution manager, that allow for local privilege escalation on a
minion, server side template injection attacks, insufficient checks for
eauth credentials, shell and command injections or incorrect validation of
SSL certificates.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2016.11.2+ds-1+deb9u7.</p>

<p>We recommend that you upgrade your salt packages.</p>

<p>For the detailed security status of salt please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/salt">https://security-tracker.debian.org/tracker/salt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2815.data"
# $Id: $
