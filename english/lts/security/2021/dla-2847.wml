<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security issue was discovered in MediaWiki, a website engine for
collaborative work: Missing validation in the mcrundo action may allow
allow an attacker to leak page content from private wikis or to bypass
edit restrictions.</p>

<p>For additional information please refer to
<a href="https://www.mediawiki.org/wiki/2021-12_security_release/FAQ">https://www.mediawiki.org/wiki/2021-12_security_release/FAQ</a></p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:1.27.7-1+deb9u11.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2847.data"
# $Id: $
