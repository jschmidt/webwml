<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in Pillow, a Python imaging
library, which could result in denial of service and potentially
the execution of arbitrary code if malformed images are processed.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
4.0.0-4+deb9u4.</p>

<p>We recommend that you upgrade your pillow packages.</p>

<p>For the detailed security status of pillow please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pillow">https://security-tracker.debian.org/tracker/pillow</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2893.data"
# $Id: $
