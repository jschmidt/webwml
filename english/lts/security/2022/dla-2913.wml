<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>xterm, an X terminal emulator, when Sixel support is enabled, allows
attackers to trigger a buffer overflow in set_sixel in graphics_sixel.c
via crafted text.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
327-2+deb9u2.</p>

<p>We recommend that you upgrade your xterm packages.</p>

<p>For the detailed security status of xterm please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xterm">https://security-tracker.debian.org/tracker/xterm</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2913.data"
# $Id: $
