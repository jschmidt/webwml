<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Security researchers of JFrog Security and Ismail Aydemir discovered two
remote code execution vulnerabilities in the H2 Java SQL database engine
which can be exploited through various attack vectors, most notably through
the H2 Console and by loading custom classes from remote servers through
JNDI. The H2 console is a developer tool and not required by any reversedependency in Debian. It has been disabled in (old)stable releases.
Database developers are advised to use at least version 2.1.210-1, currently
available in Debian unstable.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.4.193-1+deb9u1.</p>

<p>We recommend that you upgrade your h2database packages.</p>

<p>For the detailed security status of h2database please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/h2database">https://security-tracker.debian.org/tracker/h2database</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2923.data"
# $Id: $
