<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update announced as DLA 2962-1 have a regression due to
mistake in backported <a href="https://security-tracker.debian.org/tracker/CVE-2022-23608">CVE-2022-23608</a> patch. Updated packages of
pjproject are now available.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.5.5~dfsg-6+deb9u4.</p>

<p>We recommend that you upgrade your pjproject packages.</p>

<p>For the detailed security status of pjproject please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pjproject">https://security-tracker.debian.org/tracker/pjproject</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2962-2.data"
# $Id: $
