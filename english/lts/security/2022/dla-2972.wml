<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Five security issues have been discovered in libxml2: XML C parser and toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9318">CVE-2016-9318</a>

    <p>Vulnerable versions do not offer a flag directly indicating that the current
    document may be read but other files may not be opened, which makes it
    easier for remote attackers to conduct XML External Entity (XXE) attacks via
    a crafted document.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5130">CVE-2017-5130</a>

    <p>Integer overflow in memory debug code, allowed a remote attacker to
    potentially exploit heap corruption via a crafted XML file.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5969">CVE-2017-5969</a>

    <p>Parser in a recover mode allows remote attackers to cause a denial of service
    (NULL pointer dereference) via a crafted XML document.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16932">CVE-2017-16932</a>

    <p>When expanding a parameter entity in a DTD, infinite recursion could lead to
    an infinite loop or memory exhaustion.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23308">CVE-2022-23308</a>

    <p>the application that validates XML using xmlTextReaderRead() with
    XML_PARSE_DTDATTR and XML_PARSE_DTDVALID  enabled becomes vulnerable to this
    use-after-free bug. This issue can result in denial of service.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.9.4+dfsg1-2.2+deb9u6.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>For the detailed security status of libxml2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxml2">https://security-tracker.debian.org/tracker/libxml2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2972.data"
# $Id: $
