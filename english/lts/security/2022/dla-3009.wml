<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A couple of vulnerabilities were found in src:cifs-utils, a Common
Internet File System utilities, and are as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27239">CVE-2022-27239</a>

    <p>In cifs-utils, a stack-based buffer overflow when parsing the
    mount.cifs ip= command-line argument could lead to local attackers
    gaining root privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29869">CVE-2022-29869</a>

    <p>cifs-utils, with verbose logging, can cause an information leak
    when a file contains = (equal sign) characters but is not a valid
    credentials file.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2:6.7-1+deb9u1.</p>

<p>We recommend that you upgrade your cifs-utils packages.</p>

<p>For the detailed security status of cifs-utils please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cifs-utils">https://security-tracker.debian.org/tracker/cifs-utils</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3009.data"
# $Id: $
