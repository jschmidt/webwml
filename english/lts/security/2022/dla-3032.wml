<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was found in the check_chunk_name() function of pngcheck, a tool to&nbsp;
verify the integrity of PNG, JNG and MNG files. This flaw allows an attacker
who can pass a malicious file to be processed by pngcheck to cause a temporary
denial of service.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.3.0-7+deb9u1.</p>

<p>We recommend that you upgrade your pngcheck packages.</p>

<p>For the detailed security status of pngcheck please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/pngcheck">https://security-tracker.debian.org/tracker/pngcheck</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3032.data"
# $Id: $
