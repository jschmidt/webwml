<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was credential disclosure vulnerability
python-oslo.utils, a set of utilities used by OpenStack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0718">CVE-2022-0718</a>

    <p>A flaw was found in python-oslo-utils. Due to improper parsing,
    passwords with a double quote ( " ) in them cause incorrect masking in
    debug logs, causing any part of the password after the double quote to be
    plaintext.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
3.36.5-0+deb10u2.</p>

<p>We recommend that you upgrade your python-oslo.utils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3106.data"
# $Id: $
