<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2022e. Notable
changes are:</p>

<p>- Syria and Jordan are abandoning the DST regime and are changing to
  permanent +03, so they will not fall back from +03 to +02 on
  2022-10-28.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2021a-0+deb10u8.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>For the detailed security status of tzdata please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tzdata">https://security-tracker.debian.org/tracker/tzdata</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3161.data"
# $Id: $
