<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that libxml2, the GNOME XML library, was vulnerable to
integer overflows and memory corruption.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40303">CVE-2022-40303</a>

     <p>Parsing a XML document with the XML_PARSE_HUGE option enabled can result
     in an integer overflow because safety checks were missing in some
     functions. Also, the xmlParseEntityValue function did not have any length
     limitation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40304">CVE-2022-40304</a>

     <p>When a reference cycle is detected in the XML entity cleanup function the
     XML entity data can be stored in a dictionary. In this case, the
     dictionary becomes corrupted resulting in logic errors, including memory
     errors like double free.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.9.4+dfsg1-7+deb10u5.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>For the detailed security status of libxml2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxml2">https://security-tracker.debian.org/tracker/libxml2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3172.data"
# $Id: $
