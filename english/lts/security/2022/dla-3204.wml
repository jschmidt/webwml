<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes multiple memory access violations in vim.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0318">CVE-2022-0318</a>

    <p>Heap-based Buffer Overflow</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0392">CVE-2022-0392</a>

    <p>Heap-based Buffer Overflow</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0629">CVE-2022-0629</a>

    <p>Stack-based Buffer Overflow</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0696">CVE-2022-0696</a>

    <p><code>NULL</code> Pointer Dereference</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1619">CVE-2022-1619</a>

    <p>Heap-based Buffer Overflow in function <code>cmdline_erase_chars</code>.  This
    vulnerabilities are capable of crashing software, modify memory, and
    possible remote execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1621">CVE-2022-1621</a>

    <p>Heap buffer overflow in <code>vim_strncpy find_word</code>. This vulnerability is
    capable of crashing software, Bypass Protection Mechanism, Modify
    Memory, and possible remote execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1785">CVE-2022-1785</a>

    <p>Out-of-bounds Write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1897">CVE-2022-1897</a>

    <p>Out-of-bounds Write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1942">CVE-2022-1942</a>

    <p>Heap-based Buffer Overflow</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2000">CVE-2022-2000</a>

    <p>Out-of-bounds Write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2129">CVE-2022-2129</a>

    <p>Out-of-bounds Write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3235">CVE-2022-3235</a>

    <p>Use After Free</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3256">CVE-2022-3256</a>

    <p>Use After Free</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3352">CVE-2022-3352</a>

    <p>Use After Free</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2:8.1.0875-5+deb10u4.</p>

<p>We recommend that you upgrade your vim packages.</p>

<p>For the detailed security status of vim please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/vim">https://security-tracker.debian.org/tracker/vim</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3204.data"
# $Id: $
