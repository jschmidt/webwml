<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A potential cross-site scripting (XSS) vulnerability was discovered
in ruby-rails-html-sanitizer, a library to clean (or <q>sanitize</q>) HTML
for rendering within Ruby on Rails web applications.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.4-1+deb10u1.</p>

<p>We recommend that you upgrade your ruby-rails-html-sanitizer packages.</p>

<p>For the detailed security status of ruby-rails-html-sanitizer please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-rails-html-sanitizer">https://security-tracker.debian.org/tracker/ruby-rails-html-sanitizer</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3227.data"
# $Id: $
