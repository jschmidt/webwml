<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3759">CVE-2021-3759</a>

    <p>It was discovered that the memory cgroup controller did not
    account for kernel memory allocated for IPC objects.  A local user
    could use this for denial of service (memory exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3169">CVE-2022-3169</a>

    <p>It was discovered that the NVMe host driver did not prevent a
    concurrent reset and subsystem reset.  A local user with access to
    an NVMe device could use this to cause a denial of service (device
    disconnect or crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3435">CVE-2022-3435</a>

    <p>Gwangun Jung reported a flaw in the IPv4 forwarding subsystem
    which would lead to an out-of-bounds read.  A local user with
    CAP_NET_ADMIN capability in any user namespace could possibly
    exploit this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3521">CVE-2022-3521</a>

    <p>The syzbot tool found a race condition in the KCM subsystem
    which could lead to a crash.</p>

    <p>This subsystem is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3524">CVE-2022-3524</a>

    <p>The syzbot tool found a race condition in the IPv6 stack which
    could lead to a memory leak.  A local user could exploit this to
    cause a denial of service (memory exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3564">CVE-2022-3564</a>

    <p>A flaw was discovered in the Bluetooh L2CAP subsystem which
    would lead to a use-after-free.  This might be exploitable
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3565">CVE-2022-3565</a>

    <p>A flaw was discovered in the mISDN driver which would lead to a
    use-after-free.  This might be exploitable to cause a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3594">CVE-2022-3594</a>

    <p>Andrew Gaul reported that the r8152 Ethernet driver would log
    excessive numbers of messages in response to network errors.  A
    remote attacker could possibly exploit this to cause a denial of
    service (resource exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3628">CVE-2022-3628</a>

    <p>Dokyung Song, Jisoo Jang, and Minsuk Kang reported a potential
    heap-based buffer overflow in the brcmfmac Wi-Fi driver.  A user
    able to connect a malicious USB device could exploit this to cause
    a denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3640">CVE-2022-3640</a>

    <p>A flaw was discovered in the Bluetooh L2CAP subsystem which
    would lead to a use-after-free.  This might be exploitable
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3643">CVE-2022-3643</a> (XSA-423)

    <p>A flaw was discovered in the Xen network backend driver that would
    result in it generating malformed packet buffers.  If these
    packets were forwarded to certain other network devices, a Xen
    guest could exploit this to cause a denial of service (crash or
    device reset).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4139">CVE-2022-4139</a>

    <p>A flaw was discovered in the i915 graphics driver.  On gen12 <q>Xe</q>
    GPUs it failed to flush TLBs when necessary, resulting in GPU
    programs retaining access to freed memory.  A local user with
    access to the GPU could exploit this to leak sensitive
    information, cause a denial of service (crash or memory
    corruption) or likely for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4378">CVE-2022-4378</a>

    <p>Kyle Zeng found a flaw in procfs that would cause a stack-based
    buffer overflow.  A local user permitted to write to a sysctl
    could use this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41849">CVE-2022-41849</a>

    <p>A race condition was discovered in the smscufx graphics driver,
    which could lead to a use-after-free.  A user able to remove the
    physical device while also accessing its device node could exploit
    this to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41850">CVE-2022-41850</a>

    <p>A race condition was discovered in the hid-roccat input driver,
    which could lead to a use-after-free.  A local user able to access
    such a device could exploit this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42328">CVE-2022-42328</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-42329">CVE-2022-42329</a> (XSA-424)

    <p>Yang Yingliang reported that the Xen network backend driver did
    not use the proper function to free packet buffers in one case,
    which could lead to a deadlock.  A Xen guest could exploit this to
    cause a denial of service (hang).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42895">CVE-2022-42895</a>

    <p>Tamás Koczka reported a flaw in the Bluetooh L2CAP subsystem
    that would result in reading uninitialised memory.  A nearby
    attacker able to make a Bluetooth connection could exploit
    this to leak sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42896">CVE-2022-42896</a>

    <p>Tamás Koczka reported flaws in the Bluetooh L2CAP subsystem that
    can lead to a use-after-free.  A nearby attacker able to make a
    Bluetooth SMP connection could exploit this to cause a denial of
    service (crash or memory corruption) or possibly for remote code
    execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47518">CVE-2022-47518</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-47519">CVE-2022-47519</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-47521">CVE-2022-47521</a>

    <p>Several flaws were discovered in the wilc1000 Wi-Fi driver which
    could lead to a heap-based buffer overflow.  A nearby attacker
    could exploit these for denial of service (crash or memory
    corruption) or possibly for remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47520">CVE-2022-47520</a>

    <p>A flaw was discovered in the wilc1000 Wi-Fi driver which could
    lead to a heap-based buffer overflow.  A local user with
    CAP_NET_ADMIN capability over such a Wi-Fi device could exploit
    this for denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.10.158-2~deb10u1.</p>

<p>We recommend that you upgrade your linux-5.10 packages.</p>

<p>For the detailed security status of linux-5.10 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-5.10">https://security-tracker.debian.org/tracker/linux-5.10</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3244.data"
# $Id: $
