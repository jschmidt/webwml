<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of issues in graphite-web, a
tool provide realtime graphing of system statistics etc.</p>

<p>A series of cross-site scripting (XSS) vulnerabilties existed that could
have been exploited remotely. Issues existed in the Cookie Handler, Template
Name Handler and Absolute Time Range Handler components:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4728">CVE-2022-4728</a>

    <p>A vulnerability has been found in Graphite Web and classified
    as problematic. This vulnerability affects unknown code of the
    component Cookie Handler. The manipulation leads to cross site
    scripting. VDB-216742 is the identifier assigned to this
    vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4729">CVE-2022-4729</a>

    <p>A vulnerability was found in Graphite Web and classified as
    problematic. This issue affects some unknown processing of the
    component Template Name Handler. The manipulation leads to cross
    site scripting. The associated identifier of this vulnerability is
    VDB-216743.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4730">CVE-2022-4730</a>

    <p>A vulnerability was found in Graphite Web. It has been
    classified as problematic. Affected is an unknown function of the
    component Absolute Time Range Handler. The manipulation leads to
    cross site scripting. The identifier of this vulnerability is
    VDB-216744.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1.1.4-3+deb10u2.</p>

<p>We recommend that you upgrade your graphite-web packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3309.data"
# $Id: $
