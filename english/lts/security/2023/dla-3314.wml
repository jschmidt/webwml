<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in SDL2, the Simple
DirectMedia Layer library. These vulnerabilities may allow an attacker to
cause a denial of service or result in the execution of arbitrary code if
malformed images or sound files are processed.</p>

<p>For Debian 10 buster, these problems have been fixed in version
2.0.9+dfsg1-1+deb10u1.</p>

<p>We recommend that you upgrade your libsdl2 packages.</p>

<p>For the detailed security status of libsdl2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libsdl2">https://security-tracker.debian.org/tracker/libsdl2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3314.data"
# $Id: $
