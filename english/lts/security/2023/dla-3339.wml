<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Code execution through crafted PFS filesystems was fixed in binwalk,
a tool and Python module for analyzing binary blobs and executable code.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.1.2~git20180830+dfsg1-1+deb10u1.</p>

<p>We recommend that you upgrade your binwalk packages.</p>

<p>For the detailed security status of binwalk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/binwalk">https://security-tracker.debian.org/tracker/binwalk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3339.data"
# $Id: $
