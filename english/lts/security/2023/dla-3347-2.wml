<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2023-27372">CVE-2023-27372</a> broke (de)activation
of plugins with dependencies.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.2.4-1+deb10u11.</p>

<p>We recommend that you upgrade your spip packages.</p>

<p>For the detailed security status of spip please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/spip">https://security-tracker.debian.org/tracker/spip</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3347-2.data"
# $Id: $
