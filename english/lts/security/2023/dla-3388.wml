<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was found in keepalived, a failover and monitoring daemon for LVS
clusters, where an improper authentication vulnerability allows an
unprivileged&nbsp;user to change properties that could lead to an access-control
bypass.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:2.0.10-1+deb10u1.</p>

<p>We recommend that you upgrade your keepalived packages.</p>

<p>For the detailed security status of keepalived please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/keepalived">https://security-tracker.debian.org/tracker/keepalived</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3388.data"
# $Id: $
