<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Xi Lu discovered that missing input sanitizing in Emacs could result in the
execution of arbitrary shell commands.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1:26.1+1-3.2+deb10u4.</p>

<p>We recommend that you upgrade your emacs packages.</p>

<p>For the detailed security status of emacs please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/emacs">https://security-tracker.debian.org/tracker/emacs</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3416.data"
# $Id: $
