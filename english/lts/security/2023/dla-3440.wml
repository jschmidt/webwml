<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in cups, the Common UNIX Printing System.
Due to a buffer overflow vulnerability in the function format_log_line()
a remote attackers could cause a denial-of-service(DoS). The vulnerability
can be triggered when the configuration file cupsd.conf sets the value of
<q>loglevel</q> to <q>DEBUG</q>.</p>


<p>For Debian 10 buster, this problem has been fixed in version
2.2.10-6+deb10u7.</p>

<p>We recommend that you upgrade your cups packages.</p>

<p>For the detailed security status of cups please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cups">https://security-tracker.debian.org/tracker/cups</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3440.data"
# $Id: $
