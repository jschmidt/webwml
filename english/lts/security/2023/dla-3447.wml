<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two regular expression Denial of Service (ReDoS) issues were discovered in
Ruby: the first in the URI component, and the second in the Time module. Each
of these issues could have resulted in a dramatic increase in execution time
given malicious input.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28755">CVE-2023-28755</a>

    <p>A ReDoS issue was discovered in the URI component through 0.12.0 in Ruby
    through 3.2.1. The URI parser mishandles invalid URLs that have specific
    characters. It causes an increase in execution time for parsing strings to
    URI objects. The fixed versions are 0.12.1, 0.11.1, 0.10.2 and
    0.10.0.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28756">CVE-2023-28756</a>

    <p>A ReDoS issue was discovered in the Time component through 0.2.1 in Ruby
    through 3.2.1. The Time parser mishandles invalid URLs that have specific
    characters. It causes an increase in execution time for parsing strings to
    Time objects. The fixed versions are 0.1.1 and 0.2.2.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
2.5.5-3+deb10u5.</p>

<p>We recommend that you upgrade your ruby2.5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3447.data"
# $Id: $
