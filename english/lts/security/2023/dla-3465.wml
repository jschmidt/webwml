<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A heap-based buffer overflow vulnerability was found in the HTTP chunk
parsing code of minidlna, a lightweight DLNA/UPnP-AV server, which may
result in denial of service or the execution of arbitrary code.</p>


<p>For Debian 10 buster, this problem has been fixed in version
1.2.1+dfsg-2+deb10u4.</p>

<p>We recommend that you upgrade your minidlna packages.</p>

<p>For the detailed security status of minidlna please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/minidlna">https://security-tracker.debian.org/tracker/minidlna</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3465.data"
# $Id: $
