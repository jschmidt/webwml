<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Gregor Kopf of Secfault Security GmbH discovered that HSQLDB, a Java SQL
database engine, allowed the execution of spurious scripting commands in
.script and .log files. Hsqldb supports a <q>SCRIPT</q> keyword which is
normally used to record the commands input by the database admin to output
such a script. In combination with LibreOffice, an attacker could craft an
odb containing a "database/script" file which itself contained a SCRIPT
command where the contents of the file could be written to a new file whose
location was determined by the attacker.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.4.1-2+deb10u2.</p>

<p>We recommend that you upgrade your hsqldb packages.</p>

<p>For the detailed security status of hsqldb please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/hsqldb">https://security-tracker.debian.org/tracker/hsqldb</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3467.data"
# $Id: $
