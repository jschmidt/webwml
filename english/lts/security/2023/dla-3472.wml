<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Missing input validation in various functions may have resulted in 
denial of service in various functions provided by libx11, the X11
client-side library.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:1.6.7-1+deb10u3.</p>

<p>We recommend that you upgrade your libx11 packages.</p>

<p>For the detailed security status of libx11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libx11">https://security-tracker.debian.org/tracker/libx11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3472.data"
# $Id: $
