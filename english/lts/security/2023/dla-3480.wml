<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A Regular Expression Denial of Service (ReDoS) issue
was discovered in the sanitize_html function of redcloth gem.
This vulnerability allows attackers to cause a Denial of Service (DoS)
via supplying a crafted payload.</p>

<p>For Debian 10 buster, this problem has been fixed in version
4.3.2-3+deb10u1.</p>

<p>We recommend that you upgrade your ruby-redcloth packages.</p>

<p>For the detailed security status of ruby-redcloth please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-redcloth">https://security-tracker.debian.org/tracker/ruby-redcloth</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3480.data"
# $Id: $
