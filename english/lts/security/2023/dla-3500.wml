<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential denial of service attack in
Django, the popular Python-based web development framework.</p>

<p><code>EmailValidator</code> and <code>URLValidator</code> were subject to
potential regular expression denial of service attack via a very large number
of domain name labels of emails and URLs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-36053">CVE-2023-36053</a>

    <p>In Django 3.2 before 3.2.20, 4 before 4.1.10, and 4.2 before 4.2.3,
    EmailValidator and URLValidator are subject to a potential ReDoS (regular
    expression denial of service) attack via a very large number of domain name
    labels of emails and URLs.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:1.11.29-1+deb10u9.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3500.data"
# $Id: $
