<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1380">CVE-2023-1380</a>

    <p>Jisoo Jang reported a heap out-of-bounds read in the brcmfmac
    Wi-Fi driver.  On systems using this driver, a local user could
    exploit this to read sensitive information or to cause a denial of
    service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2002">CVE-2023-2002</a>

    <p>Ruiahn Li reported an incorrect permissions check in the Bluetooth
    subsystem.  A local user could exploit this to reconfigure local
    Bluetooth interfaces, resulting in information leaks, spoofing, or
    denial of service (loss of connection).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2007">CVE-2023-2007</a>

    <p>Lucas Leong (@_wmliang_) and Reno Robert of Trend Micro Zero Day
    Initiative discovered a time-of-check-to-time-of-use flaw in the
    dpt_i2o SCSI controller driver.  A local user with access to a
    SCSI device using this driver could exploit this for privilege
    escalation.</p>

    <p>This flaw has been mitigated by removing support for the I2OUSRCMD
    operation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2269">CVE-2023-2269</a>

    <p>Zheng Zhang reported that improper handling of locking in the
    device mapper implementation may result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3090">CVE-2023-3090</a>

    <p>It was discovered that missing initialization in ipvlan networking
    may lead to an out-of-bounds write vulnerability, resulting in
    denial of service or potentially the execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3111">CVE-2023-3111</a>

    <p>The TOTE Robot tool found a flaw in the Btrfs filesystem driver
    that can lead to a use-after-free.  It's unclear whether an
    unprivileged user can exploit this.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3141">CVE-2023-3141</a>

    <p>A flaw was discovered in the r592 memstick driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3268">CVE-2023-3268</a>

    <p>It was discovered that an out-of-bounds memory access in relayfs
    could result in denial of service or an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3338">CVE-2023-3338</a>

    <p>Ornaghi Davide discovered a flaw in the DECnet protocol
    implementation which could lead to a null pointer dereference or
    use-after-free.  A local user can exploit this to cause a denial
    of service (crash or memory corruption) and probably for privilege
    escalation.</p>

    <p>This flaw has been mitigated by removing the DECnet protocol
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20593">CVE-2023-20593</a>

    <p>Tavis Ormandy discovered that under specific microarchitectural
    circumstances, a vector register in AMD <q>Zen 2</q> CPUs may not be
    written to 0 correctly.  This flaw allows an attacker to leak
    sensitive information across concurrent processes, hyper threads
    and virtualized guests.</p>

    <p>For details please refer to
    <a href="https://lock.cmpxchg8b.com/zenbleed.html">https://lock.cmpxchg8b.com/zenbleed.html</a> and
    <a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">
    https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a>.</p>

    <p>This issue can also be mitigated by a microcode update through the
    amd64-microcode package or a system firmware (BIOS/UEFI) update.
    However, the initial microcode release by AMD only provides
    updates for second generation EPYC CPUs.  Various Ryzen CPUs are
    also affected, but no updates are available yet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31084">CVE-2023-31084</a>

    <p>It was discovered that the DVB Core driver does not properly
    handle locking of certain events, allowing a local user to cause a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32233">CVE-2023-32233</a>

    <p>Patryk Sondej and Piotr Krysiuk discovered a use-after-free flaw
    in the Netfilter nf_tables implementation when processing batch
    requests, which may result in local privilege escalation for a
    user with the CAP_NET_ADMIN capability in any user or network
    namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34256">CVE-2023-34256</a>

    <p>The syzbot tool found a time-of-check-to-time-of-use flaw in the
    ext4 filesystem driver.  An attacker able to mount a disk image or
    device that they can also write to directly could exploit this to
    cause an out-of-bounds read, possibly resulting in a leak of
    sensitive information or denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35788">CVE-2023-35788</a>

    <p>Hangyu Hua discovered an out-of-bounds write vulnerability in the
    Flower classifier which may result in denial of service or the
    execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35823">CVE-2023-35823</a>

    <p>A flaw was discovered in the saa7134 media driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35824">CVE-2023-35824</a>

    <p>A flaw was discovered in the dm1105 media driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35828">CVE-2023-35828</a>

    <p>A flaw was discovered in the renesas_udc USB device-mode driver
    that could lead to a use-after-free after the driver is removed or
    unbound from a device.  The security impact of this is unclear.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4.19.289-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3508.data"
# $Id: $
