<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security issue was discovered in Thunderbird, which could result in
spoofing of filenames of email attachments.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:102.13.1-1~deb10u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>For the detailed security status of thunderbird please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/thunderbird">https://security-tracker.debian.org/tracker/thunderbird</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3510.data"
# $Id: $
