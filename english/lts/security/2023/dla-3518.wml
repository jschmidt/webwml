<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in OpenImageIO, a
library for reading and writing images. Buffer overflows and out-of-bounds
read and write programming errors may lead to a denial of service
(application crash) or the execution of arbitrary code if a malformed image
file is processed.</p>

<p>For Debian 10 buster, these problems have been fixed in version
2.0.5~dfsg0-1+deb10u2.</p>

<p>We recommend that you upgrade your openimageio packages.</p>

<p>For the detailed security status of openimageio please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/openimageio">https://security-tracker.debian.org/tracker/openimageio</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3518.data"
# $Id: $
