<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The last update required an update to the database scheme, but as
zabbix does not support upgrading the database scheme if SQlite3 is used,
using zabbix-proxy-sqlite3 requires the user to drop the database and recreate
it with a supplied sql template file.</p>

<p>However, this template file has not been updated in the previous upload,
making this recreation difficult when not knowing the details.</p>

<p>Please read /usr/share/doc/zabbix-proxy-sqlite3/README.Debian for instructions
how to create the database file.
<p><b>Note</b>: All other database backends will automatically update the schema.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:4.0.4+dfsg-1+deb10u3.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>For the detailed security status of zabbix please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zabbix">https://security-tracker.debian.org/tracker/zabbix</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3538-2.data"
# $Id: $
