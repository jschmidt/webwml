<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issue have been found in ring/jami, a secure and distributed
voice, video and chat platform.
The issues are about missing boundary checks, resulting in out-of-bound
read access, buffer overflow or denial-of-service.</p>


<p>For Debian 10 buster, these problems have been fixed in version
20190215.1.f152c98~ds1-1+deb10u2.</p>

<p>We recommend that you upgrade your ring/jami packages.</p>

<p>For the detailed security status of ring please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ring">https://security-tracker.debian.org/tracker/ring</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3549.data"
# $Id: $
