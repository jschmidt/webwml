<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential denial of service vulnerability
in Django, a popular Python-based web development framework.</p>

<p>Upstream reported that there was a potential vulnerability in
<code>django.utils.encoding.uri_to_iri()</code>. This method was subject to
potential DoS attack via certain inputs with a very large number of Unicode
characters.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41164">CVE-2023-41164</a></li>
</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:1.11.29-1+deb10u10.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3558.data"
# $Id: $
