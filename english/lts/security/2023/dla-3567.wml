<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been identified in c-ares, an asynchronous name
resolver library:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22217">CVE-2020-22217</a>:

    <p>A buffer overflow vulnerability has been found in c-ares before
    via the function ares_parse_soa_reply in ares_parse_soa_reply.c.
    This vulnerability was discovered through fuzzing. Exploitation
    of this vulnerability may allow an attacker to execute arbitrary
    code or cause a denial of service condition.</p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
1.14.0-1+deb10u4.</p>

<p>We recommend that you upgrade your c-ares packages.</p>

<p>For the detailed security status of c-ares please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/c-ares">https://security-tracker.debian.org/tracker/c-ares</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3567.data"
# $Id: $
