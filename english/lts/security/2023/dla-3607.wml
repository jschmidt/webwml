<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It has been found that the update of freerdp2 (see DLA-3606-1) exposed a
bug in gnome-boxes, which breaks RDP connections with the symtoms of
hangs and black screens.
<p><b>Note</b>: sha256 is now used instead of sha1 to fingerprint certificates. This will
invalidate all hosts in FreeRDP known_hosts2 file, $HOME/.config/freerdp/known_hosts2.
In case of problems with the connection, try removing that file.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.30.3-2+deb10u1.</p>

<p>We recommend that you upgrade your gnome-boxes packages.</p>

<p>For the detailed security status of gnome-boxes please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gnome-boxes">https://security-tracker.debian.org/tracker/gnome-boxes</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3607.data"
# $Id: $
