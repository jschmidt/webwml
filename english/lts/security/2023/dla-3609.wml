<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>prometheus-alertmanager package, a component of Prometheus,
an application used for event monitoring and alerting,
was vulnerable to stored XSS type attack.</p>

<p>Alertmanager handles alerts sent by client applications such as the
Prometheus server. An attacker with the permission to perform POST
requests on the /api/v1/alerts endpoint could be able to execute
arbitrary JavaScript code on the users of Prometheus Alertmanager.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.15.3+ds-3+deb10u1.</p>

<p>We recommend that you upgrade your prometheus-alertmanager packages.</p>

<p>For the detailed security status of prometheus-alertmanager please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/prometheus-alertmanager">https://security-tracker.debian.org/tracker/prometheus-alertmanager</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3609.data"
# $Id: $
