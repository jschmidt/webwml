<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Security issues were discovered in inetutils, a collection of GNU
network utilities, which could lead to privilege escalation or
potentially execution of arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0053">CVE-2019-0053</a>

    <p>Thorsten Alteholz discovered that <a href="https://security-tracker.debian.org/tracker/CVE-2019-0053">CVE-2019-0053</a> was patched
    incorrectly in inetutils 2:1.9.4-7+deb10u2.  The original
    vulnerability remained: inetutils' telnet client doesn't
    sufficiently validate environment variables, which can lead to
    stack-based buffer overflows.  (This issue is limited to local
    exploitation from restricted shells.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40303">CVE-2023-40303</a>

    <p>Jeffrey Bencteux discovered that several <code>setuid()</code>, <code>setgid()</code>,
    <code>seteuid()</code> and <code>setguid()</code> return values were not checked in
    ftpd/rcp/rlogin/rsh/rshd/uucpd code, which may lead to privilege escalation.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2:1.9.4-7+deb10u3.</p>

<p>We recommend that you upgrade your inetutils packages.</p>

<p>For the detailed security status of inetutils please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/inetutils">https://security-tracker.debian.org/tracker/inetutils</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3611.data"
# $Id: $
