<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a authentication bypass vulnerability in
Redis, a popular key-value database similar to memcached.</p>

<p>On startup, Redis began listening on a Unix socket before adjusting its
permissions to the user-provided configuration. If a permissive
<code>umask(2)</code> was used, this created a race condition that enabled,
during a short period of time, another process to establish an otherwise
unauthorized connection.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45145">CVE-2023-45145</a>

    <p>Redis is an in-memory database that persists on disk. On startup, Redis
    begins listening on a Unix socket before adjusting its permissions to the
    user-provided configuration. If a permissive umask(2) is used, this creates
    a race condition that enables, during a short period of time, another
    process to establish an otherwise unauthorized connection. This problem has
    existed since Redis 2.6.0-RC1. This issue has been addressed in Redis
    versions 7.2.2, 7.0.14 and 6.2.14. Users are advised to upgrade. For users
    unable to upgrade, it is possible to work around the problem by disabling
    Unix sockets, starting Redis with a restrictive umask, or storing the Unix
    socket file in a protected directory.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
5:5.0.14-1+deb10u5.</p>

<p>We recommend that you upgrade your redis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3627.data"
# $Id: $
