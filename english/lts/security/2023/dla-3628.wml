<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that D-Bus, a simple interprocess messaging system, was
susceptible to a denial of service vulnerability if a monitor was being
run.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.12.28-0+deb10u1.</p>

<p>We recommend that you upgrade your dbus packages.</p>

<p>For the detailed security status of dbus please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dbus">https://security-tracker.debian.org/tracker/dbus</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3628.data"
# $Id: $
