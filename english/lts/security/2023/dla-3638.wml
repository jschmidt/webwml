<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been identified in h2o, a high-performance web server
with support for HTTP/2.</p>

<p>A security vulnerability <a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a> was discovered that could potentially
be exploited to disrupt server operation.</p>

<p>The vulnerability in the h2o HTTP/2 server was related to the handling of
certain types of HTTP/2 requests. In certain scenarios, an attacker could
send a series of malicious requests, causing the server to process them
rapidly and exhaust system resources.</p>

<p>The applied upstream patch changes the ABI. Therefore, if your application
is built against any shared libraries of h2o, you need to rebuild it.
No Debian package is affected.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.2.5+dfsg2-2+deb10u2.</p>

<p>We recommend that you upgrade your h2o packages.</p>

<p>For the detailed security status of h2o please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/h2o">https://security-tracker.debian.org/tracker/h2o</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3638.data"
# $Id: $
