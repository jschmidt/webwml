#use wml::debian::template title="PA-RISC Port" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hppa/menu.inc"

<h1>Debian for PA-RISC</h1>

<h2>Status</h2>
HPPA became an officially supported Debian architecture in release 
3.0 (woody), and was dropped as of stable release 6.0 (squeeze).
Additional information about the port may be found at
<a href="https://parisc.wiki.kernel.org/">https://parisc.wiki.kernel.org/</a>.

<p>
If you have questions, or would like to help, start by subscribing to the 
debian-hppa mailing list, documented below!

<h2>Contacts</h2>

The principal instigator of this port was Bdale Garbee, but he no longer
actively contributes to it.  
The best way to ask questions now is via the mailing list.

<h2>Mailing List</h2>

<p>
To subscribe to the mailing list for this port, send a message with the
word "subscribe" as the subject to
<a
href="mailto:debian-hppa-request@lists.debian.org">\
debian-hppa-request@lists.debian.org</a> to sign up, or use the 
<a href="$(HOME)/MailingLists/subscribe">mailing list subscription</a> page.
<p>
The list is archived at the
<a href="https://lists.debian.org/debian-hppa/">list archives</a>.

<h2>Links</h2>

<ul>

<li><a href="https://parisc.wiki.kernel.org/">The PA-RISC Linux Project Web</a>
<li><a href="https://www.openpa.net/">The OpenPA Project</a>
<li><a href="https://web.archive.org/web/20120119051222/http://h20000.www2.hp.com/bc/docs/support/SupportManual/c02722594/c02722594.pdf">
	HP Systems Documentation (from archive.org)</a>
<li><a href="https://web.archive.org/web/20070613192257/http://h21007.www2.hp.com/portal/site/dspp">
	HP PA-RISC Architecture Reference Documents, Etc (from archive.org)</a>

</ul>

