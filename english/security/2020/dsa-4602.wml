<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor, which
could result in denial of service, guest-to-host privilege escalation or
information leaks.</p>

<p>In addition this update provides mitigations for the <q>TSX Asynchronous Abort</q>
speculative side channel attack. For additional information please refer to
<a href="https://xenbits.xen.org/xsa/advisory-305.html">https://xenbits.xen.org/xsa/advisory-305.html</a></p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 4.8.5.final+shim4.10.4-1+deb9u12. Note that this will be the
last security update for Xen in the oldstable distribution; upstream
support for the 4.8.x branch ended by the end of December 2019. If you
rely on security support for your Xen installation an update to the
stable distribution (buster) is recommended.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.11.3+24-g14b62ab3e5-1~deb10u1.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>For the detailed security status of xen please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xen">https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4602.data"
# $Id: $
