<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Kobus van Schoor discovered that network-manager-ssh, a plugin to
provide VPN integration for SSH in NetworkManager, is prone to a
privilege escalation vulnerability. A local user with privileges to
modify a connection can take advantage of this flaw to execute arbitrary
commands as root.</p>

<p>This update drops support to pass extra SSH options to the ssh
invocation.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1.2.1-1+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.2.10-1+deb10u1.</p>

<p>We recommend that you upgrade your network-manager-ssh packages.</p>

<p>For the detailed security status of network-manager-ssh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/network-manager-ssh">https://security-tracker.debian.org/tracker/network-manager-ssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4637.data"
# $Id: $
