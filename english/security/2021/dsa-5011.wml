<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in Salt, a powerful
remote execution manager, that allow for local privilege escalation on a
minion, server side template injection attacks, insufficient checks for eauth
credentials, shell and command injections or incorrect validation of SSL
certificates.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2018.3.4+dfsg1-6+deb10u3.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 3002.6+dfsg1-4+deb11u1.</p>

<p>We recommend that you upgrade your salt packages.</p>

<p>For the detailed security status of salt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/salt">\
https://security-tracker.debian.org/tracker/salt</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5011.data"
# $Id: $
