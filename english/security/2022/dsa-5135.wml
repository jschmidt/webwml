<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Alexander Lakhin discovered that the autovacuum feature and multiple
commands could escape the "security-restricted operation" sandbox.</p>

<p>For additional information please refer to the upstream announcement
at <a href="https://www.postgresql.org/support/security/CVE-2022-1552/">\
https://www.postgresql.org/support/security/CVE-2022-1552/</a>/</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 11.16-0+deb10u1.</p>

<p>We recommend that you upgrade your postgresql-11 packages.</p>

<p>For the detailed security status of postgresql-11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-11">\
https://security-tracker.debian.org/tracker/postgresql-11</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5135.data"
# $Id: $
