<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Jaak Ristioja discovered a double-free vulnerability in GnuTLS, a
library implementing the TLS and SSL protocols, during verification of
pkcs7 signatures. A remote attacker can take advantage of this flaw to
cause an application using the GnuTLS library to crash (denial of
service), or potentially, to execute arbitrary code.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 3.7.1-5+deb11u2.</p>

<p>We recommend that you upgrade your gnutls28 packages.</p>

<p>For the detailed security status of gnutls28 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gnutls28">https://security-tracker.debian.org/tracker/gnutls28</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5203.data"
# $Id: $
