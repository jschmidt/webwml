<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in gdk-pixbuf, the GDK
Pixbuf library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44648">CVE-2021-44648</a>

    <p>Sahil Dhar reported a heap-based buffer overflow vulnerability when
    decoding the lzw compressed stream of image data, which may result
    in the execution of arbitrary code or denial of service if a
    malformed GIF image is processed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46829">CVE-2021-46829</a>

    <p>Pedro Ribeiro reported a heap-based buffer overflow vulnerability
    when compositing or clearing frames in GIF files, which may result
    in the execution of arbitrary code or denial of service if a
    malformed GIF image is processed.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.42.2+dfsg-1+deb11u1.</p>

<p>We recommend that you upgrade your gdk-pixbuf packages.</p>

<p>For the detailed security status of gdk-pixbuf please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gdk-pixbuf">https://security-tracker.debian.org/tracker/gdk-pixbuf</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5228.data"
# $Id: $
