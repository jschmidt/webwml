<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Apache Commons Configuration, a Java library providing a generic configuration
interface, performs variable interpolation, allowing properties to be
dynamically evaluated and expanded. Starting with version 2.4 and continuing
through 2.7, the set of default Lookup instances included interpolators that
could result in arbitrary code execution or contact with remote servers. These
lookups are:</p>
<ul>
<li><q>script</q> - execute expressions using the JVM script execution
engine (javax.script)</li>
<li><q>dns</q> - resolve dns records</li>
<li><q>url</q> - load values from urls, including from remote server.</li>
</ul>
<p>Applications using the interpolation defaults in the affected versions may
be vulnerable to remote code execution or unintentional contact with remote
servers if untrusted configuration values are used.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.8.0-1~deb11u1.</p>

<p>We recommend that you upgrade your commons-configuration2 packages.</p>

<p>For the detailed security status of commons-configuration2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/commons-configuration2">\
https://security-tracker.debian.org/tracker/commons-configuration2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5290.data"
# $Id: $
