<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2873">CVE-2022-2873</a>

    <p>Zheyu Ma discovered that an out-of-bounds memory access flaw in the
    Intel iSMT SMBus 2.0 host controller driver may result in denial of
    service (system crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3545">CVE-2022-3545</a>

    <p>It was discovered that the Netronome Flow Processor (NFP) driver
    contained a use-after-free flaw in area_cache_get(), which may
    result in denial of service or the execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3623">CVE-2022-3623</a>

    <p>A race condition when looking up a CONT-PTE/PMD size hugetlb page
    may result in denial of service or an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4696">CVE-2022-4696</a>

    <p>A use-after-free vulnerability was discovered in the io_uring
    subsystem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36280">CVE-2022-36280</a>

    <p>An out-of-bounds memory write vulnerability was discovered in the
    vmwgfx driver, which may allow a local unprivileged user to cause a
    denial of service (system crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41218">CVE-2022-41218</a>

    <p>Hyunwoo Kim reported a use-after-free flaw in the Media DVB core
    subsystem caused by refcount races, which may allow a local user to
    cause a denial of service or escalate privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45934">CVE-2022-45934</a>

    <p>An integer overflow in l2cap_config_req() in the Bluetooth subsystem
    was discovered, which may allow a physically proximate attacker to
    cause a denial of service (system crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47929">CVE-2022-47929</a>

    <p>Frederick Lawler reported a NULL pointer dereference in the traffic
    control subsystem allowing an unprivileged user to cause a denial of
    service by setting up a specially crafted traffic control
    configuration.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0179">CVE-2023-0179</a>

    <p>Davide Ornaghi discovered incorrect arithmetics when fetching VLAN
    header bits in the netfilter subsystem, allowing a local user to
    leak stack and heap addresses or potentially local privilege
    escalation to root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0266">CVE-2023-0266</a>

    <p>A use-after-free flaw in the sound subsystem due to missing locking
    may result in denial of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0394">CVE-2023-0394</a>

    <p>Kyle Zeng discovered a NULL pointer dereference flaw in
    rawv6_push_pending_frames() in the network subsystem allowing a
    local user to cause a denial of service (system crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23454">CVE-2023-23454</a>

    <p>Kyle Zeng reported that the Class Based Queueing (CBQ) network
    scheduler was prone to denial of service due to interpreting
    classification results before checking the classification
    return code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23455">CVE-2023-23455</a>

    <p>Kyle Zeng reported that the ATM Virtual Circuits (ATM) network
    scheduler was prone to a denial of service due to interpreting
    classification results before checking the classification
    return code.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.162-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5324.data"
# $Id: $
