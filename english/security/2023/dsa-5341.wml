<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42826">CVE-2022-42826</a>

    <p>Francisco Alonso discovered that processing maliciously crafted
    web content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23517">CVE-2023-23517</a>

    <p>YeongHyeon Choi, Hyeon Park, SeOk JEON, YoungSung Ahn, JunSeo Bae
    and Dohyun Lee discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23518">CVE-2023-23518</a>

    <p>YeongHyeon Choi, Hyeon Park, SeOk JEON, YoungSung Ahn, JunSeo Bae
    and Dohyun Lee discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.38.4-1~deb11u1.</p>

<p>We recommend that you upgrade your wpewebkit packages.</p>

<p>For the detailed security status of wpewebkit please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wpewebkit">\
https://security-tracker.debian.org/tracker/wpewebkit</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5341.data"
# $Id: $
