<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Kim Alvefur discovered that insufficient message sender validation in
dino-im, a modern XMPP/Jabber client, may result in manipulation of
entries in the personal bookmark store without user interaction via a
specially crafted message. Additionally an attacker can take advantage
of this flaw to change how group chats are displayed or force a user to
join or leave an attacker-selected groupchat.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 0.2.0-3+deb11u1.</p>

<p>We recommend that you upgrade your dino-im packages.</p>

<p>For the detailed security status of dino-im please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/dino-im">\
https://security-tracker.debian.org/tracker/dino-im</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5379.data"
# $Id: $
