<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in OpenSSL, a Secure
Sockets Layer toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0464">CVE-2023-0464</a>

    <p>David Benjamin reported a flaw related to the verification of X.509
    certificate chains that include policy constraints, which may result
    in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0465">CVE-2023-0465</a>

    <p>David Benjamin reported that invalid certificate policies in leaf
    certificates are silently ignored. A malicious CA could take
    advantage of this flaw to deliberately assert invalid certificate
    policies in order to circumvent policy checking on the certificate
    altogether.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0466">CVE-2023-0466</a>

    <p>David Benjamin discovered that the implementation of the
    X509_VERIFY_PARAM_add0_policy() function does not enable the check
    which allows certificates with invalid or incorrect policies to pass
    the certificate verification (contrary to its documentation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2650">CVE-2023-2650</a>

    <p>It was discovered that processing malformed ASN.1 object identifiers
    or data may result in denial of service.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1.1.1n-0+deb11u5.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5417.data"
# $Id: $
