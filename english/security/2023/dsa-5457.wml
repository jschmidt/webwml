<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-37450">CVE-2023-37450</a>

    <p>An anonymous researcher discovered that processing web content may
    lead to arbitrary code execution. Apple is aware of a report that
    this issue may have been actively exploited.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 2.40.3-2~deb11u2.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 2.40.3-2~deb12u2.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5457.data"
# $Id: $
