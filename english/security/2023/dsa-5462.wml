<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy discovered that under specific microarchitectural
circumstances, a vector register in AMD <q>Zen 2</q> CPUs may not be
written to 0 correctly.  This flaw allows an attacker to leak
sensitive information across concurrent processes, hyper threads
and virtualized guests.</p>

<p>For details please refer to
<a href="https://lock.cmpxchg8b.com/zenbleed.html">https://lock.cmpxchg8b.com/zenbleed.html</a> and
<a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">
https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a></p>

<p>This issue can also be mitigated by a microcode update through the
amd64-microcode package or a system firmware (BIOS/UEFI) update.
However, the initial microcode release by AMD only provides
updates for second generation EPYC CPUs.  Various Ryzen CPUs are
also affected, but no updates are available yet.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 6.1.38-2.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5462.data"
# $Id: $
