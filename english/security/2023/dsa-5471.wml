<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability has been discovered in libhtmlcleaner-java, a Java
HTML parser library. An attacker was able to cause a denial of service
(StackOverflowError) if the parser runs on user supplied input with deeply
nested HTML elements. This update introduces a new nesting depth limit which
can be overridden in cleaner properties.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 2.24-1+deb11u1.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 2.26-1+deb12u1.</p>

<p>We recommend that you upgrade your libhtmlcleaner-java packages.</p>

<p>For the detailed security status of libhtmlcleaner-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libhtmlcleaner-java">\
https://security-tracker.debian.org/tracker/libhtmlcleaner-java</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5471.data"
# $Id: $
