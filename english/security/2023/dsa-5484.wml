<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Zac Sims discovered a directory traversal in the URL decoder of librsvg,
a SAX-based renderer library for SVG files, which could result in read
of arbitrary files when processing a specially crafted SVG file with an
XInclude element.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 2.50.3+dfsg-1+deb11u1.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 2.54.7+dfsg-1~deb12u1.</p>

<p>We recommend that you upgrade your librsvg packages.</p>

<p>For the detailed security status of librsvg please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/librsvg">\
https://security-tracker.debian.org/tracker/librsvg</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5484.data"
# $Id: $
