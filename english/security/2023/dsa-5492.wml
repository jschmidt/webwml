<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1206">CVE-2023-1206</a>

    <p>It was discovered that the networking stack permits attackers to
    force hash collisions in the IPv6 connection lookup table, which may
    result in denial of service (significant increase in the cost of
    lookups, increased CPU utilization).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1989">CVE-2023-1989</a>

    <p>Zheng Wang reported a race condition in the btsdio Bluetooth adapter
    driver that can lead to a use-after-free. An attacker able to insert
    and remove SDIO devices can use this to cause a denial of service
    (crash or memory corruption) or possibly to run arbitrary code in
    the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2430">CVE-2023-2430</a>

    <p>Xingyuan Mo discovered that the io_uring subsystem did not properly
    handle locking when the target ring is configured with IOPOLL, which
    may result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2898">CVE-2023-2898</a>

    <p>It was discovered that missing sanitising in the f2fs file
    system may result in denial of service if a malformed file
    system is accessed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3611">CVE-2023-3611</a>

    <p>It was discovered that an out-of-bounds write in the traffic control
    subsystem for the Quick Fair Queueing scheduler (QFQ) may result in
    denial of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3772">CVE-2023-3772</a>

    <p>Lin Ma discovered a NULL pointer dereference flaw in the XFRM
    subsystem which may result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3773">CVE-2023-3773</a>

    <p>Lin Ma discovered a flaw in the XFRM subsystem, which may result
    in denial of service for a user with the CAP_NET_ADMIN capability in
    any user or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3776">CVE-2023-3776</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2023-4128">CVE-2023-4128</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2023-4206">CVE-2023-4206</a>, 
    <a href="https://security-tracker.debian.org/tracker/CVE-2023-4207">CVE-2023-4207</a>, 
    <a href="https://security-tracker.debian.org/tracker/CVE-2023-4208">CVE-2023-4208</a>

    <p>It was discovered that a use-after-free in the cls_fw, cls_u32 and
    cls_route network classifiers may result in denial of service or
    potential local privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3777">CVE-2023-3777</a>

    <p>Kevin Rich discovered a use-after-free in Netfilter when flushing
    table rules, which may result in local privilege escalation for a
    user with the CAP_NET_ADMIN capability in any user or network
    namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3863">CVE-2023-3863</a>

    <p>It was discovered that a use-after-free in the NFC implementation
    may result in denial of service, an information leak or potential
    local privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4004">CVE-2023-4004</a>

    <p>It was discovered that a use-after-free in Netfilter's
    implementation of PIPAPO (PIle PAcket POlicies) may result in denial
    of service or potential local privilege escalation for a user with
    the CAP_NET_ADMIN capability in any user or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4015">CVE-2023-4015</a>

    <p>Kevin Rich discovered a use-after-free in Netfilter when handling
    bound chain deactivation in certain circumstances, may result in
    denial of service or potential local privilege escalation for a user
    with the CAP_NET_ADMIN capability in any user or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4132">CVE-2023-4132</a>

    <p>A use-after-free in the driver for Siano SMS1xxx based MDTV
    receivers may result in local denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4147">CVE-2023-4147</a>

    <p>Kevin Rich discovered a use-after-free in Netfilter when adding a
    rule with NFTA_RULE_CHAIN_ID, which may result in local privilege
    escalation for a user with the CAP_NET_ADMIN capability in any user
    or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4155">CVE-2023-4155</a>

    <p>Andy Nguyen discovered a flaw in the KVM subsystem allowing a KVM
    guest using EV-ES or SEV-SNP to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4194">CVE-2023-4194</a>

    <p>A type confusion in the implementation of TUN/TAP network devices
    may allow a local user to bypass network filters.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4273">CVE-2023-4273</a>

    <p>Maxim Suhanov discovered a stack overflow in the exFAT driver, which
    may result in local denial of service via a malformed file system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4569">CVE-2023-4569</a>

    <p>lonial con discovered flaw in the Netfilter subsystem, which may
    allow a local attacker to cause a double-deactivations of catchall
    elements, which results in a memory leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4622">CVE-2023-4622</a>

    <p>Bing-Jhong Billy Jheng discovered a use-after-free within the Unix
    domain sockets component, which may result in local privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20588">CVE-2023-20588</a>

    <p>Jana Hofmann, Emanuele Vannacci, Cedric Fournet, Boris Koepf and
    Oleksii Oleksenko discovered that on some AMD CPUs with the Zen1
    micro architecture an integer division by zero may leave stale
    quotient data from a previous division, resulting in a potential
    leak of sensitive data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34319">CVE-2023-34319</a>

    <p>Ross Lagerwall discovered a buffer overrun in Xen's netback driver
    which may allow a Xen guest to cause denial of service to the
    virtualisation host my sending malformed packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40283">CVE-2023-40283</a>

    <p>A use-after-free was discovered in Bluetooth L2CAP socket handling.</p></li>

</ul>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 6.1.52-1. This update is released without armel builds. Changes
in the new stable series import cause a substantial increase of the
compressed image for marvell flavour. This issue will be addressed in a
future linux update.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5492.data"
# $Id: $
