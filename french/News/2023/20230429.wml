#use wml::debian::translation-check translation="b6a020c494e8e886f401ab63178faa0db93e39da" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.7</define-tag>
<define-tag release_date>2023-04-29</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la septième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction akregator "Correction de vérifications de sécurité, y compris la correction de suppression de flux et de dossiers">
<correction apache2 "Pas d'activation automatique d'apache2-doc.conf ; correction de régressions dans http2 et mod_rewrite introduites dans 2.4.56">
<correction at-spi2-core "Réglage du délai d'arrêt à 5 secondes, afin de ne pas bloquer inutilement l'arrêt du système">
<correction avahi "Correction d'un problème de déni de service local [CVE-2021-3468]">
<correction base-files "Mise à jour pour la version 11.7">
<correction c-ares "Dépassement de pile et déni de service évités [CVE-2022-4904]">
<correction clamav "Nouvelle version amont stable ; correction d'un problème potentiel d'exécution de code à distance dans l'analyseur de fichiers HFS+ [CVE-2023-20032], d'une possible fuite d'informations dans l'analyseur de fichiers DMG [CVE-2023-20052]">
<correction command-not-found "Ajout du nouveau composant non-free-firmware, corrigeant les mises à niveau vers Bookworm">
<correction containerd "Correction d'un problème de déni de service [CVE-2023-25153] ; correction d'une possible élévation de privilèges au moyen d'un réglage incorrect de groupes supplémentaires [CVE-2023-25173]">
<correction crun "Correction d'un problème d'élévation de capacités due au démarrage incorrect de conteneurs avec des permissions par défaut non vides [CVE-2022-27650]">
<correction cwltool "Ajout d'une dépendance manquante à python3-distutils">
<correction debian-archive-keyring "Ajout des clés de Bookworm ; déplacement des clés de Stretch dans le trousseau de clés retirées">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 5.10.0-22 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-ports-archive-keyring "Extension d'un an du délai d'expiration de la clé de signature 2023 ; ajout de la clé de signature 2024 ; déplacement de la clé de signature 2022 dans le trousseau de clés retirées">
<correction dpdk "Nouvelle version amont stable">
<correction duktape "Correction d'un problème de plantage [CVE-2021-46322]">
<correction e2tools "Correction d'un échec de construction en ajoutant une dépendance de construction à e2fsprogs">
<correction erlang "Correction d'un problème de contournement d'authentification du client [CVE-2022-37026] ; utilisation de l'optimisation -O1 pour armel parce que -O2 produit une erreur de segmentation d'erl sur certaines plateformes, par exemple Marvell">
<correction exiv2 "Corrections de sécurité [CVE-2021-29458 CVE-2021-29463 CVE-2021-29464 CVE-2021-29470 CVE-2021-29473 CVE-2021-29623 CVE-2021-32815 CVE-2021-34334 CVE-2021-34335 CVE-2021-3482 CVE-2021-37615 CVE-2021-37616 CVE-2021-37618 CVE-2021-37619 CVE-2021-37620 CVE-2021-37621 CVE-2021-37622 CVE-2021-37623]">
<correction flask-security "Correction d'une vulnérabilité de redirection ouverte [CVE-2021-23385]">
<correction flatpak "Nouvelle version amont stable ; protection des caractères spéciaux lors de l'affichage des permissions et des métadonnées [CVE-2023-28101] ; pas de copier-coller au moyen de l'ioctl TIOCLINUX permis lors de l'exécution dans une console virtuelle Linux [CVE-2023-28100]">
<correction galera-3 "Nouvelle version amont stable">
<correction ghostscript "Correction du chemin du fichier d'assistance PostScript dans ps2epsi">
<correction glibc "Correction d'une fuite de mémoire dans les fonctions de la famille de printf avec des chaînes longues multi-octets ; correction d'un plantage dans cette famille de fonctions dû à des allocations dépendant de la taille et de la précision ; correction d'erreur de segmentation dans printf gérant des milliers de séparateurs ; correction de dépassement dans l'implémentation de AVX2 de wcsnlen avec des pages croisées">
<correction golang-github-containers-common "Correction de l'analyse de DBUS_SESSION_BUS_ADDRESS">
<correction golang-github-containers-psgo "Pas d'entrée dans l'espace de noms utilisateur du processus [CVE-2022-1227]">
<correction golang-github-containers-storage "Fonctions précédemment internes rendues publiquement accessibles, ce qui est nécessaire pour permettre la correction du CVE-2022-1227 dans d'autres paquets">
<correction golang-github-prometheus-exporter-toolkit "Correction des tests pour éviter une situation de compétition ; correction d'un problème d'empoisonnement du cache d'authentification [CVE-2022-46146]">
<correction grep "Correction d'une correspondance incorrecte quand le dernier d'une série de motifs inclut une référence arrière">
<correction gtk+3.0 "Correction de l'association Wayland + EGL sur les plateformes uniquement GLES">
<correction guix "Correction d'un échec de construction dû à l'utilisation de clés expirées dans la suite de tests">
<correction intel-microcode "Nouvelle version amont de correction de bogues">
<correction isc-dhcp "Correction de la gestion de la durée de vie des adresses IPv6">
<correction jersey1 "Correction d'un échec de construction avec libjettison-java 1.5.3">
<correction joblib "Correction d'un problème d'exécution de code arbitraire [CVE-2022-21797]">
<correction lemonldap-ng "Correction d'un problème de contournement de validation d'URL ; correction d'un problème d'authentification à deux facteurs lors de l'utilisation du gestionnaire AuthBasic [CVE-2023-28862]">
<correction libapache2-mod-auth-openidc "Correction d'un problème de redirection ouverte [CVE-2022-23527]">
<correction libapreq2 "Correction d'un problème de dépassement de tampon [CVE-2022-22728]">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libexplain "Amélioration de la compatibilité avec les dernières versions du noyau - Linux 5.11 n'a plus l'en-tête if_frad.h, termiox supprimé depuis le noyau 5.12">
<correction libgit2 "Activation de la vérification de clé SSH par défaut [CVE-2023-22742]">
<correction libpod "Correction d'un problème d'élévation de privilèges [CVE-2022-1227] ; correction d'un problème d'élévation de capacité due au démarrage incorrect de conteneurs avec des permissions par défaut non vides [CVE-2022-27649] ; correction de l'analyse de DBUS_SESSION_BUS_ADDRESS">
<correction libreoffice "Changement de la monnaie par défaut de la Croatie pour l'Euro ; -Djava.class.path= vide évitée [CVE-2022-38745]">
<correction libvirt "Correction de problèmes liés au redémarrage de conteneurs ; correction d'échecs de tests combinés avec les nouvelles versions de Xen">
<correction libxpm "Correction de problèmes de boucle infinie [CVE-2022-44617 CVE-2022-46285] ; correction d'un problème de double libération de mémoire dans le code de gestion d'erreur ; correction de <q>les commandes de compression dépendent de PATH</q> [CVE-2022-4883]">
<correction libzen "Correction d'un problème de déréférencement de pointeur NULL [CVE-2020-36646]">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 22 ; [rt] mise à jour vers la version 5.10.176-rt86">
<correction linux-signed-amd64 "Nouvelle version amont stable ; passage de l'ABI à la version 22 ; [rt] mise à jour vers la version 5.10.176-rt86">
<correction linux-signed-arm64 "Nouvelle version amont stable ; passage de l'ABI à la version 22 ; [rt] mise à jour vers la version 5.10.176-rt86">
<correction linux-signed-i386 "Nouvelle version amont stable ; passage de l'ABI à la version 22 ; [rt] mise à jour vers la version 5.10.176-rt86">
<correction lxc "Correction de la présomption d'existence d'un fichier [CVE-2022-47952]">
<correction macromoleculebuilder "Correction d'un échec de construction en ajoutant une dépendance de construction à docbook-xsl">
<correction mariadb-10.5 "Nouvelle version amont stable ; suppression de la modification amont de l'API libmariadb">
<correction mono "Retrait du fichier de bureau">
<correction ncurses "Mise en garde contre la corruption de données de terminfo [CVE-2022-29458] ; correction du plantage de tic sur les clauses tc/use très longues">
<correction needrestart "Correction des avertissements lors de l'utilisation de l'option <q>-b</q>">
<correction node-cookiejar "Mise en garde contre les cookies de taille malveillante [CVE-2022-25901]">
<correction node-webpack "Accès à des objets interdomaines évité [CVE-2023-28154]">
<correction nvidia-graphics-drivers "Nouvelle version amont ; corrections de sécurité [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-450 "Nouvelle version amont ; corrections de sécurité [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-470 "Nouvelle version amont ; corrections de sécurité [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-modprobe "Nouvelle version amont">
<correction openvswitch "Correction de <q>la mise à jour d'openvswitch-switch laisse les interfaces arrêtées</q>">
<correction passenger "Correction de la compatibilité avec les versions plus récentes de NodeJS">
<correction phyx "Retrait d'une dépendance de construction à libatlas-cpp non nécessaire">
<correction postfix "Nouvelle version amont stable">
<correction postgis "Correction du mauvais ordre des axes de projections polaires stéréographiques">
<correction postgresql-13 "Nouvelle version amont stable ; correction d'un problème de divulgation de la mémoire du client [CVE-2022-41862]">
<correction python-acme "Correction de la version des CSR créées, pour éviter des problèmes avec les implémentations de l'API ACME respectant strictement les RFC">
<correction ruby-aws-sdk-core "Correction de la création du fichier de version">
<correction ruby-cfpropertylist "Correction de certaines fonctionnalités en abandonnant la compatibilité avec Ruby 1.8">
<correction shim "Nouvelle version amont ; nouvelle version amont stable ; activation de la prise en charge de NX au moment de la construction ; blocage des binaires de grub de Debian avec sbat &lt; 4">
<correction shim-helpers-amd64-signed "Nouvelle version amont stable ; activation de la prise en charge de NX au moment de la construction ; blocage des binaires de grub de Debian avec sbat &lt; 4">
<correction shim-helpers-arm64-signed "Nouvelle version amont stable ; activation de la prise en charge de NX au moment de la construction ; blocage des binaires de grub de Debian avec sbat &lt; 4">
<correction shim-helpers-i386-signed "Nouvelle version amont stable ; activation de la prise en charge de NX au moment de la construction ; blocage des binaires de grub de Debian avec sbat &lt; 4">
<correction shim-signed "Nouvelle version amont stable ; activation de la prise en charge de NX au moment de la construction ; blocage des binaires de grub de Debian avec sbat &lt; 4">
<correction snakeyaml "Correction de problèmes de déni de service [CVE-2022-25857 CVE-2022-38749 CVE-2022-38750 CVE-2022-38751] ; ajout de documentation concernant la prise en charge et les problèmes de sécurité">
<correction spyder "Correction de la duplication de code lors de l'enregistrement">
<correction symfony "Retrait des en-têtes privés avant le stockage de réponses avec HttpCache [CVE-2022-24894] ; suppression des jetons CSRF du stockage en cas de connexion réussie [CVE-2022-24895]">
<correction systemd "Correction d'une fuite d'informations [CVE-2022-4415], d'un problème de déni de service [CVE-2022-3821] ; ata_id : correction de l'obtention du code de réponse à partir des <q>SCSI Sense Data</q> ; logind : correction de l'obtention de la propriété OnExternalPower au moyen de D-Bus ; correction d'un plantage dans systemd-machined">
<correction tomcat9 "Ajout de la prise en charge d'OpenJDK 17 à la détection de JDK">
<correction traceroute "Interprétation des adresses v4mapped-IPv6 comme des adresses IPv4">
<correction tzdata "Mise à jour des données incluses">
<correction unbound "Correction d'une <q>Non-Responsive Delegation Attack</q> [CVE-2022-3204] ; correction d'un problème de <q>noms de domaine fantômes</q> [CVE-2022-30698 CVE-2022-30699]">
<correction usb.ids "Mise à jour des données incluses">
<correction vagrant "Ajout de la prise en charge de VirtualBox 7.0">
<correction voms-api-java "Correction d'échecs de construction en désactivant certains tests non fonctionnels">
<correction w3m "Correction d'un problème d'écriture hors limites [CVE-2022-38223]">
<correction x4d-icons "Correction d'un  échec de construction avec les nouvelles versions d'imagemagick">
<correction xapian-core "Corruption de la base de données évitée lors d'une saturation du disque">
<correction zfs-linux "Ajout de plusieurs améliorations de stabilité">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2022 5170 nodejs>
<dsa 2022 5237 firefox-esr>
<dsa 2022 5238 thunderbird>
<dsa 2022 5259 firefox-esr>
<dsa 2022 5262 thunderbird>
<dsa 2022 5282 firefox-esr>
<dsa 2022 5284 thunderbird>
<dsa 2022 5300 pngcheck>
<dsa 2022 5301 firefox-esr>
<dsa 2022 5302 chromium>
<dsa 2022 5303 thunderbird>
<dsa 2022 5304 xorg-server>
<dsa 2022 5305 libksba>
<dsa 2022 5306 gerbv>
<dsa 2022 5307 libcommons-net-java>
<dsa 2022 5308 webkit2gtk>
<dsa 2022 5309 wpewebkit>
<dsa 2022 5310 ruby-image-processing>
<dsa 2023 5311 trafficserver>
<dsa 2023 5312 libjettison-java>
<dsa 2023 5313 hsqldb>
<dsa 2023 5314 emacs>
<dsa 2023 5315 libxstream-java>
<dsa 2023 5316 netty>
<dsa 2023 5317 chromium>
<dsa 2023 5318 lava>
<dsa 2023 5319 openvswitch>
<dsa 2023 5320 tor>
<dsa 2023 5321 sudo>
<dsa 2023 5322 firefox-esr>
<dsa 2023 5323 libitext5-java>
<dsa 2023 5324 linux-signed-amd64>
<dsa 2023 5324 linux-signed-arm64>
<dsa 2023 5324 linux-signed-i386>
<dsa 2023 5324 linux>
<dsa 2023 5325 spip>
<dsa 2023 5326 nodejs>
<dsa 2023 5327 swift>
<dsa 2023 5328 chromium>
<dsa 2023 5329 bind9>
<dsa 2023 5330 curl>
<dsa 2023 5331 openjdk-11>
<dsa 2023 5332 git>
<dsa 2023 5333 tiff>
<dsa 2023 5334 varnish>
<dsa 2023 5335 openjdk-17>
<dsa 2023 5336 glance>
<dsa 2023 5337 nova>
<dsa 2023 5338 cinder>
<dsa 2023 5339 libhtml-stripscripts-perl>
<dsa 2023 5340 webkit2gtk>
<dsa 2023 5341 wpewebkit>
<dsa 2023 5342 xorg-server>
<dsa 2023 5343 openssl>
<dsa 2023 5344 heimdal>
<dsa 2023 5345 chromium>
<dsa 2023 5346 libde265>
<dsa 2023 5347 imagemagick>
<dsa 2023 5348 haproxy>
<dsa 2023 5349 gnutls28>
<dsa 2023 5350 firefox-esr>
<dsa 2023 5351 webkit2gtk>
<dsa 2023 5352 wpewebkit>
<dsa 2023 5353 nss>
<dsa 2023 5355 thunderbird>
<dsa 2023 5356 sox>
<dsa 2023 5357 git>
<dsa 2023 5358 asterisk>
<dsa 2023 5359 chromium>
<dsa 2023 5361 tiff>
<dsa 2023 5362 frr>
<dsa 2023 5363 php7.4>
<dsa 2023 5364 apr-util>
<dsa 2023 5365 curl>
<dsa 2023 5366 multipath-tools>
<dsa 2023 5367 spip>
<dsa 2023 5368 libreswan>
<dsa 2023 5369 syslog-ng>
<dsa 2023 5370 apr>
<dsa 2023 5371 chromium>
<dsa 2023 5372 rails>
<dsa 2023 5373 node-sqlite3>
<dsa 2023 5374 firefox-esr>
<dsa 2023 5375 thunderbird>
<dsa 2023 5376 apache2>
<dsa 2023 5377 chromium>
<dsa 2023 5378 xen>
<dsa 2023 5379 dino-im>
<dsa 2023 5380 xorg-server>
<dsa 2023 5381 tomcat9>
<dsa 2023 5382 cairosvg>
<dsa 2023 5383 ghostscript>
<dsa 2023 5384 openimageio>
<dsa 2023 5385 firefox-esr>
<dsa 2023 5386 chromium>
<dsa 2023 5387 openvswitch>
<dsa 2023 5388 haproxy>
<dsa 2023 5389 rails>
<dsa 2023 5390 chromium>
<dsa 2023 5391 libxml2>
<dsa 2023 5392 thunderbird>
<dsa 2023 5393 chromium>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction bind-dyndb-ldap "Cassé avec les nouvelles versions de bind9 ; pas de prise en charge possible dans stable">
<correction matrix-mirage "Dépend de python-matrix-nio qui doit être retiré">
<correction pantalaimon "Dépend de python-matrix-nio qui doit être retiré">
<correction python-matrix-nio "Problèmes de sécurité ; ne fonctionne pas avec les serveurs Matrix actuels">
<correction weechat-matrix "Dépend de python-matrix-nio qui doit être retiré">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
