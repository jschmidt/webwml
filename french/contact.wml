#use wml::debian::template title="Comment nous contacter" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9" maintainer="Jean-Pierre Giraud"
<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

# Translators:
# Christophe Le Bars, 1998-2000.
# Norbert Bottlaender-Prier, 2001-2003.
# Frédéric Bothamy, 2006, 2007.
# Simon Paillard, 2008.
# David Prévot, 2011, 2013.
# Jean-Pierre Giraud 2018-2022.

<ul class="toc">
  <li><a href="#generalinfo">Informations d'ordre général</a>
  <li><a href="#installuse">Installer et utiliser Debian</a>
  <li><a href="#press">Publications et presse</a>
  <li><a href="#events">Événements et conférences</a>
  <li><a href="#helping">Aider Debian</a>
  <li><a href="#packageproblems">Signaler des problèmes dans les paquets Debian</a>
  <li><a href="#development">Développement Debian</a>
  <li><a href="#infrastructure">Problèmes avec l'infrastructure Debian</a>
  <li><a href="#harassment">Problèmes de harcèlement</a>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Nous vous demandons de bien vouloir formuler vos requêtes initiales en <strong>anglais</strong>, dans la mesure où c'est la langue que la plupart d'entre nous parle. Si cela n'est pas possible, demandez de l'assistance sur une des <a href="https://lists.debian.org/users.html#debian-user">listes de diffusion des utilisateurs</a> qui sont disponibles dans de nombreuses langues différentes.</p>
</aside>

<p>
Debian est une vaste organisation, et il existe de nombreux moyens pour
contacter les membres du projet et les autres utilisateurs de Debian. Cette page
résume les moyens les plus souvent utilisés pour contacter Debian ; elle ne
prétend pas être exhaustive. Veuillez vous référer aux autres pages web pour
trouver des moyens de contact supplémentaires.
</p>

<p>
Veuillez remarquer que la plupart des adresses électroniques suivantes renvoient
vers des listes de diffusion ouvertes avec archivage public, aussi veuillez
consulter l'<a href="$(HOME)/MailingLists/disclaimer">avertissement</a> avant
d'envoyer un message.
</p>

<h2 id="generalinfo">Informations générales</h2>

<p>Vous pourrez trouver la plupart des informations sur Debian sur notre site
web <a href="$(HOME)">https://www.debian.org/</a>, veuillez donc naviguer sur
ce site ou y <a href="$(SEARCH)">effectuer une recherche</a> avant de nous
contacter.</p>

<p>
Vous pouvez envoyer les questions (en anglais) concernant le Projet Debian 
à la liste de diffusion <email debian-project@lists.debian.org>. N'envoyez pas
de questions générales sur Linux à cette liste ; pour plus d'informations sur
d'autres listes de diffusion, lisez plutôt ce qui suit.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">Lisez notre Foire aux Questions</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Installer et utiliser Debian</h2>

<p>
Si vous êtes certain que la solution à votre problème ne se trouve ni sur
le support d'installation, ni sur notre site web, vous pouvez vous adresser à
la liste de diffusion des utilisateurs très active, où ceux-ci et également
les développeurs de Debian pourront répondre à vos questions. Là, vous pouvez
envoyer toute question concernant :
</p>

<ul>
  <li>l'installation,</li>
  <li>la configuration,</li>
  <li>le matériel supporté,</li>
  <li>l'administration,</li>
  <li>l'utilisation de Debian.</li>
</ul>

<p>
<a href="https://lists.debian.org/debian-user/">Abonnez-vous</a>
à la liste de diffusion et envoyez vos questions à
<email debian-user@lists.debian.org> (liste en anglais) ou à
<email debian-user-french@lists.debian.org> (liste en français).
</p>

<p>
De plus, il existe des listes de diffusion dans d'autres langues. Vous en
trouverez un répertoire sur
<a href="https://lists.debian.org/users.html#debian-user">la page d'abonnement</a>.
</p>

<p>
Vous pouvez explorer les <a href="https://lists.debian.org/">archives des listes
de diffusion</a> ou <a href="https://lists.debian.org/search.html">effectuer
des recherches sur ces archives</a> sans qu'il soit nécessaire de vous abonner.
</p>

<p>
Vous pouvez également lire nos listes de diffusion comme des forums.
</p>

<p>
Si vous pensez avoir trouvé un bogue dans notre système d'installation,
informez-en <email debian-boot@lists.debian.org>. Vous pouvez aussi soumettre
<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">un rapport de bogue</a>
concernant le pseudopaquet
<a href="https://bugs.debian.org/debian-installer">debian-installer</a>.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">Publications et presse</h2>

<p>
L'<a href="https://wiki.debian.org/Teams/Publicity">équipe de publicité Debian</a>
modère les nouvelles et les annonces dans toutes les ressources officielles de
Debian, par exemple certaines listes de diffusion, le blog
<a href="https://bits.debian.org/">Bits from Debian</a>, le site des 
<a href="https://micronews.debian.org/">micro-nouvelles</a> et les canaux
officiels des médias sociaux.
</p>

<p>
Si vous écrivez sur Debian et avez besoin d'aide ou d'informations, contactez
l'<a href="mailto:press@debian.org">équipe de publicité</a>. C'est aussi la
bonne adresse si vous souhaitez envoyer des nouvelles pour nos pages
d'informations.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Événements et conférences</h2>

<p>Vos invitations pour les <a href="$(HOME)/events/">conférences</a> et
expositions ou d'autres événements doivent nous parvenir au
<a href="mailto:events@debian.org">département des événements</a>. 

<p>Envoyez vos requêtes pour recevoir des tracts et des affiches, ou pour des
participations en Europe à la
<a href="mailto:debian-events-eu@lists.debian.org">liste des événements européens</a>.
</p>

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Aider Debian</h2>

<p>Il y a de nombreuses possibilités pour soutenir le projet Debian et
contribuer à la distribution. Si vous voulez contacter Debian pour offrir
votre aide, veuillez d'abord consulter la page
<a href="devel/join/">Comment rejoindre Debian</a>.
</p>

<p>Si vous souhaitez entretenir un miroir Debian, consultez les
<a href="mirror/">pages sur la question</a>. Les nouveaux miroirs sont
enregistrés grâce à <a href="mirror/submit">ce formulaire</a>. Les problèmes
sur les miroirs existants peuvent être signalés à <email mirrors@debian.org>.
</p>

<p>Si vous désirez vendre des CD Debian, lisez les
<a href="CD/vendors/info">informations pour les vendeurs de CD</a>. Pour être
inscrit sur la liste des vendeurs de CD, vous pouvez le faire en complétant ce
<a href="CD/vendors/adding-form">formulaire</a>.
</p>

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">Signaler des problèmes dans des paquets Debian</h2>

<p>Si vous désirez signaler un bogue dans un paquet Debian, nous avons un
système de suivi des bogues que vous pouvez utiliser pour signaler votre
problème de manière simple. Veuillez lire les
<a href="Bugs/Reporting">instructions pour remplir un rapport de bogue</a>.</p>

<p>Si vous voulez seulement entrer en relation avec le responsable d'un paquet
Debian, vous pouvez utiliser les alias de courriel spéciaux configurés pour
chaque paquet. Tout message envoyé à &lt;<var>nom du paquet</var>&gt;@packages.debian.org
sera expédié au responsable de ce paquet.</p>

<p>Si vous désirez discrètement alerter les développeurs sur un problème de
sécurité de Debian, manifestez-vous à <email security@debian.org>.</p>


<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Développement de Debian</h2>

<p>Si votre question concerne plutôt le développement, il y a plusieurs
<a href="https://lists.debian.org/devel.html">listes de diffusion pour le développement</a>
où vous pouvez contacter nos développeurs.
</p>

<p>
La liste générale pour le développement est <email debian-devel@lists.debian.org>.
Vous pouvez vous y abonner en suivant ce <a href="https://lists.debian.org/debian-devel/">lien</a>.
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Problèmes avec l'infrastructure Debian</h2>

<p>Pour signaler un problème avec un service Debian, vous pouvez comme à
l'habitude <a href="Bugs/Reporting">signaler un bogue</a> concernant le
<a href="Bugs/pseudo-packages">pseudopaquet</a> approprié.</p>

<p>Ou sinon, vous pouvez envoyer un courriel à l'une des adresses suivantes&nbsp;:</p>

<define-tag btsurl>paquet: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Éditeurs des pages web</dt>
  <dd><btsurl www.debian.org><br />
    <email debian-www@lists.debian.org>
  </dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Traducteurs des pages web</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>Administrateurs des listes de diffusion et responsables des archives</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org>
  </dd>
<dt>Administrateurs du système de suivi de bogues</dt>
  <dd><btsurl bugs.debian.org><br />
      <email owner@bugs.debian.org>
  </dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>

<h2 id="harassment">Problèmes de harcèlement</h2>
<p>
Debian est une communauté de personnes qui attachent une grande importance au
respect. Si vous êtes victime d’un comportement inapproprié ou blessant, ou si
vous vous sentez harcelé, veuillez contacter l'équipe en charge de la communauté :
<email community@debian.org>.
</p>
