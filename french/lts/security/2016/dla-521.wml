#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla : plusieurs erreurs de sécurité de la mémoire,
dépassements de tampon et d'autres erreurs d'implémentation pourraient
conduire à l'exécution de code arbitraire ou à un problème d'usurpation.</p>

<p>Attendez, Firefox ? Plus de références à Iceweasel ? C'est exact, Debian
n'emploie plus de marque personnalisée. Veuillez consulter ces liens pour
plus d'information :</p>
<ul>
<li><a href="https://glandium.org/blog/?p622">https://glandium.org/blog/?p622</a></li>
<li><a href="https://en.wikipedia.org/wiki/Mozilla_software_rebranded_by_Debian">https://en.wikipedia.org/wiki/Mozilla_software_rebranded_by_Debian</a></li>
</ul>

<p>Debian suit les éditions longue durée (« extended support releases »
— ESR) de Firefox. Le suivi des séries 38.x est terminé, aussi, à partir de
cette mise à jour, Debian suit les versions 45.x. Cette mise à jour vers la
nouvelle version ESR marque aussi le moment où Debian emploie de nouveau la
marque originale.</p>

<p>Des paquets de transition pour Iceweasel sont fournis et ils assurent la
mise à niveau vers la nouvelle version. Dans la mesure où de nouveaux
paquets binaires doivent être installés, assurez-vous de l'autoriser lors
de la procédure de mise à niveau automatique (par exemple en utilisant la
commande « apt-get dist-upgrade » à la place de « apt-get upgrade »).</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 45.2.0esr-1~deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-521.data"
# $Id: $
