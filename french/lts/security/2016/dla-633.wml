#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans WordPress, un outil de
blog. Le projet « Common Vulnerabilities and Exposures » (CVE) identifie
les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8834">CVE-2015-8834</a>

<p>Une vulnérabilité de script intersite (XSS) dans wp-includes/wp-db.php
dans WordPress avant la version 4.2.2 permet à des attaquants distants
d'injecter un script web arbitraire ou du HTML à l'aide d'un long
commentaire qui est stocké incorrectement à cause de limitations du type de
données TEXT de MySQL TEXT. NOTE : Cette vulnérabilité existe du fait d'une
correction incomplète du
<a href="https://security-tracker.debian.org/tracker/CVE-2015-3440">CVE-2015-3440</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4029">CVE-2016-4029</a>

<p>WordPress avant la version 4.5 ne prend pas en considération les formats
octal et hexadécimal d'adresse IP lors de la détermination d'une adresse
intranet. Cela permet à des attaquants distants de contourner un mécanisme
voulu de protection contre une vulnérabilité SSRF (Server Side Request
Forgery) à l'aide d'une adresse contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5836">CVE-2016-5836</a>

<p>L'implémentation du protocole oEmbed dans WordPress avant la
version 4.5.3 permet à des attaquants distants de provoquer un déni de
service à l'aide de vecteurs non précisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6634">CVE-2016-6634</a>

<p>Une vulnérabilité de script intersite (XSS) dans la page de
configuration de réseau dans WordPress avant la version 4.5 permet à des
attaquants distants d'injecter un script web arbitraire ou du HTML à l'aide
de vecteurs non précisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6635">CVE-2016-6635</a>

<p>Un vulnérabilité de contrefaçon de requête intersite (CSRF) dans la
fonction wp_ajax_wp_compression_test dans wp-admin/includes/ajaxactions.php
dans WordPress avant la version 4.5 permet à des attaquants distants le
détournement de l'authentification des administrateurs pour des requêtes
qui modifient l'option de compression de script.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7168">CVE-2016-7168</a>

<p>Correction d'une vulnérabilité de script intersite à l'aide d'un nom de
fichier d'image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7169">CVE-2016-7169</a>

<p>Correction d'une vulnérabilité de traversée de répertoires dans le
chargeur de paquet de mise à niveau.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.6.1+dfsg-1~deb7u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-633.data"
# $Id: $
