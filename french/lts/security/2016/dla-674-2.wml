#use wml::debian::translation-check translation="c8457a59a89d51637f9c29eabc2b64c8a52130b6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>La mise à jour pour ghostscript publiée avec la DLA-674-1 provoquait des
régressions pour certains lecteurs de documents Postscript (evince,
zathura). Des paquets mis à jour sont maintenant disponibles pour traiter
ce problème. Pour référence, le texte de l'annonce d'origine suit.</p>

<p>Plusieurs vulnérabilités ont été découvertes dans Ghostscript,
l'interpréteur PostScript et PDF sous GPL. Cela peut conduire à l'exécution
de code arbitraire ou à la divulgation d'informations lors du traitement
d'un fichier Postscript contrefait pour l'occasion.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 9.05~dfsg-6.3+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ghostscript.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-674-2.data"
# $Id: $
