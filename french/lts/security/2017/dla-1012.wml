#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les versions de Puppet avant 4.10.1 désérialisent les données provenant du
réseau (de l’agent vers le serveur, dans ce cas) avec un format précisé par
l’attaquant. Cela pourrait être utilisé pour obliger la désérialisation de YAML
d’une manière non sûre, ce qui pourrait conduire à une exécution de code
à distance.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.7.23-1~deb7u4, en activant la sérialisation PSON sur les clients et
en refusant les formats non PSON sur le serveur.</p>

<p>Nous recommandons de mettre à niveau vos paquets puppet. Soyez sûr de mettre
à jour tous les clients avant de mettre à jour le serveur, autrement les vieux
clients ne pourront se connecter au serveur.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1012.data"
# $Id: $
