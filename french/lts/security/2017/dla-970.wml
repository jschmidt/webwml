#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>L'équipe Qualys Security a découvert que sudo, un programme conçu pour
donner des droits limités de superutilisateur à des utilisateurs
particuliers, n'analysait pas correctement « /proc/[pid]/stat » pour lire
le numéro de périphérique du tty à partir du champ 7 (tty_nr). Un
utilisateur de sudoers peut tirer avantage de ce défaut sur un système avec
SELinux activé pour obtenir les droits complets du superutilisateur.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans
la version 1.8.5p2-1+nmu3+deb7u3.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.8.10p3-1+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-970.data"
# $Id: $
