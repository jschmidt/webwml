#use wml::debian::translation-check translation="cb6bc3d73ebe6c8ee975e84a5de151bbec375689" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans wordpress, un outil de blog
web. Le projet « Common vulnérabilités et Exposures » (CVE) identifie les
problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8295">CVE-2017-8295</a>

<p>Vulnérabilité potentielle de réinitialisation non autorisée de mot de passe.
Plus d’information est disponible sur :</p>

<p><url "https://exploitbox.io/vuln/WordPress-Exploit-4-7-Unauth-Password-Reset-0day-CVE-2017-8295.html"></p>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9061">CVE-2017-9061</a>

<p>Une vulnérabilité de script intersite (XSS) existe quand quelqu’un essaie de
téléverser de très grands fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9062">CVE-2017-9062</a>

<p>Traitement incorrect de valeurs de métadonnées post dans l’API XML-RPC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9063">CVE-2017-9063</a>

<p>Vulnérabilité de script intersite (XSS) dans le traitement de la personnalisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9064">CVE-2017-9064</a>

<p>Une vulnérabilité de contrefaçon de requête intersite (CSRF) existe dans le
dialogue d’accréditations du système de fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9065">CVE-2017-9065</a>

<p>Absence de vérifications de capacité des métadonnées post dans l’API XML-RPC.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.6.1+dfsg-1~deb7u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-975.data"
# $Id: $
