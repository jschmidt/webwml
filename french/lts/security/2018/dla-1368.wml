#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>De graves vulnérabilités ont été trouvés dans la bibliothèque libvorbis,
utilisée couramment pour encoder et décoder l’audio dans des conteneurs OGG.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14633">CVE-2017-14633</a>

<p>Dans libvorbis 1.3.5 de Xiph.Org, une vulnérabilité de lecture hors limites
de tableau existe dans la fonction mapping0_forward() dans mapping0.c, qui
pourrait conduire à un déni de service lors du traitement d’un fichier audio
contrefait avec vorbis_analysis().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14632">CVE-2017-14632</a>

<p>libvorbis 1.3.5 de Xiph.Org permet l’exécution de code à distance lors de la
libération de mémoire non initialisée dans la fonction vorbis_analysis_headerout()
dans info.c quand vi->channels<=0, un problème similaire au bogue n° 550184 de
Mozilla.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11333">CVE-2017-11333</a>

<p>La fonction vorbis_analysis_wrote dans lib/block.c dans libvorbis 1.3.5 de
Xiph.Org permet à des attaquants distants de provoquer un déni de service (OOM)
à l'aide d'un fichier wav contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5146">CVE-2018-5146</a>

<p>Une écriture hors limites de mémoire dans le code d’analyse de codebook de
la bibliothèque multimédia Libvorbis pourrait aboutir à l'exécution de code
arbitraire.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.3.2-1.3+deb7u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libvorbis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1368.data"
# $Id: $
