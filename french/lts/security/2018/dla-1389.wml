#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le serveur HTTPD Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15710">CVE-2017-15710</a>

<p>Alex Nichols et Jakob Hirsch ont signalé que mod_authnz_ldap, s'il est
configuré avec l'option AuthLDAPCharsetConfig, pourrait provoquer une
lecture hors limite si un en-tête Accept-Language contrefait est fourni.
Cela pourrait éventuellement être utilisé pour une attaque par déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1301">CVE-2018-1301</a>

<p>Robert Swiecki a signalé qu'une requête contrefaite pour l'occasion
pourrait faire planter le serveur HTTP Apache, du fait d'un accès hors
limite après que la limite de taille a été atteinte en lisant l'en-tête
HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1312">CVE-2018-1312</a>

<p>Nicolas Daniels a découvert que, lors de la création d'un défi
d'authentification par Digest HTTP, le nonce envoyé par mod_auth_digest pour
empêcher les attaques par rejeu n'était pas correctement généré en utilisant
une graine pseudo-aléatoire. Dans une grappe de serveurs utilisant une
configuration d'authentification par Digest commune, les requêtes HTTP
pourraient être rejouées sans détection sur les différents serveurs.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.2.22-13+deb7u13.</p>
<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1389.data"
# $Id: $
