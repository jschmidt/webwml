#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans le moteur Tomcat
de servlet et de JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1336">CVE-2018-1336</a>

<p>Une gestion incorrecte de débordement dans le décodeur UTF-8 pour des
caractères supplémentaires peut conduire à une boucle infinie dans le décodeur
causant un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8034">CVE-2018-8034</a>

<p>La vérification du nom d’hôte lors de l’utilisation de TLS avec le client
WebSocket manquait. Elle est désormais activée par défaut.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 8.0.14-1+deb8u13.</p>
<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1491.data"
# $Id: $
