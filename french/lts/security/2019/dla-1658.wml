#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un couple de vulnérabilités a été découvert dans phpmyadmin, un outil
d’administration web pour MySQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19968">CVE-2018-19968</a>

<p>Un attaquant peut exploiter phpMyAdmin avant 4.8.4 pour divulguer le contenu
d’un fichier local à cause d’une erreur dans la fonction de transformation.
L’attaquant doit avoir accès aux tables de stockage de configuration de
phpMyAdmin, bien que celles-ci peuvent être facilement créées dans n’importe
quelle base de données accessibles par l’attaquant. Un attaquant doit avoir des
droits adéquats pour se connecter dans phpMyAdmin. Cette vulnérabilité ne permet
pas à un attaquant de contourner la procédure de connexion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19970">CVE-2018-19970</a>

<p>Une vulnérabilité de script intersite (XSS) a été découverte dans l’arbre de
navigation, où un attaquant peut fournir une charge à un utilisateur un nom
contrefait de base de données ou de table.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 4:4.2.12-2+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpmyadmin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1658.data"
# $Id: $
