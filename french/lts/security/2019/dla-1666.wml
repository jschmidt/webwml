#use wml::debian::translation-check translation="7e6b86e512d1e203a20619cdee7230be27c2eae5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Pour la version FreeRDP dans Debian Jessie LTS, une mise à jour de sécurité et
de fonctionnalité a été récemment fournie. FreeRDP est une réimplémentation du
protocole RDP de Microsoft (serveur et client) avec freerdp-x11 comme client
RDP le plus courant de nos jours.</p>

<p>Améliorations fonctionnelles :</p>

 <p>Avec l’aide de l’amont de FreeRDP (grand merci à Bernhard Miklautz et
 Martin Fleisz) nous sommes heureux d’annoncer que les prises en charge du
 protocole version 6 de RDP et de CredSSP version 3 ont été rétroportées dans
 la vieille branche FreeRDP 1.1.</p>

 <p>Depuis le deuxième trimestre de 2018, les serveurs et clients de Microsoft
 Windows reçoivent une mise à jour par défaut pour leur serveur RDP vers le
 protocole version 6. Depuis cette modification, il n’était plus possible de se
 connecter à des machines MS Windows mises à jour récemment en utilisant la
 vielle branche FreeRDP 1.1 comme trouvée dans Debian Jessie LTS et Debian
 Stretch.</p>

 <p>Avec le dernier téléversement de FreeRDP vers Debian Jessie LTS, se connecter
 à des machines MS Windows mises à jour est de nouveau possible.</p>

<p>Problèmes de sécurité :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8786">CVE-2018-8786</a>

 <p>FreeRDP contient une troncature d’entier conduisant à un dépassement
 de tampon basé sur le tas dans la fonction update_read_bitmap_update() et
 aboutissant à une corruption de mémoire et probablement même à une exécution de
 code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8787">CVE-2018-8787</a>

 <p>FreeRDP contient un dépassement d'entier conduisant à un dépassement de tampon
 basé sur le tas dans la fonction gdi_Bitmap_Decompress() et aboutissant à une
 corruption de mémoire et probablement même à une exécution de code à distance.
 </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8788">CVE-2018-8788</a>

 <p>FreeRDP contient une écriture hors limites de un à quatre octets dans la
 fonction nsc_rle_decode() aboutissant à une corruption de mémoire et
 éventuellement à une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8789">CVE-2018-8789</a>

 <p>FreeRDP contient plusieurs lectures hors limites dans le module
 d’authentification NTLM aboutissant à un déni de service (segfault).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes de sécurité ont été corrigés dans
la version 1.1.0~git20140921.1.440916e+dfsg1-13~deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freerdp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1666.data"
# $Id: $
