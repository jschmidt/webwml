#use wml::debian::translation-check translation="009930974fca60c0e0ce8720a502740e0e1b188e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été corrigés dans plusieurs
démultiplexeurs et décodeurs de la bibliothèque multimédia libav.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9987">CVE-2017-9987</a>

<p>Dans Libav existait un dépassement de tampon basé sur le tas dans la fonction
hpel_motion dans mpegvideo_motion.c. Une entrée contrefaite pouvait conduire à une
attaque distante par déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5766">CVE-2018-5766</a>

<p>Dans Libav existait un memcpy non valable dans la fonction av_paquet_ref de
libavcodec/avpaquet.c. Des attaquants distants pouvaient exploiter cette
vulnérabilité pour provoquer un déni de service (erreur de segmentation)
à l'aide d'un fichier avi contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11102">CVE-2018-11102</a>

<p>Une violation d’accès en lecture dans la fonction mov_probe dans
libavformat/mov.c permettait à des attaquants distants de provoquer un déni de
service (plantage d'application), comme démontré par avconv.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14372">CVE-2019-14372</a>

<p>Dans Libav existait une boucle infinie dans la fonction
wv_read_block_header() dans le fichier wvdec.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14442">CVE-2019-14442</a>

<p>Dans mpc8_read_header dans libavformat/mpc8.c, un fichier d’entrée pouvait
mener à une boucle infinie et un plantage d’avio_seek, avec 100 % d’utilisation
du CPU. Des attaquants pouvaient exploiter cette vulnérabilité pour provoquer un
déni de service à l'aide d'un fichier contrefait.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 6:11.12-1~deb8u8.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1907.data"
# $Id: $
