#use wml::debian::translation-check translation="599e1a56fc789fd66baadb8cd83c5cd31a915235" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10905">CVE-2016-10905</a>

<p>Une situation de compétition a été découverte dans l’implémentation du
système de fichiers GFS2 qui pourrait conduire à une utilisation de mémoire
après libération. Dans un système utilisant GFS2, un attaquant local pourrait
utiliser cela pour un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20976">CVE-2018-20976</a>

<p>L’implémentation du système de fichiers XFS ne gérait pas correctement
certaines conditions d’échec de montage. Cela pourrait conduire à une
utilisation de mémoire après libération. L’impact de sécurité n’est pas évident.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21008">CVE-2018-21008</a>

<p>Le pilote wifi rsi ne gérait pas correctement certaines conditions d’échec.
Cela pourrait conduire à utilisation de mémoire après libération. L’impact de
sécurité n’est pas évident.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0136">CVE-2019-0136</a>

<p>La mise en œuvre de soft-MAC (mac80211) wifi ne certifiait pas correctement
les messages TDLS (Tunneled Direct Link Setup). Un attaquant proche pourrait
utiliser cela pour provoquer un déni de service (perte de la connectivité wifi).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9506">CVE-2019-9506</a>

<p>Daniele Antonioli, Nils Ole Tippenhauer et Kasper Rasmussen ont découvert une
faiblesse dans le protocole d’appariement Bluetooth, appelée <q>attaque
KNOB</q>. Un attaquant proche lors de l’appariement pourrait utiliser cela pour
affaiblir le chiffrement utilisé entre les périphériques appariés et écouter ou
usurper la communication entre eux.</p>

<p>Cette mise à jour atténue l’attaque en requérant une longueur minimale de
56 bits pour la clef de chiffrement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14814">CVE-2019-14814</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14815">CVE-2019-14815</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14816">CVE-2019-14816</a>

<p>Plusieurs bogues ont été découverts dans le pilote wifi mwifiex qui pourraient
conduire à des dépassements de tampon basé sur le tas. Un utilisateur local,
autorisé à configurer un périphérique géré par ce pilote, pourrait probablement
utiliser cela pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14821">CVE-2019-14821</a>

<p>Matt Delco a signalé une situation de compétition dans la fonction Coalesced
MMIO de KVM qui pourrait conduire à un accès hors limites dans le noyau. Un
attaquant local autorisé à accéder à /dev/kvm pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une élévation de privilèges</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14835">CVE-2019-14835</a>

<p>Peter Pi de Tencent Blade Team a découvert une absence de vérification de
limites dans vhost_net, le pilote de dorsal de réseau pour les hôtes KVM, menant
à un dépassement de tampon quand l'hôte débute la migration en direct d'une
machine virtuelle. Un attaquant qui contrôle une machine virtuelle pourrait
utiliser cela pour provoquer un déni de service (corruption de mémoire ou
plantage) ou éventuellement pour une élévation de privilèges sur l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15117">CVE-2019-15117</a>

<p>Hui Peng et Mathias Payer ont signalé une absence de vérification de limites
dans le code d'analyse du descripteur du pilote usb-audio, menant à une lecture
hors limites de tampon. Un attaquant capable d'ajouter des périphériques USB
pourrait éventuellement utiliser cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15118">CVE-2019-15118</a>

<p>Hui Peng et Mathias Payer ont signalé une récursion illimitée dans le code
d'analyse du descripteur du pilote usb-audio, menant à un dépassement de pile.
Un attaquant capable d'ajouter des périphériques USB pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15211">CVE-2019-15211</a>

<p>L’outil syzkaller a trouvé un bogue dans le pilote radio-raremono qui
pourrait conduire à une utilisation de mémoire après libération. Un attaquant,
capable d’ajouter ou retirer des périphériques USB, pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15212">CVE-2019-15212</a>

<p>L’outil syzkaller a trouvé que le pilote rio500 ne fonctionnait pas
correctement si plus d’un appareil lui sont liés. Un attaquant, capable
d’ajouter des périphériques USB, pourrait utiliser cela pour provoquer un déni de
service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15215">CVE-2019-15215</a>

<p>L’outil syzkaller a trouvé un bogue dans le pilote cpia2_usb aboutissant
à une utilisation de mémoire après libération. Un attaquant, capable d’ajouter ou
de retirer des périphériques USB, pourrait utiliser cela pour provoquer un déni de
service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15218">CVE-2019-15218</a>

<p>L’outil syzkaller a trouvé que le pilote smsusb driver ne vérifiait pas que
les périphériques USB avaient les terminaisons attendues, conduisant
éventuellement à un déréférencement de pointeur NULL. Un attaquant, capable
d’ajouter des périphériques USB, pourrait utiliser cela pour provoquer un déni
de service (bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15219">CVE-2019-15219</a>

<p>L’outil syzkaller a trouvé qu’une erreur d’initialisation de périphérique
dans le pilote sisusbvga pourrait conduire à un déréférencement de pointeur
NULL. Un attaquant, capable d’ajouter des périphériques USB, pourrait utiliser
cela pour provoquer un déni de service (bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15220">CVE-2019-15220</a>

<p>L’outil syzkaller a trouvé une situation de compétition dans le pilote p54usb
qui pourrait conduire à une utilisation de mémoire après libération. Un
attaquant, capable d’ajouter ou de retirer des périphériques USB, pourrait
utiliser cela pour provoquer un déni de service (corruption de mémoire ou
plantage) ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15221">CVE-2019-15221</a>

<p>L’outil syzkaller a trouvé que le pilote line6 ne vérifiait pas les tailles
maximales de paquet des périphériques USB, ce qui pourrait conduire à un
dépassement de tampon basé sur le tas. Un attaquant, capable d’ajouter des
périphériques USB, pourrait utiliser cela pour provoquer un déni de service
(corruption de mémoire ou plantage) ou éventuellement pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15292">CVE-2019-15292</a>

<p>L’outil Hulk Robot a trouvé des vérifications d’erreurs manquantes dans
l’implémentation du protocole Appletalk, ce qui pourrait conduire à une
utilisation de mémoire après libération. L’impact de sécurité apparait peu
clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15807">CVE-2019-15807</a>

<p>Jian Luo a signalé que la bibliothèque Serial Attached SCSI (libsas) ne
gérait pas correctement l’échec de découverte de périphériques derrière un
adaptateur SAS. Cela pourrait conduire à une fuite de ressources et un plantage
(bogue). L’impact de sécurité apparait peu clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15917">CVE-2019-15917</a>

<p>L’outil syzkaller a trouvé une situation de compétition dans le code de prise
en charge des adaptateurs Bluetooth liés UART. Cela pourrait conduire à une
utilisation après libération. Un utilisateur local, avec accès à un périphérique
pty ou un autre périphérique tty approprié, pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15926">CVE-2019-15926</a>

<p>Le pilote ath6kl wifi ne vérifiait pas constamment les numéros de classe de
trafic dans les paquets de contrôle reçus, conduisant à un accès mémoire hors
limites. Un attaquant proche sur le même réseau wifi pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.74-1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1930.data"
# $Id: $
