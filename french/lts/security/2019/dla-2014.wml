#use wml::debian::translation-check translation="4911338e7e01a8f2d5955022cd05d8c9be428bc1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été identifiées dans le code VNC de vino, un
utilitaire de partage de bureau pour l’environnement de bureau GNOME.</p>

<p>Les vulnérabilités référencées ci-dessous sont des problèmes qui, à l’origine,
ont été signalés pour le paquet source libvncserver de Debian. Le paquet source
de vino dans Debian incorpore une variante dépouillée et avec une correction
personnalisée de libvncserver, par conséquent quelques correctifs de sécurité
pour libvncserver nécessitent plus de portage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6053">CVE-2014-6053</a>

<p>La fonction rfbProcessClientNormalMessage dans libvncserver/rfbserver.c dans
LibVNCServer ne gérait pas correctement les essais d’envoi de grandes quantités
de données ClientCutText. Cela permettait à des attaquants distants de provoquer
un déni de service (consommation de mémoire ou plantage du démon) à l'aide d'un
message contrefait qui était traité en utilisant un unique malloc non vérifié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7225">CVE-2018-7225</a>

<p>Un problème a été découvert dans LibVNCServer. La fonction
rfbProcessClientNormalMessage() dans rfbserver.c ne nettoyait pas msg.cct.length,
conduisant à un accès à des données non initialisées et éventuellement sensibles,
ou éventuellement à un autre impact non précisé (par exemple, un dépassement
d'entier) à l’aide de paquets VNC contrefaits pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15681">CVE-2019-15681</a>

<p>LibVNC contenait une fuite de mémoire (CWE-655) dans le code du serveur VNC
qui permettait à un attaquant de lire la mémoire de pile, et pourrait être mal
utilisée pour une divulgation d'informations. Combinée avec une autre
vulnérabilité, cela pourrait être utilisé pour divulguer de la mémoire de pile
et contourner ASLR. Cette attaque semble être exploitable à l’aide de la
connectivité réseau.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.14.0-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vino.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2014.data"
# $Id: $
