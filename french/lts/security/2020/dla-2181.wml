#use wml::debian::translation-check translation="3aa227bd632b51df6ef9eb82889e7b694b37c8e4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème de traversée de répertoires
dans Apache Shiro, un cadriciel de sécurité pour le langage de programmation Java.
Une requête contrefaite pour l'occasion pourrait causer un contournement
d’authentification.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1957">CVE-2020-1957</a>

<p>Dans Apache Shiro avant la version 1.5.2, lors de son utilisation avec des
contrôleurs dynamiques Spring, une requête contrefaite pour l'occasion pourrait
causer un contournement d’authentification.</p>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.2.3-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets shiro.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2181.data"
# $Id: $
