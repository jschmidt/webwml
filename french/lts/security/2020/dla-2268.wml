#use wml::debian::translation-check translation="044f0b378d1e97d512ed4133d9cfa3de84daad8b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans mutt, un client de courriel en
console.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14093">CVE-2020-14093</a>

<p>Mutt permettait une attaque de type « homme du milieu » sur fcc/postpone
d’IMAP à l'aide d'une réponse PREAUTH.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14954">CVE-2020-14954</a>

<p>Mutt possédait un problème de mise en tampon de STARTTLS qui affectait IMAP,
SMTP et POP3. Lorsque le serveur avait envoyé une réponse <q>begin TLS</q>, le
client lisait des données supplémentaires (par exemple, à partir d’une attaque
d’homme du milieu) et les évaluait dans un contexte TLS, c'est-à-dire « response
injection ».</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.5.23-3+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mutt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2268.data"
# $Id: $
