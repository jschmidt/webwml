#use wml::debian::translation-check translation="1b84c993a74b7b8cd0c782c95526dd22e16e69f3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Qualys Research Labs a découvert une élévation locale de privilèges dans
pkexec de PolicyKit.</p>

<p>Plus de détails sont disponibles dans l'annonce de Qualys à l'adresse
<a href="https://www.qualys.com/2022/01/25/cve-2021-4034/pwnkit.txt">\
https://www.qualys.com/2022/01/25/cve-2021-4034/pwnkit.txt</a>.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
0.105-18+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets policykit-1.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de policykit-1,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/policykit-1">\
https://security-tracker.debian.org/tracker/policykit-1</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2899.data"
# $Id: $
