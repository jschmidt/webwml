#use wml::debian::translation-check translation="50726b1d7ec4daf6f029b5daa55d51007cdd1073" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>James Kettle a découvert qu'une attaque par dissimulation de requête
peut être réalisée sur des connexions HTTP/1 sur des serveurs Varnish,
des accélérateurs web de haute performance. La requête dissimulée pourrait
être traitée comme une requête supplémentaire par le serveur Varnish, ce qui
pourrait conduire à une divulgation d'informations et à un empoisonnement
de cache.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
5.0.0-7+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets varnish.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de varnish, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/varnish">\
https://security-tracker.debian.org/tracker/varnish</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2920.data"
# $Id: $
