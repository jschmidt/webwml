#use wml::debian::translation-check translation="ed489b805ad5163cdef4ba5f17d2821aefacdcd8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jaime Frey a découvert un défaut dans HTCondor, un système de gestion de
charge distribuée. Un attaquant a besoin uniquement d'une autorisation de
niveau READ sur un démon vulnérable utilisant la méthode d'authentification
CLAIMTOBE. Cela signifie qu'il est possible d'exécuter des outils tels que
condor_q ou condor_status. Beaucoup de <q>pools</q> ne restreignent pas qui
peut émettre de commandes de niveau READ, et CLAIMTOBE est autorisé à
exécuter des commandes de niveau READ dans la configuration par défaut.
Ainsi, il est probable qu'un attaquant pourrait exécuter cette commande à
distance à partir d'un réseau non fiable, à moins que cela soit empêché par
un pare-feu ou d'autres contrôles d'accès au niveau du réseau.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
8.4.11~dfsg.1-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets condor.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de condor, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/condor">\
https://security-tracker.debian.org/tracker/condor</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">\
https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2984.data"
# $Id: $
