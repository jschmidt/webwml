#use wml::debian::translation-check translation="33f1bd2b27f7b1d854ea5067e5230d2ae0618497" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une vulnérabilité d'utilisation de mémoire après libération
dans irssi, le client IRC en mode console répandu.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13045">CVE-2019-13045</a>

<p>Irssi avant la version 1.0.8, 1.1.x avant la version 1.1.3 et 1.2.x
avant la version 1.2.1, quand SASL est activé, avait un problème
d'utilisation de mémoire après libération lors de l'envoi de connexion SASL
au serveur.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.0.7-1~deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets irssi.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3025.data"
# $Id: $
