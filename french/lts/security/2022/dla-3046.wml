#use wml::debian::translation-check translation="76e04c35224f8da4415e0609dd125fee3291942d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait un potentiel dépassement de tampon de tas dans librecad, un
système répandu de conception assistée par ordinateur (CAO). Un fichier
.dxf contrefait pour l'occasion pouvait mener à l'exécution de code
arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21897">CVE-2021-21897</a>

<p>Une vulnérabilité d'exécution de code existe dans la fonctionnalité
DL_Dxf::handleLWPolylineData de dxflib 3.17.0 de Ribbonsoft. Un fichier
.dxf contrefait pour l'occasion peut conduire à dépassement de tampon de
tas. Un attaquant peut fournir un ficher malveillant pour déclencher cette
vulnérabilité.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.1.2-1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets librecad.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3046.data"
# $Id: $
