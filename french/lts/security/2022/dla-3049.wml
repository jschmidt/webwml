#use wml::debian::translation-check translation="2db70cedbe4986cbc66ddc910d75682d939c8d12" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Mailman, un
gestionnaire de listes de diffusion basé sur le web. Un attaquant pouvait
usurper des comptes plus privilégiés au moyen de plusieurs vecteurs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43331">CVE-2021-43331</a>

<p>Une URL contrefaite vers la page des options utilisateur de
Cgi/options.py peut exécuter du code JavaScript arbitraire pour une attaque
par script intersite (XSS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43332">CVE-2021-43332</a>

<p>Le jeton CSRF pour la page admindb de Cgi/admindb.py contient une
version chiffrée du mot de passe de l'administrateur de listes. Elle
pouvait éventuellement être craquée par un modérateur au moyen d'une
attaque par force brute hors ligne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44227">CVE-2021-44227</a>

<p>Un membre de liste ou un modérateur peut obtenir un jeton CSRF et
contrefaire une requête d'administrateur (en utilisant ce jeton) pour
définir un nouveau mot de passe ou procéder à d'autres modifications.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:2.1.23-1+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mailman.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mailman, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mailman">\
https://security-tracker.debian.org/tracker/mailman</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3049.data"
# $Id: $
