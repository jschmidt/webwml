#use wml::debian::translation-check translation="5b3a2f790f8f8adde76d3a8321892b4bd807d859" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le client, relais et
serveur DHCP ISC.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2928">CVE-2022-2928</a>

<p>Le serveur DHCP ne réalisait pas correctement le compte de références
d'option quand il est configuré avec <q>allow leasequery;</q>. Un attaquant
distant pouvait tirer avantage de ce défaut pour provoquer un déni de
service (plantage du démon).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2929">CVE-2022-2929</a>

<p>Le serveur DHCP était prédisposé à un défaut de fuite de mémoire lors du
traitement du contenu des données de l'option 81 (fqdn) reçues dans un
paquet DHCP. Un attaquant distant pouvait tirer avantage de ce défaut pour
provoquer une consommation excessive de ressources par les serveurs DHCP,
avec pour conséquence un déni de service.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
4.4.1-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets isc-dhcp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de isc-dhcp, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/isc-dhcp">\
https://security-tracker.debian.org/tracker/isc-dhcp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3146.data"
# $Id: $
