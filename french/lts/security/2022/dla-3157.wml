#use wml::debian::translation-check translation="d74399e4b4de3445ba81e13bf5d88b77170669d4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BlueZ, la pile du
protocole Bluetooth de Linux. Un attaquant pouvait provoquer un déni de
service (DoS) ou divulguer des informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8921">CVE-2019-8921</a>

<p>Fuite d'information de SDP ; la vulnérabilité réside dans le traitement
de SVC_ATTR_REQ par l'implémentation de SDP de BlueZ. En contrefaisant un
CSTATE, il est possible de piéger le serveur à renvoyer plus d'octets que
le tampon n'en contient en réalité, avec pour conséquence la divulgation de
données arbitraires du tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8922">CVE-2019-8922</a>

<p>Dépassement de tas de SDP ; cette vulnérabilité réside aussi dans le
traitement par le protocole SDP des requêtes d'attribut. En demandant un
très grand nombre d'attributs en même temps, un attaquant peut faire
déborder le tampon statique fourni pour contenir la réponse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41229">CVE-2021-41229</a>

<p>sdp_cstate_alloc_buf alloue de la mémoire qui sera toujours attachée à
la liste liée individuellement de cstates et ne sera pas libérée. Cela
provoquera une fuite de mémoire avec le temps. Les données peuvent être un
très grand objet, ce qui peut être provoqué par un attaquant envoyant en
continu des paquets sdp et cela peut faire planter le service du
périphérique cible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43400">CVE-2021-43400</a>

<p>Une utilisation de mémoire après libération dans gatt-database.c peut
survenir quand un client se déconnecte durant le traitement par D-Bus d'un
appel WriteValue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0204">CVE-2022-0204</a>

<p>Une vulnérabilité de dépassement de tas a été découverte dans bluez. Un
attaquant doté d'un accès au réseau local pouvait passer des fichiers
contrefaits pour l'occasion faisant qu'une application s'arrête ou plante,
menant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39176">CVE-2022-39176</a>

<p>BlueZ permet à des attaquants proches physiquement d'obtenir des
informations sensibles parce que profiles/audio/avrcp.c ne valide pas
params_len.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39177">CVE-2022-39177</a>

<p>BlueZ permet à des attaquants proches physiquement de provoquer un déni
de service parce que des capacités mal formées et non valables peuvent être
traitées dans profiles/audio/avdtp.c.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
5.50-1.2~deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bluez.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bluez, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bluez">\
https://security-tracker.debian.org/tracker/bluez</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3157.data"
# $Id: $
