#use wml::debian::translation-check translation="c6e8eb7651edaf72a6a8b2fb3294855c3f28faa9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour comprend les modifications de tzdata 2022e. Parmi les
changements notables :</p>

<p>− La Syrie et la Jordanie abandonnent le régime d'heure d'été et passent
de façon permanente à UTC +03, ainsi ces pays ne reviendront pas à UTC +02
le 28 octobre 2022.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
2021a-0+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tzdata.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tzdata, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tzdata">\
https://security-tracker.debian.org/tracker/tzdata</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3161.data"
# $Id: $
