#use wml::debian::translation-check translation="0f4e83cb700c821f4c6eb669aef0531f078a3603" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Nicky Mouha a découvert un dépassement de tampon dans <q>_sha3</q>, le
module de fonction de hachage SHA-3 utilisé par <q>hashlib</q> dans
Python 3.7.</p>

<p>Dans la mesure où les attaques nécessitent un gros volume de données,
elles pouvaient éventuellement avoir pour conséquence l'exécution de code à
distance.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
3.7.3-2+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python3.7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python3.7, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python3.7">\
https://security-tracker.debian.org/tracker/python3.7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3175.data"
# $Id: $
