#use wml::debian::translation-check translation="90009d3937e6708f4ee90a7bee036ece08249343" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème a été découvert dans Dropbear, un serveur et client SSH
relativement petit. À cause d’une vérification non conforme à la RFC de la
méthode disponible d’authentification dans le code SSH du côté client, il était
possible pour un serveur SSH de modifier le processus de connexion en sa faveur.
Cette attaque peut contourner les mesures de sécurité additionnelles telles que
les jetons FIDO2 ou SSH-Askpass. Par conséquent, cela permet à un attaquant de
tromper un agent distant pour une connexion sur le serveur d’un autre agent
imperceptiblement.</p>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 2018.76-5+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dropbear.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dropbear,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dropbear">\
https://security-tracker.debian.org/tracker/dropbear</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3187.data"
# $Id: $
