#use wml::debian::translation-check translation="189e22544546d06630e4cf4819ba99dbe42f4175" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités d’empoisonnement de cache ont été trouvées dans node-tar,
un module de Node.js utilisé pour lire et écrire des archives tar portables,
qui pourraient aboutir à la création d’un fichier arbitraire ou un
écrasement.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37701">CVE-2021-37701</a>

<p>Il a été découvert que node-tar réalisait une protection insuffisante de
liens symboliques, rendant ainsi le cache de répertoire vulnérable à un
empoisonnement en utilisant des liens symboliques.</p>

<p>Lors de l’extraction d’une archive contenant un répertoire <q>foo/bar</q> suivi
avec un lien symbolique <q>foo\\bar</q> vers un emplacement arbitraire, node-tar
extrayait des fichiers arbitraires dans la cible du lien symbolique, par
conséquent permettant la création d’un fichier arbitraire et un écrasement.</p>

<p>De plus sur les systèmes de fichiers sensibles à la casse, un problème
similaire se produit avec un répertoire <q>FOO</q> suivi avec le lien symbolique
<q>foo</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37712">CVE-2021-37712</a>

<p>De manière similaire au
<a href="https://security-tracker.debian.org/tracker/CVE-2021-37701">CVE-2021-37701</a>,
une archive tar contrefaite pour l'occasion contenant deux répertoires et un
lien symbolique avec des noms contenant des valeurs Unicode qui
normalisait à la même valeur, pouvait contourner la vérification de liens
symboliques de node-tar pour des répertoires, par conséquent permettant la
création d’un fichier arbitraire et un écrasement.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 4.4.6+ds1-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-tar.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-tar,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-tar">\
https://security-tracker.debian.org/tracker/node-tar</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3237.data"
# $Id: $
