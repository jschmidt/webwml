#use wml::debian::translation-check translation="891421b4601c79c7f4a3cea1326017f08677ef06" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans Hawk, un schéma
d’authentification HTTP. Hawk utilisait une expression rationnelle pour analyser
l’en-tête <q>Host</q> HTTP qui était sujette à une attaque par déni de service
à l’aide d’une expression rationnelle. Chaque caractère ajouté dans l’entrée de
l’attaquant accroissait de façon exponentielle la durée du calcul.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29167">CVE-2022-29167</a>

<p>Hawk est un schéma d’authentification HTTP fournissant des mécanismes
d’authentification pour faire des requêtes HTTP authentifiées avec une
vérification partielle chiffrée de la requête et de la réponse, couvrant la
méthode HTTP, la requête d’URI, l’hôte et facultativement la requête de charge.
Hawk utilisait une expression rationnelle pour analyser l’en-tête <q>host</q>
HTTP (<q>Hawk.utils.parseHost()</q>), qui était sujette à une attaque par déni de
service à l’aide d’une expression rationnelle, c’est-à-dire que chaque caractère
ajouté dans l’entrée de l’attaquant accroissait de façon exponentielle la durée
du calcul. <q>parseHost()</q> a été corrigé dans la version 9.0.1 pour utiliser
la classe interne <q>URL</q> pour analyser le nom d’hôte à la place.
<q>Hawk.authenticate()</q> accepte un argument <q>options</q>. Si celui-ci
contient <q>host</q> et <q>port</q>, ceux-ci seront utilisés au lieu d’un appel
à <q>utils.parseHost()</q>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 6.0.1+dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-hawk.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3246.data"
# $Id: $
