#use wml::debian::translation-check translation="a4525c596e0bd2e4b56d5e12a9e6094e0f806a80" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans Apache::Session::LDAP avant la version 0.5, la validité du certificat
X.509 n’est pas vérifiée par défaut lors de la connexion à des dorsaux LDAP
distants, parce que la configuration par défaut du module Net::LDAPS pour Perl
était utilisée.</p>

<p>Cette mise à jour change le comportement par défaut pour requérir une
validation X.509 par rapport à l’ensemble
<code>/etc/ssl/certs/ca-certificates.crt</code>.
Le comportement antérieur peut être rétabli en exécutant <code>ldapVerify =&gt;
"none"</code> lors de l’initialisation de l’objet Apache::Session::LDAP.
<p><b>Remarque</b> : cette mise à jour est un prérequis pour le correctif de
LemonLDAP::NG
<a href="https://security-tracker.debian.org/tracker/CVE-2020-16093">CVE-2020-16093</a>
quand son dorsal de session est réglé à Apache::Session::LDAP.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.4-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapache-session-ldap-perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libapache-session-ldap-perl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libapache-session-ldap-perl">\
https://security-tracker.debian.org/tracker/libapache-session-ldap-perl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3284.data"
# $Id: $
