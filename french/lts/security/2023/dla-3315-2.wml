#use wml::debian::translation-check translation="65cbe3fa588392e5339981ad59ae6e8952dd8a02" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un des correctifs de sécurité publiés dans la DLA 3315 introduisait une
régression dans le traitement des fichiers WAV avec un débit binaire variable.
Des paquets sox mis à jour sont disponibles pour corriger ce problème.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 14.4.2+git20190427-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sox.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sox,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sox">\
https://security-tracker.debian.org/tracker/sox</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3315-2.data"
# $Id: $
