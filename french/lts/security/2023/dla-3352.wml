#use wml::debian::translation-check translation="c73941151246fecb19a1aedb94ac08104311e141" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans libde265, une implémentation au
code source ouvert du codec vidéo h.265, qui pouvaient aboutir à un déni de
service, avoir un autre impact non précisé ou éventuellement aboutir à une
exécution de code due à un dépassement de tampon de tas.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47664">CVE-2022-47664</a>

<p>Il a été découvert que libde265, version 1.0.9, était vulnérable à un
dépassement de tampon dans ff_hevc_put_hevc_qpel_pixels_8_sse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47665">CVE-2022-47665</a>

<p>Il a été découvert que libde265, version 1.0.9, était vulnérable à un
dépassement de tampon de tas dans de265_image::set_SliceAddrRS(int, int, int).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24751">CVE-2023-24751</a>

<p>Il a été découvert que libde265, version 1.0.10, contenait un déréférencement
de pointeur NULL dans la fonction mc_chroma dans motion.cc. Cette vulnérabilité
permettait à des attaquants de provoquer un déni de service (DoS) à l'aide d'un
fichier d’entrée contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24752">CVE-2023-24752</a>

<p>Il a été découvert que libde265, version 1.0.10, contenait un déréférencement
de pointeur NULL dans la fonction ff_hevc_put_hevc_epel_pixels_8_sse dans
sse-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer un
déni de service (DoS) à l'aide d'un fichier d’entrée contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24754">CVE-2023-24754</a>

<p>Il a été découvert que libde265, version 1.0.10, contenait un déréférencement
de pointeur NULL dans la fonction ff_hevc_put_weighted_pred_avg_8_sse dans
sse-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer un
déni de service (DoS) à l'aide d'un fichier d’entrée contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24755">CVE-2023-24755</a>

<p>Il a été découvert que libde265, version 1.0.10, contenait un déréférencement
de pointeur NULL dans la fonction put_weighted_pred_8_fallback dans
fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer
un déni de service (DoS) à l'aide d'un fichier d’entrée contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24756">CVE-2023-24756</a>

<p>Il a été découvert que libde265, version 1.0.10, contenait un déréférencement
de pointeur NULL dans la fonction ff_hevc_put_unweighted_pred_8_sse dans
sse-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer un
déni de service (DoS) à l'aide d'un fichier d’entrée contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24757">CVE-2023-24757</a>

<p>Il a été découvert que libde265, version 1.0.10, contenait un déréférencement
de pointeur NULL dans la fonction put_unweighted_pred_16_fallback dans
fallback-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer
un déni de service (DoS) à l'aide d'un fichier d’entrée contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24758">CVE-2023-24758</a>

<p>Il a été découvert que libde265, version 1.0.10, contenait un déréférencement
de pointeur NULL dans la fonction ff_hevc_put_weighted_pred_avg_8_sse dans
sse-motion.cc. Cette vulnérabilité permettait à des attaquants de provoquer un
déni de service (DoS) à l'aide d'un fichier d’entrée contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25221">CVE-2023-25221</a>

<p>Il a été découvert que libde265, version 1.0.10, contenait une vulnérabilité
de dépassement de tampon de tas dans la fonction
derive_spatial_luma_vector_prediction dans motion.cc.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.0.11-0+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libde265.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libde265,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libde265">\
https://security-tracker.debian.org/tracker/libde265</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3352.data"
# $Id: $
