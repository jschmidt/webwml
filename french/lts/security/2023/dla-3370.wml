#use wml::debian::translation-check translation="2e261b94f1c3dc6a243ec4580d63d7227918986a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs accès en mémoire hors limites et des dépassements de tampon ont été
corrigés dans xrdp, un projet au code source ouvert fournissant une invite
graphique de connexion sur des machines distantes en utilisant RDP (Remote
Desktop Protocol) de Microsoft.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23468">CVE-2022-23468</a>

<p>Les versions de xrdp inférieures à 0.9.21 contenaient un dépassement de
tampon dans la fonction xrdp_login_wnd_create(). Aucun contournement de ce
problème n’est connu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23478">CVE-2022-23478</a>

<p>Les versions de xrdp inférieures à 0.9.21 contenaient une écriture hors
limites dans la fonction xrdp_mm_trans_process_drdynvc_channel_open(). Aucun
contournement de ce problème n’est connu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23479">CVE-2022-23479</a>

<p>Les versions de xrdp inférieures à 0.9.21 contenaient un dépassement de tampon
dans la fonction xrdp_mm_chan_data_in(). Aucun contournement de ce problème
n’est connu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23483">CVE-2022-23483</a>

<p>Les versions de xrdp inférieures à 0.9.21 contenaient une lecture hors
limites dans la fonction libxrdp_send_to_channel(). Aucun contournement de ce
problème n’est connu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23484">CVE-2022-23484</a>

<p>Les versions de xrdp inférieures à 0.9.21 contenaient un dépassement d'entier
dans la fonction xrdp_mm_process_rail_update_window_text(). Aucun contournement
de ce problème n’est connu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23493">CVE-2022-23493</a>

<p>Les versions de xrdp inférieures à 0.9.21 contenaient une lecture hors
limites dans la fonction xrdp_mm_trans_process_drdynvc_channel_close(). Aucun
contournement de ce problème n’est connu.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.9.9-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xrdp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xrdp,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xrdp">\
https://security-tracker.debian.org/tracker/xrdp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3370.data"
# $Id: $
