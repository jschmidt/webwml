#use wml::debian::translation-check translation="3a7ad7286bb0093a4810ace2cdea5761b7114cf3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités potentielles de sécurité dans certains processeurs
d’Intel® ont été découvertes. Elles pouvaient permettre une divulgation
d'informations ou une élévation des privilèges. Intel publie une mise à jour du
microprogramme pour mitiger ces vulnérabilités potentielles.</p>

<p>Veuillez bien prendre en compte que le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-33196">CVE-2022-33196</a>
peut requérir une mise à jour du microprogramme.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21216">CVE-2022-21216</a>

<p>(INTEL-SA-00700)
Une granularité insuffisante du contrôle d’accès dans la gestion hors bande dans
quelques processeurs extensibles Atom et Xeon d’Intel pouvait permettre à un
utilisateur privilégié d’éventuellement augmenter ses privilèges à l'aide d’un
accès par réseau adjacent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33196">CVE-2022-33196</a>

<p>(INTEL-SA-00738)
Des permissions par défaut incorrectes dans certaines configurations de
contrôleur de mémoire pour certains processeurs Xeon® d’Intel lors de
l’utilisation des <q>Software Guards Extensions</q> pouvaient permettre à un
utilisateur privilégié d’éventuellement augmenter ses privilèges à l’aide d’un
accès local.</p>

<p>Ce correctif peut requérir une mise à jour du microprogramme pour être
effectif pour certains processeurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33972">CVE-2022-33972</a>

<p>(INTEL-SA-00730)
Un calcul incorrect dans le mécanisme de clé pour quelques processeurs
Xeon de troisième génération d’Intel pouvait permettre à un utilisateur
privilégié d’éventuellement divulguer des informations à l’aide d’un accès
local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38090">CVE-2022-38090</a>

<p>(INTEL-SA-00767)
Une isolation incorrecte de ressources partagées dans certains processeurs
d’Intel lors de l’utilisation des <q>Software Guard Extensions</q> pouvait
permettre à un utilisateur privilégié d’éventuellement divulguer des
informations à l’aide d’un accès local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21233">CVE-2022-21233</a>

<p>(INTEL-SA-00657)
Une isolation incorrecte de ressources partagées dans certains processeurs
d’Intel pouvait permettre à un utilisateur privilégié d’éventuellement
divulguer des informations à l’aide d’un accès local.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.20230214.1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3379.data"
# $Id: $
