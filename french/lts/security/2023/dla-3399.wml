#use wml::debian::translation-check translation="a3fd94e0bf67ff4cfad8894fe3fc2b7ea85fef82" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans 389-ds-base, un
serveur LDAP au code source ouvert pour Linux.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3883">CVE-2019-3883</a>

<p>Les requêtes SSL/TLS n’appliquaient pas la limite ioblocktimeout, conduisant à
une vulnérabilité de déni de service (DoS) en suspendant tous les <q>workers</q>
avec des suspensions de requête LDAP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10224">CVE-2019-10224</a>

<p>Une vulnérabilité pouvait divulguer des informations sensibles, telles que
le mot de passe du <q>Directory Manager</q>, quand les commandes dscreate et
dsconf étaient exécutées dans le mode <q>verbose</q>. Un attaquant qui pouvait
voir l’écran ou capturer la sortie d’erreur du terminal pouvait exploiter cette
vulnérabilité pour obtenir des informations confidentielles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14824">CVE-2019-14824</a>

<p>Le greffon <q>deref</q> de 389-ds-base avait une vulnérabilité qui lui
permettait de divulguer des valeurs d’attribut en utilisant les permissions de
<q>search</q>. Dans certaines configurations, un attaquant authentifié pouvait
exploiter ce défaut pour accéder à des attributs confidentiels, dont les
hachages de mot de passe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3514">CVE-2021-3514</a>

<p>Si un client sync_repl était utilisé, un attaquant authentifié pouvait
déclencher un plantage en exploitant une requête contrefaite pour l'occasion,
aboutissant à un déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3652">CVE-2021-3652</a>

<p>L’importation d’un astérisque comme hachage de mot de passe conduisait à
un authentification réussie avec n’importe quel mot de passe, permettant à
des attaquants d’accéder aux comptes avec leur mot de passe désactivé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4091">CVE-2021-4091</a>

<p>Une double libération de zone de mémoire a été découverte dans la façon dont
389-ds-base gérait le contexte virtuel d’attribut dans les recherches
persistantes. Un attaquant pouvait envoyer une série de requêtes de recherche,
forçant le serveur à se comporter de manière inattendue et planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0918">CVE-2022-0918</a>

<p>Un attaquant authentifié avec accès réseau au port LDAP pouvait provoquer un
déni de service. Le déni de service était déclenché par un seul message envoyé
à travers une connexion TCP, aucun <q>bind</q> ou autre authentification n’était
nécessaire. Le message déclenchait une erreur de segmentation qui aboutissait
au plantage de slapd.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0996">CVE-2022-0996</a>

<p>Des mots de passe expirés étaient toujours autorisés pour accéder à la
base de données. Un utilisateur dont le mot de passe était expiré était toujours
autorisé à accéder à la base de données comme si le mot de passe était toujours
valable. Une fois que le mot de passe avait expiré et que <q>grace logins</q>
était épuisé, le compte était en gros supposé verrouillé et ne devait pas
permettre de réaliser toute action nécessitant des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2850">CVE-2022-2850</a>

<p>Une vulnérabilité dans greffon de synchronisation de contenu permettait à
un attaquant authentifié de déclencher un déni de service à l'aide d'une
requête contrefaite à travers un déréférencement de pointeur NULL.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.4.0.21-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets 389-ds-base.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de 389-ds-base,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/389-ds-base">\
https://security-tracker.debian.org/tracker/389-ds-base</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3399.data"
# $Id: $
