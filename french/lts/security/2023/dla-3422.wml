#use wml::debian::translation-check translation="8c612d183eb4738bec70dec9f048c3eadfe7893d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été trouvés dans PostgreSQL, qui pouvaient
aboutir à une élévation des privilèges ou à une application incorrecte de la
politique.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 11.20-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-11">\
https://security-tracker.debian.org/tracker/postgresql-11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3422.data"
# $Id: $
