#use wml::debian::translation-check translation="1284f981a9d0ed8a6160d422076b98181b9e68f2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une attaque potentielle de vol
d’identifiants dans <code>epiphany-browser</code>, le navigateur web par défaut
de GNOME.</p>

<p>Lors de l’utilisation de CSP (Content Security Policy) dans un bac à sable
ou de la balise HTML <code>iframe</code>, le contenu web dans le bac à sable était
accepté par la ressource principale ou environnante. Après cette modification,
le gestionnaire de mots de passe est néanmoins entièrement désactivé dans cette
situation, de façon à ce que du contenu web non fiable ne puisse exfiltrer les
mots de passe.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26081">CVE-2023-26081</a>

<p>Dans Epiphany jusqu’à la version 43.0, du contenu web non fiable pouvait
amener des utilisateurs à exfiltrer des mots de passe à cause de remplissements
automatiques se produisant dans des contextes de bac à sable.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.32.1.2-3~deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets epiphany-browser.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3423.data"
# $Id: $
