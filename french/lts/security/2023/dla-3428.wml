#use wml::debian::translation-check translation="d7c422f10cbe31676d59e340032510e58af3a303" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>node-nth-check, est un module de NodeJS utilisé pour analyser et compiler
les <q>nth-check</q> quand ils apparaissent dans les fonctions CSS 3 nth-child()
et nth-last-of-type().</p>

<p>Ce module était vulnérable à un déni de service à l’aide d’une expression
rationnelle utilisée pour l’analyse.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.0.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-nth-check.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-nth-check,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-nth-check">\
https://security-tracker.debian.org/tracker/node-nth-check</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3428.data"
# $Id: $
