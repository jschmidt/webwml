#use wml::debian::translation-check translation="f641487c161346e72bb703ac04bcfb4f5ecd32ce" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été corrigées dans Wordpress, un
cadriciel de gestion de contenu populaire.</p>

<p>WordPress Core est vulnérable à une traversée de répertoires à l’aide du
paramètre <q>wp_lang</q>. Cela permettait à des attaquants authentifiés
d’accéder et charger des fichiers de traduction arbitraires. Dans le cas où un
attaquant était capable de téléverser un fichier de traduction contrefait sur
le site, comme à l'aide d'un formulaire de téléversement, cela pouvait aussi
être utilisé pour réaliser une attaque par script intersite.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5.0.19+dfsg1-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wordpress,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/wordpress">\
https://security-tracker.debian.org/tracker/wordpress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3462.data"
# $Id: $
