#use wml::debian::translation-check translation="d2cd3e5195f24051263b23d7de760c15f8f45df8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans cups, le système commun d'impression pour
UNIX™. À cause d’un bogue d’utilisation de mémoire après libération, un
attaquant pouvait provoquer un déni de service. Dans le cas où il avait accès
aux fichiers de journal, un attaquant pouvait aussi récupérer les clés privées
ou d’autres informations sensibles du démon de cups.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.2.10-6+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cups.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cups,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cups">\
https://security-tracker.debian.org/tracker/cups</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3476.data"
# $Id: $
