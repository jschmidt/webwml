#use wml::debian::translation-check translation="532c6a9a161847085a8efaa0e0d2a5e182fee161" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une fuite de mémoire a été découverte dans yajl, un analyseur JSON et
petit générateur de validation JSON écrit en ANSI C. Elle pouvait permettre à un
attaquant de provoquer une situation d’épuisement de mémoire et, éventuellement,
causer un plantage.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.1.0-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets yajl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de yajl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/yajl">\
https://security-tracker.debian.org/tracker/yajl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3478.data"
# $Id: $
