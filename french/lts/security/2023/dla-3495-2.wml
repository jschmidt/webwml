#use wml::debian::translation-check translation="1638eb680c6bf775ca6a5a6f5bb959d78ff86bb0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>L’équipe de sécurité d’Ubuntu a remarqué après des tests intensifs que la
DLA-3495-1 était incomplète car une démonstration de faisabilité pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-2400">CVE-2022-2400</a>
(en particulier la sortie du chroot) fonctionnait toujours dans la version
corrigée du paquet.</p>

<p>Une analyse plus approfondie du correctif de l’amont et de la version
DLA-3495-1 a permis de révéler que la vulnérabilité était toujours présente car
la DLA 3495-1 n’incluait pas le commit 7adf00f9 qui ajoutait des vérifications
de chroot à un des chemins du code.</p>

<p>Un grand merci à Camila Camargo de Matos de l’équipe de sécurité d’Ubuntu.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.6.2+dfsg-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-dompdf.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php-dompdf,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php-dompdf">\
https://security-tracker.debian.org/tracker/php-dompdf</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3495-2.data"
# $Id: $
