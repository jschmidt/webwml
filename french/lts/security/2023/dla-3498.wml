#use wml::debian::translation-check translation="af20be509696191a5e7188dd7bc6753ad226849c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un déni de service potentiel (DoS)
dans bind9, le serveur populaire de noms de domaine (DNS). Shoham Danino, Anat
Bremler-Barr, Yehuda Afek et Yuval Shavitt ont découvert qu’un défaut dans
l’algorithme de nettoyage de cache utilisé dans named peut provoquer que la
limite de taille de cache configurée de named pouvait être largement dépassée
aboutissant éventuellement à une attaque par déni de service.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2828">CVE-2023-2828</a></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:9.11.5.P4+dfsg-5.1+deb10u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3498.data"
# $Id: $
