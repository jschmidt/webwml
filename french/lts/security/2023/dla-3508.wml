#use wml::debian::translation-check translation="0715874f5416c3bfeb15fd28abdc9cf1c8a2cfc7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1380">CVE-2023-1380</a>

<p>Jisoo Jang a signalé une lecture hors limites de tas dans le pilote brcmfmac
Wi-Fi. Sur les systèmes utilisant ce pilote, un utilisateur local pouvait
exploiter cela pour lire des informations sensibles ou provoquer un déni de
service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2002">CVE-2023-2002</a>

<p>Ruiahn Li a signalé une vérification incorrecte des permissions dans le
sous-système Bluetooth. Un utilisateur local pouvait exploiter cela pour
reconfigurer les interfaces Bluetooth locales, aboutissant à une fuite
d'informations, une usurpation ou un déni de service (perte de connexion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2007">CVE-2023-2007</a>

<p>Lucas Leong (@_wmliang_) et Reno Robert de Trend Micro Zero Day
Initiative ont découvert un défaut de <q>time-of-check-to-time-of-use</q> dans
le pilote de contrôleur SCSI dpt_i2o. Un utilisateur local avec accès à un
périphérique SCSI utilisant ce pilote pouvait exploiter cela pour une élévation
des privilèges.</p>

<p>Ce défaut a été mitigé en supprimant la prise en charge de l’opération
I2OUSRCMD.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2269">CVE-2023-2269</a>

<p>Zheng Zhang a signalé que le traitement incorrect du verrouillage dans
l’implémentation de mappage de périphériques pouvait mener à un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3090">CVE-2023-3090</a>

<p>Il a été découvert qu’une initialisation manquante dans la mise en réseau
ipvlan pouvait conduire à une vulnérabilité d’écriture hors limites, aboutissant
à un déni de service ou éventuellement à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3111">CVE-2023-3111</a>

<p>L’outil TOTE Robot à trouvé un défaut dans le pilote de système de fichiers
Btrfs qui pouvait conduire à une utilisation de mémoire après libération. Il
n’est pas sûr qu’un utilisateur non privilégié pouvait exploiter cela.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3141">CVE-2023-3141</a>

<p>Un défaut a été découvert dans le pilote r592 memstick qui pouvait conduire à
une utilisation de mémoire après libération après que le pilote ait été retiré
ou délié du périphérique. L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3268">CVE-2023-3268</a>

<p>Il a été découvert qu’un accès en mémoire hors limites dans relayfs
pouvait aboutir à un déni de service ou à une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3338">CVE-2023-3338</a>

<p>Ornaghi Davide a découvert un défaut dans l’implémentation du protocole
DECnet qui pouvait conduire à un déréférencement de pointeur NULL ou à une
utilisation de mémoire après libération. Un utilisateur local pouvait exploiter
cela pour provoquer un déni de service (plantage ou corruption de mémoire) et
probablement pour une élévation des privilèges.</p>

<p>Ce défaut a été mitigé en supprimant l’implémentation du protocole DECnet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20593">CVE-2023-20593</a>

<p>Tavis Ormandy a découvert que dans certaines circonstances
microarchitecturales, un registre de vecteur dans les CPU AMD <q>Zen 2</q>
n’était par réglé correctement à zéro. Ce défaut permettait à un attaquant de
divulguer des informations sensibles à travers des processus concurrents, des
threads hyper et des invités virtualisés.</p>

<p>Pour plus de détails, consulter
<a href="https://lock.cmpxchg8b.com/zenbleed.html">https://lock.cmpxchg8b.com/zenbleed.html</a> et
<a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">
https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a>.</p>

<p>Ce problème peut être aussi mitigé par une mise à jour du microcode à travers
la mise à jour du paquet amd64-microcode ou un mise à jour du microcode
(BIOS/UEFI). Cependant, la publication initiale du microcode par AMD fournit
seulement des mises à jour pour les CPU de seconde génération EPYC. Divers CPU
Ryzen sont aussi affectés, mais aucune mise à jour n’est encore disponible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31084">CVE-2023-31084</a>

<p>Il a été découvert que le pilote Core DVB ne gère pas correctement le blocage
de certains évènements, permettant à un utilisateur local de provoquer un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32233">CVE-2023-32233</a>

<p>Patryk Sondej et Piotr Krysiuk ont découvert un défaut d’utilisation de
mémoire après libération dans l’implémentation Netfilter de nf_tables lors du
traitement de requêtes par lot, qui pouvait aboutir à une élévation locale des
privilèges pour un utilisateur ayant la capacité CAP_NET_ADMIN dans n’importe
quel espace de noms utilisateur ou réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34256">CVE-2023-34256</a>

<p>L’outil syzbot a trouvé un défaut de <q>time-of-check-to-time-of-use</q> dans
le pilote de système de fichiers ext4. Un attaquant capable de monter une image
disque ou un périphérique qu’il pouvait éditer directement pouvait exploiter
cela pour provoquer une lecture hors limites, éventuellement aboutissant à une
divulgation d’informations sensibles ou à un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35788">CVE-2023-35788</a>

<p>Hangyu Hua a découvert une vulnérabilité d’écriture hors limites dans le
classificateur Flower, qui pouvait aboutir à un déni de service ou à l’exécution
de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35823">CVE-2023-35823</a>

<p>Un défaut a été découvert dans le pilote de média saa7134, qui pouvait
conduire à une utilisation de mémoire après libération après que le pilote ait
été retiré ou délié d’un périphérique. L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35824">CVE-2023-35824</a>
<p>Un défaut a été découvert dans le pilote de média dm1105, qui pouvait
conduire à une utilisation de mémoire après libération après que le pilote ait
été retiré ou délié d’un périphérique. L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35828">CVE-2023-35828</a>

<p>Un défaut a été découvert dans le pilote de mode de périphérique USB
renesas_udc, qui pouvait conduire à une utilisation de mémoire après libération
après que le pilote ait été retiré ou délié d’un périphérique. L’impact de
sécurité est incertain.</p>

<p>Ce pilote n’est pas activé dans les configurations officielles du noyau de
Debian.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 4.19.289-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3508.data"
# $Id: $
