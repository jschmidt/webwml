#use wml::debian::translation-check translation="65a83f503cc1ce34a1e86a1fbb3dd094502996c8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans tiff, une bibliothèque et des
outils fournissant une prise en charge pour TIFF (Tag Image File Format).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2908">CVE-2023-2908</a>

<p>Déréférencement de pointeur NULL dans tif_dir.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3316">CVE-2023-3316</a>

<p>Déréférencement de pointeur NULL dans TIFFClose()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3618">CVE-2023-3618</a>

<p>Dépassement de tampon dans tiffcrop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25433">CVE-2023-25433</a>

<p>Dépassement de tampon dans tiffcrop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26965">CVE-2023-26965</a>

<p>Utilisation de mémoire après libération dans tiffcrop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26966">CVE-2023-26966</a>

<p>Dépassement de tampon dans uv_encode()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40745">CVE-2023-40745</a>

<p>Dépassement d'entier dans tiffcp</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41175">CVE-2023-41175</a>

<p>Dépassement d'entier dans raw2tiff</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 4.1.0+git191117-2~deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tiff,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tiff">\
https://security-tracker.debian.org/tracker/tiff</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3513.data"
# $Id: $
