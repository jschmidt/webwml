#use wml::debian::translation-check translation="cbbd46c1cbce70367f34acf29abc088a6471a9da" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La suite bureautique LibreOffice était affectée par plusieurs vulnérabilités.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3874">CVE-2022-38745</a>

<p>Libreoffice pouvait être configuré pour ajouter une entrée vide dans le
chemin d’accès aux classes Java. Cela pouvait conduire à une exécution de code Java arbitraire
à partir du répertoire de travail.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0950">CVE-2023-0950</a>

<p>Une vulnérabilité de validation incorrecte dans les indices de tableau dans
le composant tableur permettait à un attaquant de confectionner un document
tableur qui provoquait le soupassement d’un indice de tableau lors du
chargement. Dans les versions affectées de LibreOffice, certaines formules d’un
tableur mal formé, telles que AGGREGATE, pouvaient être créées avec moins de
paramètres passés à l’interpréteur de la formule que ceux attendus, conduisant à un
soupassement d’indice de tableau, auquel cas du code arbitraire pouvait être
exécuté.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2255">CVE-2023-2255</a>

<p>Un contrôle incorrect d’accès dans les composants d’éditeur de LibreOffice
permettait à un attaquant de confectionner un document qui provoquait le
chargement de liens externes sans invite. Dans les versions affectées de
LibreOffice, des documents qui utilisaient des <q>cadres flottants</q> liés aux
fichiers externes, chargeaient le contenu de ces cadres sans demander à
l’utilisateur la permission de le faire. Cela était incohérent avec le
traitement de contenus liés dans LibreOffice.</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:6.1.5-3+deb10u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libreoffice.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libreoffice,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libreoffice">\
https://security-tracker.debian.org/tracker/libreoffice</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3526.data"
# $Id: $
