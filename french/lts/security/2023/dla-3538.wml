#use wml::debian::translation-check translation="ea906494c55b7522834c1a7298f4d747d5b9d11a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans zabbix, une
solution de supervision réseau, permettant éventuellement le plantage du
serveur, la divulgation d'informations ou des attaques par script intersite.</p>

<p>Remarques importantes :
Pour mitiger le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17382">CVE-2019-17382</a>,
sur les installations existantes, le compte du client doit être désactivé
manuellement, par exemple, en désactivant le <q>Guest group</q> dans l’interface
utilisateur : Administration -> User groups -> Guests -> Untick Enabled</p>

<p>Cette mise à jour corrige aussi une régression du
<a href="https://security-tracker.debian.org/tracker/CVE-2022-35229">CVE-2022-35229</a>,
qui casse la possibilité d’éditer et d’ajouter des règles de recherche dans
l’interface utilisateur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-7484">CVE-2013-7484</a>

<p>Zabbix avant la version 4.4.0alpha2 stocke les identifiants dans la table
<q>users</q> avec le hachage de mot de passe stocké dans un hachage MD5, connu
comme être une méthode non sécurisée. De plus aucun sel n’était ajouté pour le
hachage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17382">CVE-2019-17382</a>

<p>(Controversé, pas vu par l’amont comme un problème de sécurité.)</p>

<p>Un problème a été découvert dans zabbix.php?actionÚshboard.view&amp;dashboardid=1
dans Zabbix jusqu’à la version 4.4. Un attaquant pouvait contourner la page de
connexion et accéder à la page dashboard et puis créer un Dashboard, Report,
Screen ou Map sans nom d’utilisateur/mot de passe (c’est-à-dire anonymement).
Tous les éléments créés (Dashboard/Report/Screen/Map) étaient accessibles aux
autres utilisateurs et à l’administrateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-35229">CVE-2022-35229</a>

<p>Un utilisateur authentifié pouvait créer un lien avec du code Javascript
réfléchi à l’intérieur pour la page de découverte et l’envoyer à d’autres
utilisateurs. La charge pouvait seulement être exécutée avec une valeur connue
de jeton CSRF de la victime, qui est modifiée périodiquement et qui est
difficile à prédire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43515">CVE-2022-43515</a>

<p>L’interface de Zabbix fournit une fonctionnalité qui permettait aux
administrateurs d’entretenir l’installation et assurer que seulement certaines
adresses IP pouvaient y accéder. De cette façon aucun utilisateur ne pouvait
accéder à l’interface de Zabbix tandis qu’il était entretenu et les données
sensibles ne pouvaient être divulguées. Un attaquant pouvait contourner cette
protection et accéder à cette instance en utilisant des adresses IP
n’appartenant pas à l’intervalle défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29450">CVE-2023-29450</a>

<p>Le prétraitement de JavaScript pouvait être utilisé par un attaquant pour
obtenir l’accès au système de fichiers (en écriture seulement sur le compte de
l’utilisateur <q>zabbix</q>) sur le serveur ou le mandataire de Zabbix,
conduisant éventuellement à un accès non autorisé à des données sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29451">CVE-2023-29451</a>

<p>Une chaine contrefaite pour l'occasion pouvait provoquer un dépassement de
tampon dans la bibliothèque de l’analyseur bibliothèque JSON, conduisant à un
plantage du serveur ou du mandataire de Zabbix.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29454">CVE-2023-29454</a>

<p>Une vulnérabilité de script intersite (XSS) stockée ou persistante a été
découverte dans la section <q>Users</q> dans l’onglet <q>Media</q> dans le champ
de formulaire <q>Send to</q>. Quand un nouveau média était créé avec du code
malveillant inclus dans le champ <q>Send to</q>, ce code était exécuté lors de
l’édition du même média.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29455">CVE-2023-29455</a>

<p>Une attaque XSS réfléchi, aussi connue non permanente, a été découverte où
un attaquant pouvait passer du code malveillant dans des requêtes GET à
graph.php et que le système le stockait et qui était exécuté quand la page graph
était ouverte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29456">CVE-2023-29456</a>

<p>Un schéma de validation d’URL recevait une entrée d’un utilisateur et
ensuite l’analysait pour identifier ses divers composants. Le schéma de
validation doit assurer que tous les composants de l’URL sont conformes aux
normes d’Internet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29457">CVE-2023-29457</a>

<p>Une attaque XSS réfléchi, aussi connue non permanente, a été découverte où des
cookies de session XSS pouvaient être révélés, autorisant le coupable à se faire
passer pour un utilisateur autorisé et trafiquer son compte privé.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:4.0.4+dfsg-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zabbix.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zabbix,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zabbix">\
https://security-tracker.debian.org/tracker/zabbix</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3538.data"
# $Id: $
