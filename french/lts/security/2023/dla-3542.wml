#use wml::debian::translation-check translation="cf13f3e4e557c5cdd66ff4c562e2131cdb36098d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut spécifique lors du traitement de volumes de sauvegarde existait
dans UnRAR, un extracteur de fichiers rar. Il permettait à des attaquants
distants d’exécuter du code arbitraire sur les installations affectées. Une
interaction utilisateur était requise pour exploiter cette vulnérabilité. La
cible devait visiter une page malveillante ou ouvrir un fichier rar
malveillant.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:5.6.6-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unrar-nonfree.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de unrar-nonfree,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/unrar-nonfree">\
https://security-tracker.debian.org/tracker/unrar-nonfree</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3542.data"
# $Id: $
