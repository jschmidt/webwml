#use wml::debian::translation-check translation="2583d3aa00b99af25fb9f67b944d2e84b5c73b35" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des problèmes de sécurité ont été trouvés dans PHP, un langage de script
généraliste au source libre couramment utilisé, qui pouvaient aboutir à une
divulgation d'informations, à un déni de service ou éventuellement à une
exécution de code à distance.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3823">CVE-2023-3823</a>

<p>Diverses fonctions XML reposent sur l’état global de libxml pour suivre les
variables de configuration, comme quelles entités sont chargées. Cet état est
supposé constant à moins que l’utilisateur change cela explicitement en appelant
les fonctions appropriées. Joas Schilling et Baptista Katapi ont découvert que,
puisque l’état est global pour les processus, d’autres modules, tels que
ImageMagick, pouvaient utiliser cette bibliothèque dans le même processus et
modifier l’état global pour leurs besoins internes, et le laisser dans un état
où le chargement d’entités externes était activé. Cela pouvait conduire à une
situation où du XML externe était analysé avec des entités externes chargées et
permettre la divulgation de tout fichier local accessible avec PHP. Cet état de
vulnérabilité pouvait persister dans le même processus à travers de nombreuses
requêtes jusqu’à ce que le processus soit arrêté.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3824">CVE-2023-3824</a>

<p>Niels Dossche a découvert que lors du chargement d’un fichier tout en lisant
les entrées de répertoire PHAR, une vérification insuffisante de longueur
pouvait conduire à un dépassement de tampon de pile, conduisant éventuellement
à une corruption de mémoire ou une exécution de code à distance.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 7.3.31-1~deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php7.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php7.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php7.3">\
https://security-tracker.debian.org/tracker/php7.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3555.data"
# $Id: $
