#use wml::debian::translation-check translation="0e02221f2434a84eeb0774c4dd6bf086e44013ac" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, le serveur de
connexion, d’impression et de fichiers SMB/CIFS pour Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2124">CVE-2016-2124</a>

<p>Un défaut a été découvert dans la façon dont samba mettait en œuvre
l’authentification SMB1. Un attaquant pouvait utiliser ce défaut pour retrouver
les mots de passe en texte simple envoyés sur le réseau même si une
authentification Kerberos était demandé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10218">CVE-2019-10218</a>

<p>Un défaut a été découvert dans le client samba pour toutes les versions avant
4.11.2, 4.10.10 et 4.9.15, quand un serveur malveillant pouvait fournir au
client un chemin avec des séparateurs. Cela pouvait permettre au client
d’accéder à des fichiers ou des répertoires en dehors des chemins de réseau SMB.
Un attaquant pouvait utiliser cette vulnérabilité pour créer des fichiers en
dehors du répertoire de travail en cours en utilisant les privilèges de
l’utilisateur client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14833">CVE-2019-14833</a>

<p>Un défaut a été découvert dans Samba dans toutes les versions depuis
samba 4.5.0 avant samba 4.9.15, samba 4.10.10, samba 4.11.2, dans la manière
dont il gérait le changement de mot de passe ou la création d’un nouveau pour un
utilisateur de samba. Le contrôleur de domaine Active Directory de Samba pouvait
être configuré pour utiliser un script personnalisé pour vérifier la complexité
du mot de passe. Cette configuration pouvait échouer à vérifier cela quand des
caractères non ASCII étaient utilisés dans le mot de passe, ce qui pouvait
conduire à l’établissement de mots de passe faibles pour les utilisateurs de
samba, les rendant vulnérables à des attaques par dictionnaires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14847">CVE-2019-14847</a>

<p>Un défaut a été découvert dans samba 4.0.0 avant samba 4.9.15 et samba 4.10.x
avant 4.10.10. Un attaquant pouvait planter le serveur LDAP de DC d’AD à l’aide
de dirsync aboutissant à un déni de service. Une élévation des privilèges n’était
pas possible avec ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14902">CVE-2019-14902</a>

<p>Un problème existait dans toutes les versions de samba 4.11.x avant
4.11.5, toutes les versions de samba 4.10.x avant 4.10.12 et toutes les
versions de samba 4.9.x avant 4.9.18, quand la suppression de la permission de
créer ou de modifier un sous-arbre n’était pas automatiquement supprimée pour
tous les contrôleurs de domaine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14907">CVE-2019-14907</a>

<p>Toutes les versions de samba 4.9.x avant 4.9.18, 4.10.x avant 4.10.12 et
4.11.x avant 4.11.5 avaient un problème par lequel, si réglées avec
« log level = 3 » (ou au-dessus), la chaine obtenue du client, après une
conversion de caractère défectueuse, était imprimée. De telles chaines pouvaient
être fournies durant l’échange d’authentification NTLMSSP. Dans le DC d’AC de
Samba en particulier, cela pouvait faire que les processus de longue durée (tels
que le serveur RPC) se terminent (dans le cas du serveur de fichiers, la cible
la plus probable, smbd, opère comme client par processus et donc un plantage était
sans conséquence).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19344">CVE-2019-19344</a>

<p>Un problème d’utilisation de mémoire après libération existait dans toutes
les versions de samba 4.9.x avant 4.9.18, toutes les versions samba 4.10.x
avant 4.10.12 et toutes les versions samba 4.11.x avant 4.11.5, essentiellement
due à un appel à realloc() alors que les autres variables locales pointaient
vers le tampon originel.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2:4.9.5+dfsg-5+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Il est rappelé et fortement encouragé aux administrateurs de configurations
de contrôleur de domaine d’Active Directory de mettre à niveau vers
<q>Bullseye</q> et puis vers <q>Bookworm</q>, car ces configurations ne sont
plus prises en charge dans <q>Buster</q> depuis la
 <a href="https://www.debian.org/security/2021/dsa-5015">DSA 5015-1</a>
et dans <q>Bullseye</q> depuis la
<a href="https://www.debian.org/security/2023/dsa-5477">DSA 5477-1</a>.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3563.data"
# $Id: $
