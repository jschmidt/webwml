#use wml::debian::translation-check translation="ed7b72d0cbd29575352e108dd0535745ba89eabc" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été identifiée dans c-ares, une bibliothèque de
résolution de noms asynchrone.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22217">CVE-2020-22217</a>:

<p>Une vulnérabilité de dépassement de tampon a été découverte dans c-ares,
versions 1.16.1 jusqu’à 1.17.0 à l’aide de la fonction ares_parse_soa_reply
dans ares_parse_soa_reply.c. Cette vulnérabilité a été découverte à l’aide de
tests à données aléatoires. L’exploitation de cette vulnérabilité pouvait
permettre à un attaquant d’exécuter du code arbitraire ou de causer une
condition de déni de service.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.14.0-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets c-ares.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de c-ares,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/c-ares">\
https://security-tracker.debian.org/tracker/c-ares</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3567.data"
# $Id: $
