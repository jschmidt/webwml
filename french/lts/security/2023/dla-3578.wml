#use wml::debian::translation-check translation="3edd32bb7d1ddee0f8c00582be024909f06b9059" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Matteo Memelli a découvert un défaut dans lldpd, une implémentation du
protocole 802.1ab de l’IEEE. En contrefaisant un paquet PDU CDP avec des TLV
CDP_TLV_ADDRESSES particuliers, un acteur malveillant pouvait à distance
forcer le démon lldpd à réaliser une lecture hors limites dans la mémoire de
tas.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.0.3-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lldpd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lldpd,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/lldpd">\
https://security-tracker.debian.org/tracker/lldpd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3578.data"
# $Id: $
