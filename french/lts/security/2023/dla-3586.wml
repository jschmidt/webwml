#use wml::debian::translation-check translation="86686a093b942687309c4b3901441404871826de" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de lecture hors limites a été découvert dans la fonction
postprocess_terminfo de ncurses, une boîte à outils basée sur le texte
d’interface utilisateur, qui pouvait éventuellement conduire à une exposition
d’informations sensibles ou à un déni de service.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 6.1+20181013-2+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ncurses.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ncurses,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ncurses">\
https://security-tracker.debian.org/tracker/ncurses</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3586.data"
# $Id: $
