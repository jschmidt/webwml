#use wml::debian::translation-check translation="f80e16c33235f972aec4f6e0cb3f0cd3871ab961" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans qemu, un émulateur rapide de
processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24165">CVE-2020-24165</a>

<p>Une utilisation de mémoire après libération dans le
générateur de code pouvait conduire à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0330">CVE-2023-0330</a>

<p>Un problème de réentrance DMA-MMIO dans le périphérique lsi53c895a pouvait
conduire à des bogues de corruption de mémoire, tels qu’un dépassement de pile
ou une utilisation de mémoire après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3180">CVE-2023-3180</a>
<p>La fonction virtio_crypto_sym_op_helper, une partie de l’implémentation de
périphérique virtuel de chiffrement de qemu, ne vérifiait pas si les valeurs de
<q>src_len</q> et <q>dst_len</q> étaient les mêmes. Cela pouvait conduire à un
dépassement de tampon de tas.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:3.1+dfsg-8+deb10u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3604.data"
# $Id: $
