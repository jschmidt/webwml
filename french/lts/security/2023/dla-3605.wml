#use wml::debian::translation-check translation="9ce384c4bc303a2bdbd4ebee83c618f0ec6ea1af" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été signalés dans le paquet grub2 du
chargeur d’amorçage GRand Unified Bootloader version 2, qui pouvaient causer
une écriture hors limites et un dépassement de tampon de tas.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.06-3~deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets grub2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de grub2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/grub2">\
https://security-tracker.debian.org/tracker/grub2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3605.data"
# $Id: $
