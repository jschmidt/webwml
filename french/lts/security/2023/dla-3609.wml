#use wml::debian::translation-check translation="6f700544432ad9790d4c35144ef67a9442718948" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le paquet prometheus-alertmanager, un composant de Prometheus, une
application utilisée pour la supervision d’évènements et les alertes, était
vulnérable à une attaque XSS permanent.</p>

<p>Alertmanager gère les alertes envoyées par les applications clientes telles
que le serveur Prometheus. Un attaquant avec la permission de réaliser des
requêtes POST sur le point terminal /api/v1/alerts pouvait exécuter du code
JavaScript arbitraire sur les utilisateurs de Prometheus Alertmanager.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.15.3+ds-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets prometheus-alertmanager.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de prometheus-alertmanager,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/prometheus-alertmanager">\
https://security-tracker.debian.org/tracker/prometheus-alertmanager</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3609.data"
# $Id: $
