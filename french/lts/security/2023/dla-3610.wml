#use wml::debian::translation-check translation="edfbbbaac89b881ab11280d834fc68b2654326ca" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités de sécurité ont été trouvées dans python-urllib3, une
bibliothèque HTTP avec réserve de connexions <q>thread-safe</q> pour Python, qui
pouvaient conduire à une divulgation d'informations ou à un contournement
d’autorisation.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25091">CVE-2018-25091</a>

<p>Yoshida Katsuhiko a découvert que le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-20060">CVE-2018-20060</a>
ne couvrait pas les en-têtes de requête ne suivant pas la casse de titre. Par
exemple, les en-têtes de requête <code>authorization</code> n’étaient pas
retirés lors de redirections multi-origines (selon la
<a href="https://datatracker.ietf.org/doc/html/rfc7230#section-3.2">RFC7230 sec. 3.2</a>, les champs d’en-tête doivent être traités indépendamment de la casse).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11236">CVE-2019-11236</a>

<p>Hanno Böck a découvert qu’un attaquant contrôlant le paramètre de requête
pouvait injecter des en-têtes en injectant des caractères CR/LF. Le problème est
similaire au
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>
de CPython.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11324">CVE-2019-11324</a>

<p>Christian Heimes a découvert que lors de la vérification de connexions HTTPS
en passant un <code>SSLContext</code> à urllib3, les certificats CA du système
étaient chargés dans <code>SSLContext</code> par défaut en plus de ceux ajoutés
manuellement. Cela faisait que les initialisations de connexion TLS qui devaient
avorter selon les certificats spécifiés manuellement réussissaient en se basant
sur les certificats CA du système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26137">CVE-2020-26137</a>

<p>Il a été découvert qu’une injection CRLF était possible si l’attaquant
contrôlait la méthode de requête HTTP, comme le montrait l’insertion de caractères
de contrôle CR et LF dans le premier argument de <code>putrequest()</code>.
Ce problème est similaire au <a href="https://security-tracker.debian.org/tracker/CVE-2020-26116">CVE-2020-26116</a> de urllib.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43804">CVE-2023-43804</a>

<p>Il a été découvert qu’un en-tête de requête <code>Cookie</code> n’est pas
dépouillé lors d'une redirection multi-origine. Il était par conséquent possible
pour un utilisateur spécifiant un en-tête <code>Cookie</code> de faire fuiter
sans s’en apercevoir des informations à l’aide de redirections HTTP vers une
origine différente (à moins que l’utilisateur ne désactive explicitement les
redirections). Le problème est similaire au
 <a href="https://security-tracker.debian.org/tracker/CVE-2018-20060">CVE-2018-20060</a>,
mais pour l’en-tête <code>Cookie</code> plutôt que <code>Authorization</code>.
</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.24.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-urllib3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-urllib3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-urllib3">\
https://security-tracker.debian.org/tracker/python-urllib3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3610.data"
# $Id: $
