#use wml::debian::translation-check translation="75867dc15d137d7c31059fc6e4de8d5e17106647" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Python 3.7.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48560">CVE-2022-48560</a>

<p>Un problème d’utilisation de mémoire après libération a été découvert dans la
fonction heappushpop dans le module heapq.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48564">CVE-2022-48564</a>

<p>Une vulnérabilité potentielle de déni de service a été découverte dans la
fonction read_ints utilisée lors du traitement de certains fichiers mal formés de
listes de propriétés d’Apple au format binaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48565">CVE-2022-48565</a>

<p>Un problème d’entités externes XML (XXE) a été découvert. Dans le but
d’éviter des vulnérabilités possibles, le module plistlib n’accepte plus les
déclarations d’entités dans des fichiers plist XML.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48566">CVE-2022-48566</a>

<p>Des optimisations possibles d’annulation de temps constant
(constant-time-defeating) ont été découvertes dans la variable d’accumulateur
dans hmac.compare_digest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40217">CVE-2023-40217</a>

<p>Il a été découvert qu’il était possible de contourner certaines des
protections mises en œuvre par l’initialisation de connexion TLS dans la classe
ssl.SSLSocket. Par exemple, des données non authentifiées pouvaient être lues
par un programme attendant des données authentifiées par un certificat de
client.</p></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.7.3-2+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python3.7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python3.7,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python3.7">\
https://security-tracker.debian.org/tracker/python3.7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3614.data"
# $Id: $
