#use wml::debian::translation-check translation="dd25e149396ddc19f12d22967858766bf74d0991" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle d’injection
de code dans <code>org-mode</code>, une extension populaire pour l’éditeur de
texte Emacs.</p>

<p>Des attaquants pouvaient exécuter des commandes arbitraires d’interpréteur
à l'aide d'un nom de fichier (ou un nom de répertoire) contenant des
métacaractères.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28617">CVE-2023-28617</a>

<p>org-babel-execute:latex dans ob-latex.el, dans le mode Org jusqu’à la
version 9.6.1 pour GNU Emacs, permettait à des attaquants d’exécuter des
commandes arbitraires à l’aide d’un nom de fichier ou de répertoire contenant
des métacaractères d’interpréteur.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 9.1.14+dfsg-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets org-mode.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3616.data"
# $Id: $
