#use wml::debian::translation-check translation="7eacf61bf7985f7b04942402268c5c6364c37abf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de vérification hors limite dans la fonction <q>dsaVerify</q> a
été découvert dans node-browserify-sign. Il permettait à un attaquant de
construire des signatures pouvant être vérifiées avec succès par n’importe
quelle clé publique, autorisant une attaque par contrefaçon de signature.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 4.0.4-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-browserify-sign.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-browserify-sign,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-browserify-sign">\
https://security-tracker.debian.org/tracker/node-browserify-sign</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3635.data"
# $Id: $
