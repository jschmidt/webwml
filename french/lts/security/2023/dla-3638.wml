#use wml::debian::translation-check translation="cd5f7c8e9ce92c7ca740b0da2976f9533803e4b5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été identifiée dans h2o, un serveur web de haute
performance avec prise en charge d’HTTP/2.</p>

<p>Une vulnérabilité de sécurité
<a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>
a été découverte qui pouvait éventuellement être exploité pour
perturber les opérations de serveur.</p>

<p>La vulnérabilité dans le serveur HTTP/2 h2o concernait le traitement de
certains types de requêtes HTTP/2. Dans certains scénarios, un attaquant pouvait
envoyer une série de requêtes malveillantes, amenant le serveur à les traiter
rapidement et épuisant les ressources du système.</p>

<p>Le correctif appliqué par l’amont modifie l’ABI. Par conséquent, si une
application est construite avec n’importe quelle bibliothèque partagée de h2o,
elle doit être reconstruite. Aucun paquet de Debian n’est affecté.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.2.5+dfsg2-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets h2o.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de h2o,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/h2o">\
https://security-tracker.debian.org/tracker/h2o</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3638.data"
# $Id: $
