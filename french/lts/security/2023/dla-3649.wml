#use wml::debian::translation-check translation="d4d3e4aed4991092df23c3f6b8d39208da8673c4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que python-urllib3, une bibliothèque HTTP conviviale
cliente pour Python, ne supprimait pas le corps de requête HTTP quand une
réponse de redirection HTTP avec un état 301, 302 ou 303 après la requête avait
sa méthode changée en une qui pouvait accepter un corps de requête, tel que POST
vers GET, comme requis par les RFC HTTP. Cela pouvait conduire à une divulgation
d'informations.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.24.1-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-urllib3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-urllib3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-urllib3">\
https://security-tracker.debian.org/tracker/python-urllib3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3649.data"
# $Id: $
