#use wml::debian::translation-check translation="19fdc288616ee3bfe6ee122b16cd10940121ffb2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Harry Sintonen de F-Secure Corporation a découvert plusieurs
vulnérabilités dans OpenSSH, une implémentation du protocole SSH. Toutes
les vulnérabilités ont été découvertes dans le client scp implémentant le
protocole SCP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20685">CVE-2018-20685</a>

<p>Du fait d'une mauvaise validation du nom de répertoire, le client scp
permet aux serveurs de modifier les droits du répertoire cible en utilisant
un nom vide ou constitué d'un point (« dot directory »).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6109">CVE-2019-6109</a>

<p>Du fait de l'absence d'encodage des caractères dans l'affichage de la
progression, le nom d'objet peut être utilisé pour manipuler la sortie du
client, par exemple pour employer des codes ANSI afin de cacher le
transfert de fichiers supplémentaires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6111">CVE-2019-6111</a>

<p>Du fait d'une validation insuffisante des entrées du client scp des noms
de chemin envoyés par le serveur, un serveur malveillant peut effectuer
l'écrasement arbitraire de fichiers dans le répertoire cible. Si l'option
de récursivité (-r) est fournie, le serveur peut aussi manipuler des
sous-répertoires.</p>

<p>La vérification ajoutée à cette version peut conduire à une régression
si le client et le serveur ont des règles d'expansion des caractères
génériques différentes. Si la confiance dans le serveur, pour cette option,
est garantie la vérification peut être désactivée en passant la nouvelle
option -T au client scp.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 1:7.4p1-10+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssh">\
https://security-tracker.debian.org/tracker/openssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4387.data"
# $Id: $
