#use wml::debian::translation-check translation="bf3aefbd9be97d0ca8c1685ef203e418a303094c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le serveur
mandataire et de cache Squid :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28116">CVE-2021-28116</a>

<p>Amos Jeffries a découvert une fuite d'informations si WCCPv2 est activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46784">CVE-2021-46784</a>

<p>Joshua Rogers a découvert qu'une erreur dans l'analyse de réponses d'un
serveur Gopher peut avoir pour conséquence un déni de service.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 4.6-1+deb10u7.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 4.13-10+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de squid, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/squid">\
https://security-tracker.debian.org/tracker/squid</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5171.data"
# $Id: $
