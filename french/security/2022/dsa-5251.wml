#use wml::debian::translation-check translation="6756ceee05381fa63bc4c02e4e26bd4902cb1ac4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le client, relais et
serveur DHCP ISC.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2928">CVE-2022-2928</a>

<p>Le serveur DHCP ne réalisait pas correctement le compte de références
d'option quand il est configuré avec <q>allow leasequery;</q>. Un attaquant
distant pouvait tirer avantage de ce défaut pour provoquer un déni de
service (plantage du démon).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2929">CVE-2022-2929</a>

<p>Le serveur DHCP était prédisposé à un défaut de fuite de mémoire lors du
traitement du contenu des données de l'option 81 (fqdn) reçues dans un
paquet DHCP. Un attaquant distant pouvait tirer avantage de ce défaut pour
provoquer une consommation excessive de ressources par les serveurs DHCP,
avec pour conséquence un déni de service.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 4.4.1-2.3+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets isc-dhcp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de isc-dhcp, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/isc-dhcp">\
https://security-tracker.debian.org/tracker/isc-dhcp</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5251.data"
# $Id: $
