#use wml::debian::translation-check translation="15ae297ff58b032ad0fc7280082e2effe1260f80" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Gregor Kopf de Secfault Security GmbH a découvert que HSQLDB, un moteur
de base de données SQL Java, permettait l'exécution de commandes de script
frauduleuses dans les fichiers .script et .log. HSQLDB prend en charge un
mot-clé <q>SCRIPT</q> qui est normalement utilisé pour enregistrer
l'entrée des commandes par l'administration de la base de données pour
produire ce type de script. En combinaison avec LibreOffice, un attaquant
pouvait contrefaire un fichier odb contenant un fichier <q>database/script</q>
qui lui-même contenait une commande SCRIPT où le contenu du fichier pouvait
être écrit dans un nouveau fichier dont l'emplacement était déterminé par
l'attaquant.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 1.8.0.10+dfsg-10+deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ce problème a été corrigé dans
la version 1.8.0.10+dfsg-11+deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets hsqldb1.8.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de hsqldb1.8.0,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/hsqldb1.8.0">\
https://security-tracker.debian.org/tracker/hsqldb1.8.0</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5436.data"
# $Id: $
