#use wml::debian::translation-check translation="2479bf20af4bbeac5b1e2120345e9ccb1e4269d7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans Asterisk, un autocommutateur téléphonique
privé (PBX) au code source ouvert. Une vulnérabilité de dépassement de
tampon affecte les utilisateurs qui utilisent le résolveur DNS PJSIP. Cette
vulnérabilité est liée au
<a href="https://security-tracker.debian.org/tracker/CVE-2022-24793">CVE-2022-24793</a>,
à la différence que ce problème réside dans l'analyse de l'enregistrement
de requête <q>parse_query()</q> tandis que le problème dans
<a href="https://security-tracker.debian.org/tracker/CVE-2022-24793">CVE-2022-24793</a>
est dans <q>parse_rr()</q>. Un contournement consiste à désactiver la
résolution DNS dans la configuration de PJSIP (en réglant
<q>nameserver_count</q>" à zéro) ou à utiliser à la place une implémentation
externe de résolveur.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 1:16.28.0~dfsg-0+deb11u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de asterisk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/asterisk">\
https://security-tracker.debian.org/tracker/asterisk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5438.data"
# $Id: $
