#use wml::debian::translation-check translation="ee975366116a1b862ff69cc041c1533dd5832e5d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3390">CVE-2023-3390</a>

<p>Un défaut d'utilisation de mémoire après libération dans le sous-système
netfilter, provoqué par le traitement incorrect d'un chemin d'erreur,
pouvait avoir pour conséquences un déni de service ou une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3610">CVE-2023-3610</a>

<p>Un défaut d'utilisation de mémoire après libération dans le sous-système
netfilter, provoqué par le traitement incorrect de refcount sur le chemin
de <q>destroy</q> de table et <q>chain</q> pouvait avoir pour conséquences
un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20593">CVE-2023-20593</a>

<p>Tavis Ormandy a découvert que, sous des circonstances particulière liées
à la microarchitecture, un registre de vecteur dans les processeurs
<q>Zen 2</q> pouvait ne pas être mis à zéro correctement. Ce défaut permet
à un attaquant de divulguer le contenu du registre à travers des processus
concurrents, des hyper flux et des clients virtualisés.</p>

<p>Pour plus de détails veuillez consulter
<a href="https://lock.cmpxchg8b.com/zenbleed.html">\
https://lock.cmpxchg8b.com/zenbleed.html</a> et
<a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">\
https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a>.</p>

<p>Ce problème peut aussi être atténué par une mise à jour du microcode au
moyen du paquet amd64-microcode ou la mise à jour du microprogramme du
système (BIOS/UEFI). Cependant,la version initiale du microcode d'AMD ne
fournit des mises à jour que pour les processeurs EPYC de deuxième
génération : divers processeurs Ryzen sont aussi affectés, mais les mises à
jour ne sont pas encore disponibles.</p>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 5.10.179-3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5461.data"
# $Id: $
