#use wml::debian::translation-check translation="c6b3ef24182164bd9de0036f0ae7c8ac84319d6e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38133">CVE-2023-38133</a>

<p>YeongHyeon Choi a découvert que le traitement d'un contenu web peut
divulguer des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38572">CVE-2023-38572</a>

<p>Narendra Bhati a découvert qu'un site web peut être capable de
contourner la politique de même origine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38592">CVE-2023-38592</a>

<p>Narendra Bhati, Valentino Dalla Valle, Pedro Bernardo, Marco Squarcina
et Lorenzo Veronese ont découvert que le traitement d'un contenu web peut
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38594">CVE-2023-38594</a>

<p>Yuhao Hu a découvert que le traitement d'un contenu web peut conduire à
l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38595">CVE-2023-38595</a>

<p>Un chercheur anonyme, Jiming Wang et Jikai Ren ont découvert que le
traitement d'un contenu web peut conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38597">CVE-2023-38597</a>

<p>Junsung Lee a découvert que le traitement d'un contenu web peut conduire
à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38599">CVE-2023-38599</a>

<p>Hritvik Taneja, Jason Kim, Jie Jeff Xu, Stephan van Schaik, Daniel
Genkin et Yuval Yarom ont découvert qu'un site web peut être capable de
suivre des informations sensibles de l'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38600">CVE-2023-38600</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web peut
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38611">CVE-2023-38611</a>

<p>Francisco Alonso a découvert que le traitement d'un contenu web peut
conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 2.40.5-1~deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 2.40.5-1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5468.data"
# $Id: $
