#use wml::debian::translation-check translation="51f6249603067a1b92d30d72bb5aac395d810fe6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la
servlet Tomcat et le moteur JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28709">CVE-2023-28709</a>

<p>Déni de service. Si une configuration différente de celle par défaut du
connecteur HTTP était utilisée, telle que la taille définie par l'attribut
maxParameterCount pouvait être atteinte en utilisant les paramètres d'une
chaîne de requête et qu'une requête soumise fournissait exactement des
paramètres de la taille de maxParameterCount dans une chaîne de requête, la
limite de taille des parties de requête envoyées pouvait être contournée
avec la possibilité qu'un déni de service survienne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41080">CVE-2023-41080</a>

<p>Redirection ouverte. Si l'application web <q>ROOT</q> (par défaut) était
configurée pour utiliser l'authentification <q>FORM</q>, alors il était
possible qu'une URL contrefaite pour l'occasion soit utilisée pour
déclencher une redirection vers une URL choisie par les attaquants.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-42795">CVE-2023-42795</a>

<p>Divulgation d'informations. Lors du recyclage de divers objets internes
comprenant la requête et la réponse, avant la réutilisation par la requête
ou la réponse suivante, une erreur pouvait faire que Tomcat saute quelques
étapes du processus de recyclage, menant à une fuite d'informations à
partir de la requête ou de la réponse actuelle vers la suivante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>

<p>Déni de service provoqué par une surcharge de trame HTTP/2 (attaque de
type <q>Rapid Reset</q>).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45648">CVE-2023-45648</a>

<p>Dissimulation de requête. Tomcat n'analysait pas correctement les
en-têtes de fin HTTP. Un en-tête de fin non valable, contrefait pour
l'occasion, pouvait faire que Tomcat traite une requête unique comme des
requêtes multiples, menant à une possible dissimulation de requête quand il
se trouvait derrière un mandataire inverse.</p></li>

</ul>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 10.1.6-1+deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat10, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tomcat10">\
https://security-tracker.debian.org/tracker/tomcat10</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5521.data"
# $Id: $
