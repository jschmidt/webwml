#use wml::debian::translation-check translation="7aa48410922eecd94f62622425399bea87336e9a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la
servlet Tomcat et le moteur JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24998">CVE-2023-24998</a>

<p>Déni de service. Tomcat utilise une copie empaquetée renommée de Commons
FileUpload d'Apache pour fournir la fonction d'envoi de ficher définie dans
la spécification de la Servlet Jakarta. Apache Tomcat était donc aussi
exposé à la vulnérabilité de Commons FileUpload
<a href="https://security-tracker.debian.org/tracker/CVE-2023-24998">CVE-2023-24998</a>
comme il n'y avait pas de limite au nombre de parties de requête traitées.
Cela avait pour conséquence la possibilité pour un attaquant de déclencher
un déni de service grâce à un envoi malveillant ou une série d'envois.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41080">CVE-2023-41080</a>

<p>Redirection ouverte. Si l'application web <q>ROOT</q> (par défaut) était
configurée pour utiliser l'authentification <q>FORM</q>, alors il était
possible qu'une URL contrefaite pour l'occasion soit utilisée pour
déclencher une redirection vers une URL choisie par les attaquants.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-42795">CVE-2023-42795</a>

<p>Divulgation d'informations. Lors du recyclage de divers objets internes
comprenant la requête et la réponse, avant la réutilisation par la requête
ou la réponse suivante, une erreur pouvait faire que Tomcat saute quelques
étapes du processus de recyclage, menant à une fuite d'informations à
partir de la requête ou de la réponse actuelle vers la suivante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>

<p>Déni de service provoqué par une surcharge de trame HTTP/2 (attaque de
type <q>Rapid Reset</q>).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45648">CVE-2023-45648</a>

<p>Dissimulation de requête. Tomcat n'analysait pas correctement les
en-têtes de fin HTTP. Un en-tête de fin non valable, contrefait pour
l'occasion, pouvait faire que Tomcat traite une requête unique comme des
requêtes multiples, menant à une possible dissimulation de requête quand il
se trouvait derrière un mandataire inverse.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 9.0.43-2~deb11u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5522.data"
# $Id: $
