#use wml::debian::translation-check translation="9a44700986db597a72de83706f8876f24687e02b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités exploitables à distance ont été découvertes dans
Jetty 9, un serveur web basé sur Java et un moteur de servlet.
L'implémentation du protocole HTTP/2 ne vérifiait pas suffisamment si les
valeurs de l'en-tête HPACK excédaient la limite de taille. De plus, le
protocole HTTP/2 permettait un déni de service (consommation des ressources
du serveur) parce que des annulations de requêtes pouvaient réinitialiser
rapidement beaucoup de flux. Ce problème est aussi connu sous le nom de
<q>Rapid Reset Attack</q>.</p>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 9.4.50-4+deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 9.4.50-4+deb12u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jetty9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jetty9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jetty9">\
https://security-tracker.debian.org/tracker/jetty9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5540.data"
# $Id: $
