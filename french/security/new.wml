#use wml::debian::template title="Informations de sécurité" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7f22cca3566a4338b2de8ef3bf79e4dbeb5d91be" maintainer="Jean-Pierre Giraud"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Garder un système Debian sûr</a></li>
<li><a href="#DSAS">Annonces récentes</a></li>
<li><a href="#infos">Les sources des informations de sécurité</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian prend les questions de sécurité très au sérieux. Nous traitons tous les problèmes de sécurité qui sont portés à notre attention et nous nous assurons qu'ils sont corrigés dans un délai raisonnable.</p>
</aside>

<p>
L'expérience a montré que <q>la sécurité par le secret</q> ne fonctionne
jamais. Une diffusion publique des problèmes de sécurité apporte plus
rapidement de meilleures solutions. Dans cet esprit, cette page indique l'état
de Debian sur différents trous de sécurité connus, qui pourraient
potentiellement affecter le système d'exploitation Debian.
</p>

<p>
Le projet Debian gère de nombreuses annonces de sécurité en coordination avec
les autres distributeurs de logiciel libre, et par conséquent, ces annonces
sont publiées le même jour que la vulnérabilité associée. Pour obtenir les
dernières informations de sécurité de Debian, abonnez-vous à la liste de
diffusion (en anglais) :
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

# "reasonable timeframe" might be too vague, but we don't have
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.


<p>
Debian participe aussi aux efforts de standardisation en matière de sécurité&nbsp;:
</p>

<ul>
  <li>Les <a href="#DSAS">annonces de sécurité Debian</a> (<i>Debian Security
Advisories</i>) sont <a href="cve-compatibility">compatibles avec le CVE</a>
(référez-vous aux <a href="crossreferences">renvois croisés</a>).</li>
  <li>Debian <a href="oval/">publie</a> ses informations de sécurité en
utilisant le langage <a href="https://github.com/CISecurity/OVALRepo">OVAL</a>
(Open Vulnerability Assessment Language).</li>
</ul>


<h2><a id="keeping-secure">Garder un système Debian sûr</a></h2>

<p>
Le paquet <a href="https://packages.debian.org/stable/admin/unattended-upgrades">\
unattended-upgrades</a> est installé avec l'environnement de bureau GNOME
et garde automatiquement l'ordinateur à jour des dernières mises à jour de
sécurité (et d'autres).

La <a href="https://wiki.debian.org/UnattendedUpgrades">page du wiki</a>
contient des informations détaillées sur la configuration manuelle
du paquet <tt>unattended-upgrades</tt>.

<p>
Pour plus d'informations sur les problèmes de sécurité dans Debian, veuillez
consulter la FAQ et la documentation :
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">FAQ de sécurité</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Manuel de sécurisation de Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Annonces récentes</a></h2>

<p>Vous trouverez ici les Annonces de sécurité récentes de (Debian Security
Advisories - DSA) postées sur la liste
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
<br><b>T</b> est le lien vers les informations du
<a href="https://security-tracker.debian.org/tracker">Debian Security Bug Tracker</a>,
le numéro de DSA est le lien vers le courriel de l'annonce.

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<h2><a id="infos">Les sources des informations de sécurité</a></h2>
#include "security-sources.inc"

<p>
Les dernières informations de sécurité sont aussi disponibles sous la forme
de <a href="dsa.fr.rdf">fichiers RDF</a>. Nous vous proposons également une
<a href="dsa-long.fr.rdf">version légèrement plus longue</a> des fichiers qui
contient le premier paragraphe de l'alerte en question.
</p>
