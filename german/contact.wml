#use wml::debian::template title="Wie Sie sich mit uns in Verbindung setzen" NOCOMMENTS="yes" MAINPAGE="true"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9"
# Translator: Thimo Neubauer <thimo@debian.org>
# Updated: Holger Wansing <linux@wansing-online.de>, 2011 - 2013.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020, 2022.


<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">Allgemeine Informationen</a>
  <li><a href="#installuse">Installation und Anwendung von Debian</a>
  <li><a href="#press">Öffentlichkeitsarbeit &amp; Presse</a>
  <li><a href="#events">Veranstaltungen &amp; Konferenzen</a>
  <li><a href="#helping">Debian helfen</a>
  <li><a href="#packageproblems">Probleme in Debian-Paketen melden</a>
  <li><a href="#development">Debian entwickeln</a>
  <li><a href="#infrastructure">Probleme mit der Debian-Infrastruktur</a>
  <li><a href="#harassment">Probleme mit Mobbing/Belästigung</a>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Wir bitten darum, dass anfängliche Anfragen
   auf <strong>Englisch</strong> gestellt werden, da dies die Sprache ist, die die meisten
   von uns sprechen. Falls das nicht möglich ist, gehen Sie bitte über eine unserer
   <a href="https://lists.debian.org/users.html#debian-user">Benutzer-Mailinglisten</a>, die in
   vielen unterschiedlichen Sprachen zur Verfügung stehen.</p>
</aside>

<p>Debian ist eine große Organisation, und es gibt viele Wege, mit Projektmitgliedern und anderen
   Debian-Nutzern in Kontakt zu treten. Diese Seite fasst die am häufigsten angefragten Möglichkeiten
   zur Kontaktierung zusammen; sie ist keinesfalls vollständig. Bitte lesen Sie den Rest der Webseiten
   für weitere Möglichkeiten zur Kontaktaufnahme.
</p>

<p>Bitte beachten Sie, dass hinter den meisten unten aufgeführten E-Mail-Adressen offene Mailinglisten mit
   öffentlichem Archiv stehen. Lesen Sie den <a href="$(HOME)/MailingLists/disclaimer">\
   Haftungsausschluss (Disclaimer)</a>, bevor Sie irgendwelche Nachrichten schicken.
</p>

<h2 id="generalinfo">Allgemeine Informationen</h2>

<p>Die meisten Informationen über Debian sind auf unseren Webseiten
   (<a href="$(HOME)">https://www.debian.org/</a>) gesammelt, daher lesen und
   <a href="$(SEARCH)">durchsuchen</a> Sie sie, bevor Sie uns kontaktieren.
</p>

<p>Sie können Fragen zum Debian-Projekt an die Mailingliste
   <email debian-project@lists.debian.org> schicken. Bitte stellen Sie auf diesem Wege aber keine
   Fragen zu Linux allgemein; diesbezüglich haben wir weiter unten Informationen zu anderen
   Mailinglisten.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">Lesen Sie unsere FAQ</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Installation und Anwendung von Debian</h2>

<p>Falls Sie sicher sind, dass die Dokumentation auf dem Installationsmedium und
   unserer Website keine Lösung für Ihr Problem enthält, gibt es sehr aktive
   Benutzer-Mailinglisten, auf denen Benutzer und Entwickler Ihre Fragen
   beantworten. Hier können Sie alle Fragen zu folgenden Themen stellen:
</p>

<ul>
  <li>Installation</li>
  <li>Konfiguration</li>
  <li>Unterstützte Hardware</li>
  <li>Administration von Rechnern</li>
  <li>Verwendung von Debian</li>
</ul>

<p>Bitte abonnieren Sie die deutsche-sprachige Liste
   <a href="https://lists.debian.org/debian-user-german/">debian-user-german</a>
   und schicken Sie Ihre Fragen an <email debian-user-german@lists.debian.org>.
</p>

<p>Alternativ gibt es auch noch Benutzer-Mailinglisten für andere Sprachen.
   Schauen Sie auf die Seite für <a href="https://lists.debian.org/users.html#debian-user">verschiedene
   internationale Benutzer-Mailinglisten</a>.
</p>

<p>Sie können die <a href="https://lists.debian.org/">Archive unserer Mailinglisten</a> durchforsten
   oder über ein Suchformular <a href="https://lists.debian.org/search.html">durchsuchen</a>, ohne eine Liste abonniert
   zu haben.
</p>

<p>Des Weiteren können Sie unsere Mailinglisten auch als Newsgroups lesen.
</p>

<p>Wenn Sie glauben, einen Fehler in unserem Installationssystem gefunden zu haben,
   schicken Sie die Informationen dazu bitte an <email debian-boot@lists.debian.org>.
   Sie können auch einen <a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">Fehlerbericht</a> gegen das Pseudo-Paket <a
   href="https://bugs.debian.org/debian-installer">debian-installer</a> einreichen.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">Öffentlichkeitsarbeit &amp; Presse</h2>

<p>Das <a href="https://wiki.debian.org/Teams/Publicity">Debian Publicity Team</a> moderiert News und Ankündigungen
   in allen offiziellen Debian-Informationskanälen, z.B. Mailinglisten, dem Blog, der Micronews-Website
   sowie unseren offiziellen Social-Media-Kanälen.
</p>

<p>Wenn Sie über Debian schreiben möchten und dazu Hilfe oder Informationen benötigen,
   setzen Sie sich bitte mit unserer <a href="mailto:press@debian.org">Presseabteilung</a> in Verbindung.
   Dies ist auch die richtige Adresse, falls Sie planen, einen Beitrag für unsere eigene
   Neuigkeiten-Seite einzureichen.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Veranstaltungen &amp; Konferenzen</h2>

<p>Einladungen zu <a href="$(HOME)/events/">Konferenzen</a>, Messen und anderen Veranstaltungen
   senden Sie bitte an <email events@debian.org>.
</p>

<p>Anfragen bezüglich Flugblättern, Postern und zur Teilnahme an Veranstaltungen in Europa
   sollten an die <email debian-events-eu@lists.debian.org>-Mailingliste gesendet werden.
</p>

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Debian helfen</h2>

<p>Es gibt viele Möglichkeiten, das Debian-Projekt zu unterstützen und zu unserer Distribution
   beizutragen. Wenn Sie Hilfe anbieten möchten, informieren Sie sich bitte zuerst
   auf unserer Website unter <a href="devel/join/">Wie Sie zum Projekt beitragen können</a>.
</p>

<p>Wenn Sie einen Debian-Spiegel betreiben möchten, lesen Sie die Seiten über
   das <a href="mirror/">Spiegeln von Debian</a>. Angebote für neue Spiegel-Server
   können über <a href="mirror/submit">dieses Formular</a> eingereicht werden.
   Probleme mit bestehenden Spiegeln melden Sie uns bitte über <email mirrors@debian.org>.
</p>

<p>Wenn Sie Debian-CDs verkaufen möchten, lesen Sie die
   <a href="CD/vendors/info">Informationen für CD-Verkäufer</a>. Um in die Liste der
   CD-Verkäufer aufgenommen zu werden, verwenden Sie bitte
   <a href="CD/vendors/adding-form">dieses Formular</a>.
</p>

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">Probleme in Debian-Paketen melden</h2>

<p>Wenn Sie einen Fehler in einem Debian-Paket melden möchten, betreiben wir hierfür
   eine Fehlerdatenbank, über die Sie auf einfache Weise Ihr Problem berichten können. 
   Bitte lesen Sie dazu die <a href="Bugs/Reporting">Anweisungen zum Senden eines Fehlerberichts</a>.
</p>

<p>Möchten Sie sich einfach nur mit dem Betreuer eines Debian-Paketes in Verbindung
   setzen, dann können Sie das spezielle Mail-Alias verwenden, das für
   jedes Paket eingerichtet ist. Jede Mail an
   &lt;<var>Paketname</var>&gt;@packages.debian.org wird an den für das Paket
   zuständigen Betreuer weitergeleitet.
</p>

<p>Falls Sie die Debian-Entwickler diskret auf eine Sicherheitslücke hinweisen möchten, 
   schicken Sie bitte eine E-Mail an <email security@debian.org>.
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Debian entwickeln</h2>

<p>Wenn Sie Fragen haben, die sich eher um die Entwicklung drehen, gibt es
   einige <a href="https://lists.debian.org/devel.html">Entwickler-Mailinglisten</a>,
   die Sie verwenden können, um unsere Entwickler zu kontaktieren.
</p>

<p>Wir haben auch eine allgemeine Entwicklerliste: <email debian-devel@lists.debian.org>.
   Bitte folgen Sie diesem <a href="https://lists.debian.org/debian-devel/">Link</a>, um sie
   zu abonnnieren.
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Probleme mit der Debian-Infrastruktur</h2>

<p>Um Probleme mit einem Debian-Service zu berichten, können Sie üblicherweise
   <a href="Bugs/Reporting">einen Fehlerbericht</a> gegen das entsprechende
   <a href="Bugs/pseudo-packages">Pseudo-Paket</a> einreichen.
</p>

<p>Alternativ können Sie auch eine E-Mail an eine der folgenden Adressen schicken:
</p>

<define-tag btsurl>package: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Bearbeiter der Webseiten:</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>

#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Übersetzer der Webseiten:</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">

<dt>Administratoren für die Mailinglisten und Archivbetreuer:</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>

<dt>Administratoren der Fehlerdatenbank:</dt>
  <dd><btsurl bugs.debian.org><br /> 
      <email owner@bugs.debian.org></dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">Probleme mit Mobbing/Belästigung</h2>

<p>Debian ist eine Gemeinschaft von Leuten, für die Respekt einen hohen Stellenwert hat.
   Falls Sie unter unangemessenem Verhalten leiden, oder sich verletzt oder belästigt fühlen,
   kontaktieren Sie bitte unser Community-Team über <email community@debian.org>.
</p>
