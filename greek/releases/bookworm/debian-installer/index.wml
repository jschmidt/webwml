#use wml::debian::template title="Πληροφορίες εγκατάστασης του Debian &ldquo;bookworm&rdquo;" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#use wml::debian::translation-check translation="1f438c4027761acce77e56dc1e4a4fc8e3353760" maintainer="galaxico"

<h1>Εγκαθιστώντας το Debian <current_release_bookworm></h1>

<if-stable-release release="trixie">
<p><strong>Debian 12 has been superseded by
<a href="../../trixie/">Debian 13 (<q>trixie</q>)</a>. Some of these
installation images may no longer be available, or may no longer work, and
you are recommended to install trixie instead.
</strong></p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>
For installation images and documentation about how to install <q>bookworm</q>
(which is currently Testing), see
<a href="$(HOME)/devel/debian-installer/">the Debian-Installer page</a>.
</if-stable-release>

<if-stable-release release="bookworm">
<p>
<strong>Για να εγκαταστήσετε το Debian</strong> <current_release_bookworm>
(<em>bookworm</em>), μεταφορτώστε οποιαδήποτε από τις ακόλουθες εικόνες (όλες οι εικόνες
CD/DVD για i386 και amd64 μπορούν να χρησιμοποιηθούν και σε κλειδιά USB):
</p>

<div class="line">
<div class="item col50">
	<p><strong>εικόνα netinst CD </strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>πλήρες σετ CD</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>πλήρες σετ DVD</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (μέσω <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (μέσω <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>άλλες εικόνες (netboot, ευέλικτα κλειδιά usb , κλπ.)</strong></p>
<other-images />
</div>
</div>



<p>
<strong>Σημειώσεις</strong>
</p>
<ul>
    <li>
	Για να κατεβάσετε πλήρεις εικόνες CD και DVD συνιστάται η χρήση BitTorrent ή jigdo.
    </li><li>
	Για τις λιγότερο συνηθισμένες αρχιτεκτονικές, είναι διαθέσιμος ένας περιορισμένος μόνο αριθμός
	εικόνων από τα σετ CD και DVD ως αρχείο ISO ή μέσω BitTorrent.
	Τα πλήρη σετ είναι διαθέσιμα μόνο μέσω The full sets are only available via jigdo.
    </li><li>
	Για τις εικόνες εγκατάστασης, είναι διαθέσιμα αρχεία επαλήθευσης (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> και άλλα) από τον ίδιο κατάλογο στον οποίο βρίσκονται και οι εικόνες.
    </li>
</ul>


<h1>Τεκμηρίωση</h1>

<p>
<strong>Αν χρειάζεστε ένα μοναδικό κείμενο </strong> πριν την εγκατάσταση, διαβάστε το
<a href="../i386/apa">Το πώς της Εγκατάστασης</a>, μια γρήγορη περιήγηση στη διαδικασία
εγκατάστασης. Άλλες πηγές χρήσιμης τεκμηρίωσης περιλαμβάνουν:
</p>

<ul>
<li><a href="../installmanual">Οδηγός Εγκατάστασης της έκδοσης Bookworm</a><br />
λεπτομερείς οδηγίες εγκατάστασης</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Συχνές ερωτήσεις του Εγκαταστάτη του Debian</a>
και <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
συχνές ερωτήσεις και απαντήσεις</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki του Εγκαταστάτη του Debian</a><br />
τεκμηρίωση που συντηρείται από την κοινότητα</li>
</ul>

<h1 id="errata">Παροράματα</h1>

<p>
Αυτή είναι μια λίστα γνωστών προβλημάτων του Εγκαταστάτη που έρχεται
με την έκδοση Debian <current_release_bookworm>. Άν έχετε εμπειρία ενός προβλήματος
εγκαθιστώντας το Debian και δεν το βλέπετε να αναφέρεται εδώ, παρακαλούμε στείλτε μας 
μια <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">αναφορά εγκατάστασης</a>
περιγράφοντας το πρόβλημα ή 
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">ελέγξτε το wiki</a>
για άλλα γνωστά προβλήματα.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Παροράματα για την έκδοση 12.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
Βελτιωμένες εκδόσεις του συστήματος εγκαταστήσετες αναπτύσσονται για την επόμενη 
έκδοση του Debian, και μπορούν επίσης να χρησιμοποιηθούν για να εγκαταστήσετε την 
έκδοση bookworm. Για λεπτομέρειες δείτε τη σελίδα 
<a href="$(HOME)/devel/debian-installer/">Σελίδα του σχεδίου του Εγκαταστάτη του Debian</a>.
</p>
</if-stable-release>
