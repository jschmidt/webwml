#use wml::debian::cdimage title="Immagini live installabili"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="18c4b33fe6dcee4c5cb83603609a9b4bd481da0c" maintainer="Giuseppe Sacco"

#include "$(ENGLISHDIR)/releases/images.data"

<p>Un'immagine <q>live installabile</q> contiene un sistema Debian in grado di
avviarsi senza modificare alcun file sul disco fisso, e permette anche
l'installazione di Debian a partire dal contenuto dell'immagine stessa.</p>

<p><a name="choose_live"><strong>È conveniente usare un'immagine live?</strong></a> Ecco alcune
considerazioni che aiuteranno nella decisione.
<ul>
<li><b>Versioni:</b> le immagini live sono disponibili in varie <q>versioni</q>
a seconda dell'ambiente desktop fornito (GNOME, KDE, LXDE, Xfce, Cinnamon e
MATE). Molti utenti troveranno adatta questa scelta iniziale di pacchetti e
potranno successivamente installare tutti i pacchetti aggiuntivi
dalla rete.</li>

<li><b>Architettura:</b> al momento sono fornite soltanto le immagini per
l'architettura più diffusa, PC a 64-bit (amd64).</li>

<li><b>Installatore:</b> a partire da Debian 10 Buster, le immagini live
mettono a disposizione il semplice da usare <a
href="https://calamares.io">Calamares Installer</a>, un sistema per
l'installazione indipendente dalla distribuzione, come alternativa al ben
conosciuto <a href="$(HOME)/devel/debian-installer">Debian-Installer</a>.</li>

<li><b>Dimensione:</b> ogni immagine è molto più piccola rispetto al set
completo di DVD, ma più grande della netinstall.</li>

<li><b>Lingue:</b> le immagini non contengono i pacchetti per la totalit&agrave;
delle lingue. Se dovessero essere necessari metodi di input, set di caratteri e
pacchetti supplementari per la localizzazione, bisognerà installarli in
seguito.</li>
</ul>

<p>Le seguenti immagini sono disponibili per il download:</p>
<ul>
  <li>Immagini <q>live</q> ufficiali per il rilascio <q>stabile</q> &mdash; <a
  href="#live-install-stable">vedi sotto</a></li>
</ul>

<h2 id="live-install-stable">Immagini live ufficiali per il rilascio <q>stabile</q></h2>
<p>Disponibili in diversi formati, ciascuno di dimensioni diverse come detto
in precedenza, queste immagini sono adatte sia per provare un sistema Debian,
composto da una serie di pacchetti predefiniti selezionati, che per installarlo
usando lo stesso supporto.</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB (tramite <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p>Le immagini ISO <q>ibride</q> sono adatte per la scrittura su DVD-R(W) e
su chiavette USB di dimensioni adeguate. Se possibile, usare BitTorrent
perché riduce il carico dei nostri server.</p>
          <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
<p>Le immagini ISO <q>ibride</q> sono adatte per la scrittura su DVD-R(W) e
su chiavette USB di dimensioni adeguate.</p>
          <stable-live-install-iso-cd-images />
</div>
</div>

<p>Per informazioni su cosa siano questi file e su come usarli, visitare
le <a href="../faq/">FAQ</a>.</p>

<p>Chi vuole installare Debian dall'immagine live scaricata, dovrebbe leggere
le <a href="$(HOME)/releases/stable/installmanual">informazioni dettagliate
sul processo di installazione</a>.</p>

<p>Visitare la <a href="$(HOME)/devel/debian-live">pagina del progetto Debian
Live</a> per ulteriori informazioni sui sistemi Live Debian forniti da
queste immagini.</p>

