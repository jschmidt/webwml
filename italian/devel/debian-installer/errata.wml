#use wml::debian::template title="Errata corrige dell'Installatore Debian"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="9a1725c7a7c2a16470f0814224c0b78cecb2e2fe" maintainer="Luca Monducci" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"


<h1>Errata per <humanversion /></h1>


<p>
Questo è l'elenco dei problemi conosciuti presenti nella versione <humanversion />
dell'Installatore Debian. Chi riscontra un problema non ancora presente
in questa pagina è invitato a inviare un <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">resoconto
dell'installazione</a> in cui viene descritto il problema.
</p>


<dl class="gloss">
	<dt>Tema grafico usato nell'installatore</dt>
	<dd>Ancora non c'è un tema grafico per Bookworm, l'installatore
	continua a utilizzare il tema di Bullseye.
	<br />
	<b>Stato:</b> Risolto in Bookworm RC 1.</dd>

	<dt>Alcune schede sonore richiedono un firmware</dt>
	<dd>Un certo numero di schede audio richiede il caricamento del proprio
	firmware per emettere dei suoni (<a
	href="https://bugs.debian.org/992699">#992699</a>).
	<br />
	<b>Stato:</b> Risolto in Bookworm Alpha 1.</dd>

	<dt>LVM criptato potrebbe non funzionare sui sistemi con poca memoria</dt>
	<dd>È possibile che i sistemi con poca memoria (ad esempio 1 GB) non
	riescano a configurare LVM criptato: cryptsetup potrebbe scatenare un
	errore fatale di out-of-memory durante la formattazione della partizione
	LUKS (<a href="https://bugs.debian.org/1028250">#1028250</a>, <a
	href="https://gitlab.com/cryptsetup/cryptsetup/-/issues/802">cryptsetup
	upstream</a>).
	<br />
	<b>Stato:</b> Risolto in Bookworm RC 2.</dd>

	<dt>Partizionamento &quot;Guidato - usa l'intero disco e configura LVM&quot; in modalità UEFI</dt>
	<dd>L'uso di questo tipo di partizionamento guidato comporta la richiesta
	&quot;Forzare l'installazione UEFI?&quot; (anche quando non ci sono altri
	sistemi operativi), con &quot;No&quot; come risposta predefinita (<a
	href="https://bugs.debian.org/1033913">#1033913</a>). Se ci si attiene
	alla risposta predefinita, è molto probabile che se è attivo il Secure
	Boot l'installazione si concluderà lasciando il sistema in uno stato da
	cui non può essere avviato. Cambiare la risposta in &quot;Sì&quot;
	dovrebbe evitare di incorrere in questo problema.
	<br />
	<b>Stato:</b> Risolto in Bookworm RC 2.</dd>
</dl>
