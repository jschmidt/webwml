#use wml::debian::template title="Coordinamento dei gruppi l10n"
#use wml::debian::translation-check translation="1469899244087c9d456baf8dc6d1e40a328c9b34" maintainer="Giuseppe Sacco"


<h1>Pseudo-URL</h1>

<p>
Il programma che ascolta sulle liste debian-l10n-* capisce alcuni pseudo URL
inseriti nell'oggetto dell'email.
Gli pseudo URL devono essere nel seguente formato:
</p>
<div class="center"><code>[&lt;stato&gt;]&nbsp;&lt;tipo&gt;://&lt;pacchetto&gt;/&lt;file&gt;</code></div>

<p>
Lo <i>state</i> deve essere uno dei seguenti:
TAF, MAJ, ITT, RFR, LCFC, BTS#&lt;numero bug&gt;, DONE o HOLD.
</p>

<dl>
  <dt>
    <tt>TAF</tt> (<i>Travail À Faire</i>)
  </dt>
  <dd>
    Inviato dal <strong>coordinatore</strong> (non da un generico iscritto
    alla lista) per indicare che c'è un documento sul quale lavorare.
  </dd>
  <dt>
    <tt>MAJ</tt> (<i>Mise À Jour</i>)
  </dt>
  <dd>
    Inviato dal <strong>coordinatore</strong> (non da un generico iscritto 
    alla lista) per indicare che c'è un documento che va aggiornato e che
    questo lavoro è riservato al traduttore precedente.
  </dd>
  <dt>
    <tt>ITT</tt> (Intent To Translate)
  </dt>
  <dd>
    Inviato per indicare che tu pianifichi di lavorare sulla traduzione;
    è utilizzato per evitare la sovrapposizione.<br />
    Se invii un messaggio <tt>[ITT]</tt> e un'altra persona invia un altro
    [ITT] per lo stesso file, manda subito un email alla lista per ricordare
    che hai la priorità. Lo scopo è di evitare lavoro inutile.
  </dd>
  <dt>
    <tt>RFR</tt> (Request For Review)
  </dt>
  <dd>
    Una bozza iniziale della traduzione è allegata. Altri nella lista
    hanno la possibilità di controllarla e inviare risposte con correzioni
    (possibilmente in privato se non ci sono problemi).<br />
    Ulteriori RFR possono seguire nel caso ci siano cambiamenti
    sostanziali.
  </dd>
  <dt>
    <tt>ITR</tt> (Intent To Review)
  </dt>
  <dd>
    Utilizzato per evitare che sia inviato un LCFC mentre sono in corso
    le revisioni.<br />
    Principalmente usato quando si prevede che la revisione richieda
    parecchi giorni (perché la traduzione è lunga oppure perché non si ha
    tempo prima del fine settimana, eccetera).<br />
    Il corpo dell'email dovrebbe indicare quanto attendere la revisione.<br />
    Notare che gli pseudo URL di tipo ITR sono ignorati dal programma.<br />
  </dd>
  <dt>
    <tt>LCFC</tt> (Last Chance For Comment)
  </dt>
  <dd>
    Indica che una traduzione è completa, integrata con le correzioni
    della revisione, e che verrà inviata alla destinazione appropriata.
    <br />
    Può essere inviato quando non ci sono ITR e la discussione che ha
    seguito l'RFR è completata da alcuni giorni.
    <br />
    Non dovrebbe essere inviato prima di aver ricevuto almeno una
    revisione.
  </dd>
  <dt>
    <tt>BTS#&lt;numero bug&gt;</tt> (Bug Tracking System)
  </dt>
  <dd>
    Utilizzato per registrare un numero di bug una volta che la
    traduzione sia stata inviato al BTS.<br />
    Il programma controllerà periodicamente se la segnalazione del
    bug sia stata chiusa o risolta.
  </dd>
  <dt>
    <tt>DONE</tt>
  </dt>
  <dd>
    Utilizzato per chiudere il ciclo una volta che la traduzione sia
    stata completata; utile nel caso non sia stata mandata al BTS.
  </dd>
  <dt>
    <tt>HOLD</tt>
  </dt>
  <dd>
    Utilizzato per mettere in pausa una traduzione, per esempio quando
    ci sono altre modifiche da fare (se la traduzione contiene errori
    nel pacchetto o se è disponibile altrove).<br />
    Lo scopo di questo stato è di evitare lavoro inutile.
  </dd>
</dl>

<p>
Il <i>tipo</i> può essere qualsiasi cosa indichi il tipo di documento, come
po-debconf, po, po4a o wml.
</p>

<p>
<i>pacchetto</i> è il nome del pacchetto dal quale è preso il documento.
Usare <i>www.debian.org</i> oppure nulla per i file WML del sito web Debian.
</p>

<p>
<i>file</i> è il nome del file del documento; può contenere altre informazioni
per identificare univocamente il documento, come il suo percorso.
È normalmente un nome, come <i>lc</i>.po dove <i>lc</i> è il codice della lingua
(esempio: <i>de</i> per tedesco o <i>pt_BR</i> per portoghese brasiliano).
</p>

<p>
La struttura di <i>file</i> dipende dal tipo scelto e, naturalmente,
dalla lingua.
In genere è solo un'identificatore, ma, poiché è utilizzato
in queste pagine web per tracciare lo stato delle traduzioni,
è raccomandato fortemente di usare il seguente schema.
</p>

<ul>
<li><code>po-debconf://nome-pacchetto/it.po</code> (per le installazioni dei pacchetti)</li>
<li><code>po://nome-pacchetto/percorso-nel-pacchetto-sorgente/it.po</code> (per i file po classici)</li>
<li><code>po4a://nome-pacchetto/percorso-nel-pacchetto-sorgente/it.po</code> (per la documentazione convertita nel formato po)</li>
<li><code>wml://percorso-sotto-la-lingua-in-CVS</code> (per la pagine del sito web)</li>
<li><code>ddp://document/nomefile.po</code> (per la documentazione Debian)</li>
<li><code>xml://installation-guide/lingua/percorso-nel-pacchetto-sorgente/nomefile.xml</code> (per il manuale d'installazione)</li>
</ul>

<p>
Lo stato del BTS è leggermente speciale; registra il numero di bug
in modo che l10n-bot possa tracciare lo stato della traduzione una
volta inviata al BTS controllando se una delle segnalazioni sia stata
chiusa. Ad esempio, la lista debian-l10n-spanish potrebbe usare:
</p>
<div class="center"><code>[BTS#123456] po-debconf://cups/es.po</code></div>

<p>
Se si ha intenzione di tradurre molti pacchetti, si può inviare
un solo ITT unificato. Ad esempio (per la lista debian-l10n-danish):
</p>
<div class="center"><code>[ITT] po-debconf://{cups,courier,apache2}/da.po</code></div>
<p>
Quindi vanno elencati i nomi dei pacchetti tra parentesi graffe,
separati da virgole. Non ci vanno spazi aggiuntivi!
</p>
