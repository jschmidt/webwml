#use wml::debian::template title="Informazioni sulla sicurezza LTS" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5f02bf77809ef7784529787e5f4f6384bf5e288c"


<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Mantenere il sistema Debian LTS
sicuro</toc-add-entry>

<p>
I pacchetti <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>
sono installati insieme a GNOME desktop e mantengono automaticamente
il computer con gli ultimi aggiornamenti di sicurezza (e anche gli
altri). La <a href="https://wiki.debian.org/UnattendedUpgrades">pagina
wiki</a> contiene ulteriori informazioni su come configurare manualmente
<tt>unattended-upgrades</tt>.
</p>

<p>
Per ulteriori informazioni sui problemi di sicurezza in Debian, fare
riferimento alle <a href="../../security">Informazioni sulla sicurezza</a>.
</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Ultimi bollettini</toc-add-entry>

<p>
Queste pagine contengono un archivio dei bollettini della sicurezza
postate nella lista <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.
</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>
Gli ultimi bollettini della sicurezza di Debian LTS sono disponibili
anche in <a href="dla">formato RDF</a>. È anche disponibile anche un
<a href="dla-long">secondo file</a> che include il primo paragrafo del
relativo bollettino in modo che sia possibile capire cosa riguarda
l'avviso.
</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Sono disponibili anche i precedenti bollettini sulla sicurezza:
<:= get_past_sec_list(); :>

<!-- This section a copy from /security/dsa.wml. TODO: create and include file -->
<toc-add-entry name="infos">Fonti di informazioni sulla
sicurezza</toc-add-entry>

<ul>
<li><a href="https://security-tracker.debian.org/">Debian Security Tracker</a>
fonte primaria per tutte le informazioni relative alla sicurezza, opzioni
di ricerca</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">elenco
in JSON</a> contiene la descrizione di CVE, il nome del pacchetto, il numero
di bug Debian, le versioni del pacchetto con la correzione, non è incluso il
DSA</li>

<li><a
href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">Elenco
dei DSA</a> contiene i DSA con la data, i numeri dei CVE correlati, le
versioni dei pacchetti con le correzioni.</li>

<li><a
href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">Elenco
dei DLA</a> contiene i DLA con la data, i numeri dei CVE correlati, le
versioni dei pacchetti con le correzioni.</li>

<li><a href="https://lists.debian.org/debian-security-announce/">annunci
DSA</a></li>

<li><a href="https://lists.debian.org/debian-lts-announce/">annunci
DLA</a></li>

<li><a href="oval">Archivi Oval</a></li>

<li>Cercare un DSA (il maiuscolo è importante)<br>
esempio <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt></li>

<li>Cercare un DLA ( -1 è importante)<br>
esempio <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt></li>

<li>Cercare un CVE<br>
esempio <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt></li>
</ul>

<ul>
<li>Consultare le <a href="https://wiki.debian.org/LTS/FAQ">FAQ su Debian
LTS</a> La tua domanda potrebbe già avere una risposta!</li>

<li><a href="https://lts-team.pages.debian.net/wiki/Contact">Contatti del
team Debian LTS</a>.</li>
</ul>
