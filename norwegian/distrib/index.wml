#use wml::debian::template title="Få tak i Debian"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="0691e0b35ed0aa5df10a4b47799051f77e519d25" maintainer="Hans F. Nordhaug"
# Oversatt til norsk av Tor Slettnes <tor@slett.net>
# Oppdatert av Hans F. Nordhaug <hansfn@gmail.com>, 2008-2021.

<p>Debian er <a href="../intro/free">fritt tilgjengelig</a>.
Du kan laste ned hele distribusjonen fra et av 
<a href="ftplist">filspeilene</a> våre.
<a href="../releases/stable/installmanual">Installasjonsmanualen</a>
inneholder detaljert instruksjon for installasjonen. Se også 
<a href="../releases/stable/releasenotes">utgivelsesmerknadene</a>.
</p>

<p>
  Denne siden har valg for installasjon av Debian Stable. Hvis du er interessert i Testing 
  eller Unstable, besøk vår <a href="../releases/">side med utgaver</a>.
</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Laste ned en bildefil for installasjon</a></h2>
    <p>Avhengig av din internettforbindelse, kan du laste ned følgende:</p>
    <ul>
      <li>En <a href="netinst"><strong>liten bildefil for installasjon</strong></a>:
        kan lastes ned hurtig og bør lagres på en CD, DVD eller USB.
        For å bruke denne, trenger du en maskin med internettforbindelse.
	<ul class="quicklist downlist">
	  <li><a title="Last ned installasjonsprogram for 64-bit Intel og AMD PC"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bit
	      PC netinst iso</a></li>
	  <li><a title="Last ned installasjonsprogram for normal 32-bit Intel og AMD PC"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bit
	      PC netinst iso</a></li>
	</ul>
      </li>
      <li>En større <a href="../CD/"><strong>komplett bildefil for installasjon</strong></a>: 
        inneholder flere pakker, som gjør det lettere å installere på maskiner
        uten internettforbindelse.
	<ul class="quicklist downlist">
	  <li><a title="Last ned DVD torrents for 64-bit Intel og AMD PC"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bit PC torrents (DVD)</a></li>
	  <li><a title="Last ned DVD torrents for normal 32-bit Intel og AMD PC"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bit PC torrents (DVD)</a></li>
	  <li><a title="Last ned CD torrents for 64-bit Intel og AMD PC"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bit PC torrents (CD)</a></li>
	  <li><a title="Last ned CD torrents for normal 32-bit Intel og AMD PC"
		 href="<stable-images-url/>/i386/bt-cd/">32-bit PC torrents (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Bruk en Debian cloud-bildefil</a></h2>
    <p>En offisiell <a href="https://cloud.debian.org/images/cloud/"><strong>cloud-bildefil</strong></a>, bygget av Debian Cloud-laget, kan bli brukt på:</p>
    <ul>
      <li>Din cloud-tilbyder, i qcow2 eller raw format.
      <ul class="quicklist downlist">
        <li>64-bit AMD/Intel
          (<a title="OpenStack bildefil for 64-bit AMD/Intel qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">qcow2</a>,
           <a title="OpenStack bildefil for 64-bit AMD/Intel raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.raw">raw</a>)</li>
        <li>64-bit ARM
          (<a title="OpenStack bildefil for 64-bit ARM qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">qcow2</a>,
           <a title="OpenStack bildefil for 64-bit ARM raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.raw">raw</a>)</li>
        <li>64-bit Little Endian PowerPC
          (<a title="OpenStack bildefil for 64-bit Little Endian PowerPC qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.qcow2">qcow2</a>,
           <a title="OpenStack bildefil for 64-bit Little Endian PowerPC raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, enten som maskin-bildefil eller via AWS-markedsplassen.
        <ul class="quicklist downlist">
          <li><a title="Amazon Machine Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon maskin-bildefil</a></li>
          <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS-markedsplassen</a></li>
        </ul>
      </li>
      <li>Microsoft Azure, på Azure-markedsplassen.
        <ul class="quicklist downlist">
          <li><a title="Debian 11 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
          <li><a title="Debian 10 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10?tab=PlansAndPrice">Debian 10 ("Buster")</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Kjøpe et sett CD-er eller DVD-er
      fra en av forhandlerne som selger Debian-CD-er.</a></h2>

    <p>
      Mange forhandlere selger distribusjonen for mindre enn 50 kroner pluss 
      frakt (se deres nettsider for å finne ut om de sender til 
      utlandet).
      <br />
      Det følger også CD-er med noen av <a href="../doc/books">bøkene om 
      Debian</a>.
    </p>

    <p>Her er de grunnleggende fordelene med CD-er:</p>

    <ul>
      <li>Det er enklere å installere fra CD-ene.</li>
      <li>Du kan installere på maskiner som ikke har Internett-tilgang.</li>
      <li>Du kan installere Debian (på så mange maskiner som du har lyst til) 
          uten å laste ned alle pakkene selv.</li>
      <li>Med CD-en kan du lettere redde et skadet Debian-system.</li>
    </ul>

    <h2><a href="pre-installed">Kjøpe en datamaskin hvor Debian er
      forhåndsinstallert</a></h2>
    <p>Det er flere fordeler med det:</p>
    <ul>
      <li>Du behøver ikke å installere Debian.</li>
      <li>Installasjonen er tilpasset maskinvaren.</li>
      <li>Forhandleren yter kanskje teknisk støtte.</li>
    </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Prøve Debian før du installerer</a></h2>
    <p>
      Du kan nå prøve Debian ved å starte opp et live-system fra en CD, DVD
      eller USB-minnepinne uten å installere noen filer på datamaskinen. Når du
      er klar, kan du kjøre det inkluderte installasjonsprogrammet (som fra og
      med Debian 10 Buster er det brukervennlige programmet 
      <a href="https://calamares.io">Calamares</a>).
      Forutsatt at bildefilen oppfyller dine krav til størrelse, språk og pakkeutvalg, 
      kan denne metoden være passende for deg. Les 
      <a href="../CD/live#choose_live">mer informasjon om denne metoden</a>
      hvis du trenger hjelp til å ta en beslutning.
    </p>
	
    <ul class="quicklist downlist">
      <li><a title="Last ned livetorrents til 64-bit Intel og AMD PC"
          href="<live-images-url/>/amd64/bt-hybrid/">64-bit PC live torrents</a></li>
    </ul>
  </div>
</div>

#include "index_sub_non_free_firmware.data"
