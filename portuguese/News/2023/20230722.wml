#use wml::debian::translation-check translation="1f51232f1964140bbf71ecfd1fc7bdcbd252e744"
<define-tag pagetitle>Atualização Debian 12: 12.1 lançado</define-tag>
<define-tag release_date>2023-07-22</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
        push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a primeira atualização de sua
versão estável (stable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, além de pequenos ajustes para problemas mais sérios. Avisos de
segurança já foram publicados em separado e são referenciados quando
necessário.</p>

<p>Por favor, note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de jogar fora as antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir de
security.debian.org não terão que atualizar muitos pacotes, e a maioria de tais
atualizações estão incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction aide "Manipula corretamente a criação de usuário(a) do sistema; corrige o processamento de subdiretórios correspondentes">
<correction autofs "Corrige travamento ao usar autenticação LDAP via Kerberos">
<correction ayatana-indicator-datetime "Corrige o toque de alarmes customizados">
<correction base-files "Atualização para a versão pontual 12.1">
<correction bepasty "Corrige a renderização de textos enviados">
<correction boost1.81 "Adiciona dependência ausente (libboost-json1.81.0) em libboost-json1.81-dev">
<correction bup "Restaura corretamente ACLs POSIX">
<correction context "Habilita socket em ConTeXt mtxrun">
<correction cpdb-libs "Corrige uma vulnerabilidade de estouro de buffer [CVE-2023-34095]">
<correction cpp-httplib "Corrige vulnerabilidade de injeção CRLF [CVE-2023-26130]">
<correction crowdsec "Corrige acquis.yaml padrão para também incluir a fonte de dados journalctl, limitada à unidade ssh.service, certificando-se de que a aquisição ocorra mesmo sem o tradicional arquivo auth.log; e também certificando-se de que uma fonte de dados inválida não faça o motor falhar">
<correction cups "Correções de segurança: use-after-free [CVE-2023-34241]; estouro de buffer de heap [CVE-2023-32324]">
<correction cvs "Configura o caminho (path) completo para ssh">
<correction dbus "Lançamento de nova versão estável; corrige vulnerabilidade de negação de serviço (denial of service) [CVE-2023-34969]; não leva DPKG_ROOT em consideração, restaurando a cópia de /etc/machine-id ao invés de criar um identificador inteiramente novo">
<correction debian-installer "Sobe a ABI do kernel Linux para 6.1.0-10; reconstrói contra proposed-updates">
<correction debian-installer-netboot-images "Reconstrói contra proposed-updates">
<correction desktop-base "Remove alternativas emerald na remoção do pacote">
<correction dh-python "Reintroduz Breaks+Replaces em python2 para ajudar o apt em alguns cenários de atualização">
<correction dkms "Adiciona Breaks contra pacotes *-dkms obsoletos e incompatíveis">
<correction dnf "Corrige constante DNF padrão PYTHON_INSTALL_DIR">
<correction dpdk "Lançamento de nova versão estável">
<correction exim4 "Corrige o tratamento (parsing) de argumentos na expansão ${run }; corrige ${srs_encode ..} retornando resultados incorretos a cada 1024 dias">
<correction fai "Corrige o tempo de vida de endereços IP">
<correction glibc "Corrige um estouro de buffer em gmon; corrige um deadlock em getaddrinfo (__check_pf) com deferred cancellation; corrige suporte a y2038 em strftime em arquiteturas 32 bits; corrige tratamento (parsing) de /etc/gshadow em casos raros nos quais ponteiros incorretos podem ser retornados, causando falhas (segfault) em aplicações; corrige um deadlock em system() quando chamada concorrentemente de múltiplas threads; cdefs: limita a definição de macros de fortificação a __FORTIFY_LEVEL &gt; 0 para suportar antigos compiladores C90">
<correction gnome-control-center "Lançamento de nova versão de correção de bugs">
<correction gnome-maps "Lançamento de nova versão de correção de bugs">
<correction gnome-shell "Lançamento de nova versão de correção de bugs">
<correction gnome-software "Lançamento de nova versão estável; correção de vazamentos de memória">
<correction gosa "Silencia avisos de obsolescência do PHP 8.2; corrige template padrão ausente em tema padrão; corrige estilo de tabela; corrige o uso de debugLevel &gt; 0">
<correction groonga "Corrige links em documentação">
<correction guestfs-tools "Atualização de segurança [CVE-2022-2211]">
<correction indent "Restaura a macro ROUND_UP e ajusta o tamanho inicial do buffer">
<correction installation-guide "Ativa a tradução indonésia">
<correction kanboard "Corrige a injeção maliciosa de tags HTML no DOM [CVE-2023-32685]; corrige a referência indireta baseada em parâmetros a objetos que leva a exposição de arquivos privados [CVE-2023-33956]; corrige controles de acesso ausentes [CVE-2023-33968, CVE-2023-33970]; corrige o armazenamento de XSS na funcionalidade Task External Link [CVE-2023-33969]">
<correction kf5-messagelib "Procura também por subkeys">
<correction libmatekbd "Corrige vazamentos de memória">
<correction libnginx-mod-http-modsecurity "Reconstrução com pcre2">
<correction libreoffice "Lançamento de nova versão de correção de bugs">
<correction libreswan "Corrige vulnerabilidade de negação de serviço (denial-of-service) [CVE-2023-30570]">
<correction libxml2 "Corrige vulnerabilidade de NULL pointer dereference [CVE-2022-2309]">
<correction linux "Lançamento de nova versão estável; netfilter: nf_tables: não ignora genmask ao procurar cadeia por id [CVE-2023-31248], previne acesso OOB em nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-amd64 "Lançamento de nova versão estável; netfilter: nf_tables: não ignora genmask ao procurar cadeia por id [CVE-2023-31248], previne acesso OOB em nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-arm64 "Lançamento de nova versão estável; netfilter: nf_tables: não ignora genmask ao procurar cadeia por id [CVE-2023-31248], previne acesso OOB em nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-i386 "Lançamento de nova versão estável; netfilter: nf_tables: não ignora genmask ao procurar cadeia por id [CVE-2023-31248], previne acesso OOB em nft_byteorder_eval [CVE-2023-35001]">
<correction mailman3 "Remove cron job redundante; trata ordenamento de serviços quando MariaDB está presente">
<correction marco "Show correct window title when owned by superuser">
<correction mate-control-center "Corrige vários vazamentos de memória">
<correction mate-power-manager "Corrige vários vazamentos de memória">
<correction mate-session-manager "Corrige vários vazamentos de memória; permite o uso de backends clutter além do x11">
<correction multipath-tools "Esconde caminhos subjacentes do LVM; previne falha inicial do serviço em novas instalações">
<correction mutter "Lançamento de nova versão de correção de bugs">
<correction network-manager-strongswan "Constrói o componente do editor com suporte ao GTK 4">
<correction nfdump "Retorna sucesso ao iniciar; corrige segfault no tratamento de opções (option parsing)">
<correction nftables "Corrige regressão no formato set listing">
<correction node-openpgp-seek-bzip "Instala corretamente arquivos no pacote seek-bzip">
<correction node-tough-cookie "Corrige vulnerabilidade de poluição de protótipo (prototype pollution) [CVE-2023-26136]">
<correction node-undici "Correções de segurança: protege o campo <q>Host</q> do cabeçalho HTTP de injeção CLRF [CVE-2023-23936]; potencial ReDoS em Headers.set e Headers.append [CVE-2023-24807]">
<correction node-webpack "Correção de segurança (objetos cross-realm) [CVE-2023-28154]">
<correction nvidia-cuda-toolkit "Atualização do openjdk-8-jre embutido">
<correction nvidia-graphics-drivers "Lançamento de nova versão estável; correções de segurança [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla "Lançamento de nova versão estável; correções de segurança [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "Lançamento de nova versão estável; correções de segurança [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-modprobe "Lançamento de nova versão de correção de bugs">
<correction nvidia-open-gpu-kernel-modules "Lançamento de nova versão estável; correções de segurança [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-support "Adiciona Breaks contra pacotes incompatíveis do bullseye">
<correction onionshare "Corrige instalação de desktop furniture">
<correction openvpn "Corrige vazamento de memória e ponteiro solto (um possível vetor de erros)">
<correction pacemaker "Corrige regressão em escalonador de recursos">
<correction postfix "Lançamento de nova versão de correção de bugs; corrige <q>postfix set-permissions</q>">
<correction proftpd-dfsg "Não ativa socket inetd-style na instalação">
<correction qemu "Lançamento de nova versão estável; corrige a indisponibilidade de dispositivos USB para XEN HVM domUs; 9pfs: impede a abertura de arquivos especiais [CVE-2023-2861]; corrige problema de reentrância na controladora LSI [CVE-2023-0330]">
<correction request-tracker5 "Corrige links para documentação">
<correction rime-cantonese "Ordena palavras e caracteres por frequência">
<correction rime-luna-pinyin "Instala dados do schema pinyyin ausentes">
<correction samba "Lançamento de nova versão estável; garante geração das manpages durante a construção; habilita a habilidade de armazenar tickets kerberos no chaveiro do kernel; corrige problemas de construção em armel e mipsel; corrige problemas de logon/trust com atualizações 2023-07">
<correction schleuder-cli "Correção de segurança (value escaping)">
<correction smarty4 "Corrige vulnerabilidade de execução arbitrária de código (arbitrary code execution) [CVE-2023-28447]">
<correction spip "Várias correções de segurança; correção de segurança (filtragem de dados de autenticação)">
<correction sra-sdk "Corrige a instalação de arquivos em libngs-java">
<correction sudo "Corrige o formato de eventos no log">
<correction systemd "Lançamento de nova versão de correção de bugs">
<correction tang "Corrige condição de corrida ao criar/rotacionar chaves [CVE-2023-1672]">
<correction texlive-bin "Desabilita socket em luatex por padrão [CVE-2023-32668]; permite a instalação em i386">
<correction unixodbc "Adiciona Breaks+Replaces contra odbcinst1debian1">
<correction usb.ids "Atualiza dados incluídos">
<correction vm "Desabilita byte compilation">
<correction vte2.91 "Lançamento de nova versão de correção de bugs">
<correction xerial-sqlite-jdbc "Usa o UUID para ID de conexão [CVE-2023-32697]">
<correction yajl "Correção de segurança de vazamento de memória; corrige vulnerabilidade de negação de serviço [CVE-2017-16516] e de estouro de inteiro [CVE-2022-24795]">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
A equipe de segurança já lançou um aviso para cada uma dessas atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2023 5423 thunderbird>
<dsa 2023 5425 php8.2>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5429 wireshark>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5448 linux-signed-amd64>
<dsa 2023 5448 linux-signed-arm64>
<dsa 2023 5448 linux-signed-i386>
<dsa 2023 5448 linux>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
</table>



<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas
na versão estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informações da versão estável (stable) (notas de lançamento, errata, etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional completamente livre Debian.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com o time de
lançamento da estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>
