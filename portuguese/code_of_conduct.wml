#use wml::debian::template title="Código de conduta Debian" BARETITLE=true
#use wml::debian::translation-check translation="7055acd6ba9588ebafbe9a60b2d80ba81cd413d8"

{#meta#:
<meta name="keywords" content="code of conduct, coc">
:#meta#}

<p>
  Versão 1.0 ratificada em 28 de abril de 2014.
</p>

<p>O Projeto Debian, produtor do sistema Debian, têm adotado um
código de conduta para os(as) participantes de suas listas de discussão, canais
de IRC e outros meios de comunicação no âmbito do projeto.</p>


<hr />
    <h2><q>Código de conduta</q> Debian</h2>

    <ol>
      <li>
	    <strong>Seja respeitoso(a)</strong>
	    <p>
    Em um projeto do tamanho do Debian, inevitavelmente haverá pessoas com quem
    você pode discordar, ou encontrar dificuldade de cooperar. Aceite isso,
    mas mesmo assim, continue respeitoso(a). Discordância não é desculpa para
    mau comportamento ou ataques pessoais, e uma comunidade na qual pessoas se
    sentem ameaçadas não é uma comunidade saudável.
	    </p>
      </li>
      <li><strong>Considere boa-fé</strong>
	    <p>
    Os(As) contribuidores(as) do Debian têm muitas maneiras de alcançar o nosso
    objetivo comum de fazer um sistema operacional <a href="$(HOME)/intro/free">livre</a>
    que podem ser diferentes das suas maneiras. Considere que as outras pessoas
    estejam trabalhando para atingir esse objetivo.
        </p><p>
    Note que muitos(as) dos(as) nossos(as) contribuidores(as) não falam inglês
    nativamente ou podem ter diferentes origens culturais.
    </p>
      </li>
      <li><strong>Seja colaborativo(a)</strong>
	    <p>
    O Debian é um projeto grande e complexo; sempre há algo a mais para aprender
    sobre o Debian. É bom pedir ajuda quando você precisar. Da mesma forma,
    ofertas de ajuda devem ser vistas no contexto do nosso objetivo comum de
    melhorar o Debian.
    </p><p>
    Quando você fizer algo em benefício do projeto, esteja disposto a explicar
    aos(as) outros(as) como funciona, de modo que eles(as) possam construir sobre o seu
    trabalho para torná-lo ainda melhor.
    </p>
      </li>
      <li><strong>Tente ser conciso(a)</strong>
        <p>
    Tenha em mente que o que você escreve será lido por centenas
    de pessoas. Escrever uma mensagem curta significa que as pessoas podem
    entender a conversa da forma mais eficiente possível. Considere adicionar um
    resumo quando uma longa explicação for necessária.
    </p><p>
    Tente trazer novos argumentos para uma conversa, para que cada mensagem
    acrescente algo único à discussão, tendo em mente que o resto da
    discussão ainda contém as outras mensagens com argumentos que já
    foram feitos.
    </p><p>
    Tente se manter no assunto, especialmente em discussões que já estão
    bastante longas.
        </p>
      </li>
      <li><strong>Seja aberto(a)</strong>
        <p>
    A maioria dos meios de comunicação usados no Debian permite a comunicação
    pública e privada. Conforme o parágrafo três do <a
    href="$(HOME)/social_contract">contrato social</a>, você deve usar
    preferencialmente métodos públicos de comunicação para mensagens
    relacionadas ao Debian, a menos que publique algo sensível.
    </p><p>
    Isso também se aplica a mensagens de ajuda ou suporte relacionadas ao
    Debian; um pedido de suporte público tem muito mais chances de resultar
    em uma resposta para sua pergunta, e também garante que quaisquer erros
    involuntários cometidos pelas pessoas ao responder sua pergunta serão
    mais facilmente detectados e corrigidos.
        </p>
      </li>
      <li><strong>Em caso de problemas</strong>
      <p>Embora este código de conduta deva ser respeitado pelos(as) participantes,
      nós reconhecemos que às vezes as pessoas podem ter um dia ruim, ou não ter
      conhecimento de algumas diretrizes deste código de conduta. Quando isso
      acontecer, você pode responder a elas e indicar este código de conduta.
      Tais mensagens podem ser em público ou em privado, o que for mais
      apropriado. No entanto, independentemente da mensagem ser pública ou não,
      ela deve aderir às partes relevantes deste código de conduta; em particular,
      não deve ser abusiva ou desrespeitosa. Assuma boa-fé; é mais provável
      que os(as) participantes não percebam o mau comportamento deles(as) e não estejam
      intencionalmente degradando a qualidade da discussão.
      </p><p>
      Infratores(as) graves ou persistentes serão temporariamente ou
      permanentemente banidos(as) de se comunicarem através dos sistemas do
      Debian. As reclamações devem ser feitas (em privado) aos(às)
      administradores(as) do fórum de comunicação do Debian em questão. Para
      encontrar as informações de contato desses(as) administradores(as), por
      favor, veja <a href="$(HOME)/intro/organization"> a respectiva página na
      estrutura organizacional do Debian</a>.
       </p>
      </li>
     </ol>
<hr />

<h2 id="reporting">Reportando problemas</h2>

<p>Contate a <a href="$(HOME)/intro/organization#community">
equipe de comunidade Debian</a> (em inglês): community@debian.org</p>

<h2 id="guidelines">Leitura complementar</h2>
     <p>Alguns dos links nesta seção não se referem a documentos que são
     parte deste código de conduta, nem são oficiais do Debian. No entanto,
     todos eles contêm informações úteis sobre como se comportar em nossos
     canais de comunicação.
     </p>
     <ul>
     <li>O Debian tem uma <a
     href="$(HOME)/intro/diversity">Declaração de diversidade</a>(<q>Diversity Statement</q>) .
     </li>
     <li>As <a href="https://people.debian.org/~enrico/dcg/">diretrizes da comunidade Debian</a>
     (<q>Debian Community Guidelines</q>), do Enrico Zini, contêm
     alguns conselhos sobre como se comunicar efetivamente.</li>
     <li>O <a href="$(HOME)/MailingLists/#codeofconduct">código de conduta
     das listas de discussão</a> são úteis para conselhos específicos para as
     listas de discussão do Debian.</li>
     <li>As <a href="https://wiki.debian.org/DebianIRCChannelGuidelines">diretrizes
     para os canais IRC</a> são úteis para conselhos específicos para os canais
     IRC do Debian.</li>
     <li>O time de comunidade escreve alguns guias extras
     em <a href="$(HOME)/code_of_conduct_interpretation">como interpretar
     </a> o código de conduta.</li>

</ul>

<p><em>Atualizações deste código de conduta devem seguir o procedimento normal
de uma <q>GR - General Resolution</q>.
No entanto, o(a) DPL (ou os(as) delegados(as) indicados(as) por ele(a)) pode
adicionar ou remover links para outros documentos na seção "leitura complementar"
após consultar o projeto, e sem a necessidade de uma GR.</em></p>

