#use wml::debian::template title="Construções (builds) do Debian-Installer"
#use wml::debian::translation-check translation="c1221b640653be886b43ce18e8e72c5516aa770f"

<h2>Construções de CDs</h2>

<p>
Existem algumas construções (builds) diferentes das imagens
de CD do Debian-Installer (Instalador do Debian) que possuem propósitos
diferentes.
</p>
<p>
A construção mais importante é a <a href="index">versão oficial atual</a>,
atualmente incluída no Debian 6.0. Estas imagens são estáticas e
imutáveis, e são as que têm a maior probabilidade de funcionar para
a maioria das pessoas. Embora testar estas imagens seja útil, a
maioria dos problemas com elas são bem conhecidos pelos(as) desenvolvedores(as)
dentro de umas poucas semanas do seu lançamento. Veja a <a href="errata">\
página de erratas</a> para os piores problemas conhecidos.
</p>
<p>
As outras construções mais usadas geralmente são as construções diárias (daily
builds). Estas são imagens mais novas que precisam de teste na esperança de
tornarem-se uma versão oficial posteriormente. Elas são apenas um link para um
dos dois tipos de imagem descritos abaixo; qual deles depende de onde estamos em
nosso ciclo de lançamento.
<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">Os relatórios de instalação</a>
que utilizam estas imagens são muito valiosos para nós.
</p>
<p>
As <a href="https://cdimage.debian.org/cdimage/daily-builds/sid_d-i/">imagens
de CD sid_d-i</a> são imagens novas produzidas diariamente.
Estas imagens usam a versão do instalador da distribuição instável (unstable),
embora ainda instalem a distribuição teste (testing). Os links de construções
diárias de CD tipicamente apontam para estas imagens.
</p>
<p>
As <a href="https://cdimage.debian.org/cdimage/daily-builds/jessie_d-i/">imagens
jessie_d-i</a> também são produzidas diariamente. Elas usam a
versão da teste (testing) do instalador, e instalam a teste (testing). Na época
do lançamento, uma destas imagens se torna a imagem do lançamento
oficial. Perto de um lançamento, os links para as construções
diárias de CD são alterados para apontar para estas imagens
para que elas possam ser testadas.
</p>
<p>
As construções dos <a href="https://cdimage.debian.org/cdimage/weekly-builds/i386/iso-cd/">CDs completos semanais</a>
e dos <a href="https://cdimage.debian.org/cdimage/weekly-builds/i386/iso-dvd/">DVDs</a>
levam vários dias e portanto são regerados apenas uma vez por semana.
A versão do instalador destas construções varia, mas geralmente é a
versão que nós queremos que seja testada na época.
</p>

<h2>Construções de initrd</h2>

<p>
Todas as outras imagens do Debian-Installer, incluindo netboot,
são conhecidas coletivamente como <q>imagens initrd</q>. Várias construções
diferentes também são usadas.
</p>
<p>
Assim como as imagens de CD, a construção initrd mais importante
é a <a href="index">versão oficial atual</a>.
</p>
<p>
As outras construções initrd mais usadas normalmente são as
construções diárias (daily builds).  Estas imagens são construídas
aproximadamente uma vez por dia por alguns(as) desenvolvedores(as) do
Debian-Installer, normalmente em seus próprios computadores pessoais. Elas
sempre incluem a última versão do instalador da distribuição instável (unstable).
</p>
<p>
De tempos em tempos, uma construção initrd oficial do Debian-Installer
será feita como parte do lançamento do pacote <tt>debian-installer</tt>.
Estas imagens são construídas na rede autoconstrutora (autobuilder network) do
Debian como qualquer outro pacote, e são colocadas no subdiretório
<tt>dists/unstable/main/binary-&lt;arch&gt;/current</tt>.
</p>
<p>
Quando o Debian-Installer é lançado, uma destas construções oficiais
é copiada para o subdiretório
<tt>dists/testing/main/binary-&lt;arch&gt;/current</tt>.
</p>

<h2>Página de estado das construções</h2>

<p>
O estado de todas as imagens construídas periodicamente é gerenciado e
coletado na <a
href="https://d-i.debian.org/daily-images/build-logs.html">página de estado
das construções</a>. Esta página não diz se uma imagem funciona, apenas se
ela foi construída com sucesso.
</p>
