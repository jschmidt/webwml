#use wml::debian::template title="Uso de HTML em páginas web do Debian" BARETITLE=true
#use wml::debian::common_tags
#use wml::debian::acronyms
#use wml::debian::toc
#use wml::debian::translation-check translation="b2595631761ec70fe18145ffa27080c0d5a5dd69"

<p>
Esta página ainda é um esboço.
</p>

<toc-display/>

<toc-add-entry name="preface">Prefácio</toc-add-entry>

<p>Esta página foi criada para ajudar editores(as) e tradutores(as) a criar
páginas com tags bem estruturadas. Há dicas sobre uso de tags e sobre como criar
novas páginas de forma que a tradução fique mais fácil.</p>


<toc-add-entry name="general">Algumas dicas gerais</toc-add-entry>
<p>Para novas páginas ou traduções, aqui está uma lista de conselhos gerais</p>
<dl>	
<dt>não use linhas longas</dt>
<dd>
Os arquivos wml e outros devem conter linhas que caibam numa janela de terminal
normal. Desse modo fica mais fácil editar no vi, fazer pesquisas e traduzir.
Também é importante porque é mais difícil solucionar conflitos em linhas longas.
</dd>
<dt>mantenha tags em linhas separadas, se possível</dt>
<dd>
Grande parte das tags HTML podem ser mantidas em linhas separadas. Algumas delas
são &lt;div&gt;, &lt;p&gt;, &lt;table&gt;, &lt;ul&gt;. Para deixar as coisas
mais simples para tradutores(as), mantenha em linhas separadas todas as tags que
podem ser posicionadas assim. De outra forma, tradutores(as) podem apagar
tags acidentalmente e esquecer de restaurá-las após terminar a tradução.
</dd>
<dt>não use espaços ou quebras de linhas em tags de linha única</dt>
<dd>
Algumas tags produzem espaços se forem colocadas em linhas separadas. Uma
delas é a tag &lt;q&gt;, para pequenas citações ou menções. Você só pode
separar essas tags como um todo, com o conteúdo numa única linha. De outro modo,
pode ocorrer um espaço entre o conteúdo e a tag na página HTML final. Entre as
palavras dessas tags você pode ter quantas quebras de linhas ou espaços quiser.
</dd>
</dl>

<toc-add-entry name="abbreviations">Abreviaturas e Acrônimos</toc-add-entry>
<p>
Para abreviaturas e acrônimos, a tag HTML &lt;acronym&gt; deve ser usada.
Existem duas razões para que o uso da tag &lt;abbr&gt; não seja recomendada:
primeiro, nem todos os navegadores suportam essa tag; segundo, existem
definições inconsistentes sobre o que é um acrônimo e o que é uma abreviatura.
</p>
<p>
Um acrônimo é adicionado numa página pela seguinte sintaxe:
<code>&lt;acronym lang="código do idioma" title="Definição completa do
acrônimo"&gt;ACRÔNIMO&lt;/acronym&gt;</code>. O título contém todas as palavras
em extenso. Se o acrônimo é criado a partir das letras iniciais de palavras,
essas letras devem estar em maiúsculas no título. O atributo lang somente é
necessário se o acrônimo ou abreviatura está num idioma estrangeiro.
</p>
<p>
Já existe um conjunto de acrônimos comuns nos modelos wml, incluídos para serem
utilizados em sua página; você tem que adicionar uma linha para usar
<code>acrônimos</code> no arquivo wml. Por exemplo, a tag wml para DD é
&lt;acronym_DD /&gt;.
</p>

<toc-add-entry name="citations">Citações e Menções</toc-add-entry>
<p>
Existem diversas regras sobre o que significa uma citação ou uma menção para
diferentes idiomas. Se você tem uma citação curta de uma linha, você tem que
usar a tag &lt;q&gt; .
O processamento do conteúdo é feito pela linguagem CSS. As tags &lt;q&gt; não
devem ter espaços ou quebras de linha entre a tag de abertura e fechamento, e o
conteúdo.
</p>
<p>
Para citações mais longas, a tag &lt;blockquote&gt; é usada. A tag
&lt;blockquote&gt; inclui um ou mais parágrafos de texto, que são marcados com
&lt;p&gt;. Por favor, não use tags &lt;blockquote&gt; para centralizar blocos de
texto que não sejam citações. Tags blockquote são exclusivas para citações e
serão processadas no futuro por código específico da linguagem CSS.
</p>
<p>
Também existe uma tag HTML &lt;cite&gt;. A tag &lt;cite&gt; não é usada para
citação de texto propriamente, mas para a fonte da citação. Pode ser o nome da
pessoa que originou a citação. A tag é adicionada como um atributo para um
&lt;blockquote&gt;, como uma URL.
</p>

<toc-add-entry name="code">Nome de programas e Código</toc-add-entry>
<p>
Para nomes de programas e códigos de computador existe uma tag chamada
&lt;code&gt;. Navegadores normalmente sabem como exibir código e nomes de
programas, mas o processamento também pode ser alterado por CSS. Não é uma boa
ideia usar &lt;tt&gt; no lugar, já que ela não diz nada sobre o conteúdo.
</p>

<toc-add-entry name="samp">Amostras de saídas de processamento</toc-add-entry>
<p>
Para saídas de processamento na tela, existe uma tag especial chamada
&lt;samp&gt;. Se você tem um bloco grande de saída de processamento, você também
deve dar uma olhada no arquivo CSS, caso exista uma classe especial para isto.
</p>

<toc-add-entry name="kbd">Entrada de teclado</toc-add-entry>
<p>Se existir exemplos em que o(a) usuário(a) tenha que digitar alguma coisa no
teclado, a tag &lt;kbd&gt; é usada para a entrada de usuário(a). Veja também o
capítulo sobre <a href="#var">variáveis</a> para saber como usar tags nas
entradas de variável.
</p>

<toc-add-entry name="var">Variáveis</toc-add-entry>
<p>
Algumas vezes é preciso enfatizar uma entrada de variável, como um endereço IP
especial ou o nome de usuário(a) que deve ser dado numa chamada de programa da
linha de comando. Para essas entradas de variável, a tag &lt;var&gt; é usada.
</p>

<toc-add-entry name="pre">Conteúdo pré-formatado</toc-add-entry>
<p>
A tag &lt;pre&gt; é usada somente para texto pré-formatado. Tamanho de linha,
espaços e outras coisas serão preservados. Naturalmente, esta tag não pode
conter a maioria das outras tags HTML.
</p>

<toc-add-entry name="img">Imagens</toc-add-entry>
<p>
Se há imagens adicionadas na página, não é preciso adicionar border=0 como um
atributo inválido. Mas, se possível, o tamanho da imagem e o atributo
<code>alt</code> deve ser adicionado. O tamanho é adicionado pelo wml se não
estiver presente, mas isso demanda tempo de compilação. O atributo
<code>alt</code> deve conter alguma coisa que diga o quê está na imagem para
usuários(as) que navegam via lynx e pessoas com deficiência visual.
</p>

<toc-add-entry name="links">Links</toc-add-entry>
<p>
Se você pretende criar um link para uma página dentro de
<code>https://www.debian.org</code> na documentação, por favor use variáveis
definidas no arquivo <code>.wmlrc</code> para cada idioma. Por exemplo, a URL na
tag <br />
<code>&lt;a href="https://www.debian.org/devel/website/htmlediting"&gt;htmlediting&lt;/a&gt;</code><br />
deve ser substituída usando a variável <tt>DEVEL</tt> desse jeito
<code>&lt;a href="&#36;(DEVEL)/website/htmlediting"&gt;htmlediting&lt;/a&gt;</code>.
</p>

# <toc-add-entry name=""></toc-add-entry>

