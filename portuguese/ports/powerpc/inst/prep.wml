#use wml::debian::template title="Porte para PowerPC (PReP)" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/inst/menu.inc"
#use wml::debian::translation-check translation="baca2cc9eb45158bf723feec7aa48e19ee745253"

<h1>Página do Debian GNU/Linux PowerPC PReP</h1>

<h3>Introdução</h3>
<p>
 Tanto IBM quanto Motorola fazem ou fizeram máquinas de arquitetura
 PReP. Até o momento, o suporte a PReP só foi testado em sistemas
 Motorola legacy PowerStack(tm) (placas-mãe Blackhawk, Comet, Utah)
 e na atual solução embarcada na família PowerPlus(tm). Estes incluem
 MTX, MTX+, MVME2300(sc)/2400/2600/2700/3600/4600 e MCP(n)750. A IBM
 produz uma variedade de estações de trabalho desktop que são compatíveis
 com PowerPC PReP. Estes incluem RS/6000 40P, 43P, 830, 850, 860, 6015 e 6030.

</p>

<h3>Problemas conhecidos</h3>
<p>
MTX+ (sistema 7 PCI slot MTX) não pode fazer boot do kernel 2.2, ele trava
na inicialização do driver IDE. Isto é um problema conhecido e será corrigido
em um futuro patch do kernel.
</p>
<p>
Todos os sistemas Motorola PowerPlus têm suporte de IDE quebrado no kernel.
Este problema foi identificado e corrigido. Alterações logo estarão
disponíveis como um pacote Debian de patch de kernel e serão introduzidas
no código-fonte do kernel.
</p>

<h3>Firmware</h3>
<p>

Dependendo da idade e/ou do fabricante do seu sistema PReP, você terá
um firmware PPCBUG (Motorola), Open Firmware (IBM ou Motorola) ou um PReP da
IBM. Os comandos de boot são um pouco diferentes, mas todos esses sistemas
implementam o padrão PReP, então o mesmo kernel pode ser inicializado em
todas as plataformas. Uma explicação completa sobre comandos de firmware
está fora do escopo deste documento, mas alguma informação será fornecida
onde necessária.

</p>

<h3>Configuração para instalação</h3>

<h4>Instalação baseada em disquete</h4>

<p>
Grave <code>boot1440.bin</code>, <code>root1440.bin</code>,
<code>resc1440.bin</code>, <code>drv14-*.bin</code> e as imagens
para os disquetes. O comando <code>dd</code> pode ser usado em
um sistema Linux/Unix ou o <code>rawrite</code> pode ser usado em
um sistema DOS/Windows.
</p>

<h4>Instalação baseada em rede</H4>
<p>

Coloque os arquivos <code>resc1440.bin</code> e <code>drivers.tgz</code>
em um sistema de arquivos NFS exportado em seu servidor NFS. É melhor
colocar os arquivos da seguinte forma:
<code>/[ponto-de-montagem]/debian/[arquivos-de-instalação]</code>
</p>

<p>
Coloque o arquivo <code>boot.bin</code> no diretório tftp em seu servidor TFTP.
</p>

<h4>Opções de console</h4>
<p>
Em sistemas Motorola PPCBUG, você pode fazer boot e instalar usando
 ou o console VGA, ou o console serial. PPCBUG e Linux retornarão ao modo
 de console serial se nenhum VGA ou teclado é detectado.
</p>

<p>
De modo a ver adequadamente a instalação em um console serial, algumas
capacidades de emulação de terminal são necessárias. De modo a ver a
instalação em cores, é necessário ter um emulador de terminal com
suporte à cor ANSI. O xterm colorido padrão incluído na maioria
das distribuições Linux é suficiente, como também o são a maior parte
dos programas de terminal no Windows (por exemplo, <a
href="http://www.chiark.greenend.org.uk/~sgtatham/putty/">PuTTY</a>).
De modo a ver os caracteres de desenho de linha
que fazem as caixas da GUI, é essencial que seja usada uma fonte contendo
caracteres IBM de linha estendida de arte. Sob o Linux, a fonte "linux8x8" pode
ser usada e, sob o Windows, pode-se selecionar a fonte MSLineDraw.
</p>


<h3>Inicializando</h3>
<dl>
<dt> PPCBUG
<dd> Use o comando "ENV" e ligue a opção "Network PReP boot enable".
<dd> Use o comando "NIOT" para configurar os enredeços IP do cliente, servidor
     gateway opcional que será usado para fazer tftp do kernel. Se é uma placa
     recém-adquirida, certifique-se de definir <tt>SET <var>&lt;data/hora&gt;</var></tt>
     para iniciar o hwclock, ou a rede e outras coisas não funcionarão.
<dt> Faça o boot da imagem com
<dd> PPCBUG&gt;NBO 0 0 ,,, bootprep.bin
<dt> OpenFirmware
<dt> Use o seguinte comando para fazer o tftp realizar o boot da imagem PReP:
<dd> &gt;boot &lsaquo;server_ipaddr&rsaquo;,&lsaquo;file&rsaquo;,&lsaquo;client_ipaddr&rsaquo;
</dl>



<h2>Instalação do Debian</h2>
<ol>
<li> Configure o teclado
<p>
 Se no console serial, a etapa de configurar o teclado é omitida.
</p>

<li> Particionando o disco rígido
<p>
Crie uma partição primária de 2MB e mude seu tipo para PPC PReP boot
(type 41). Máquinas OF PReP podem ter problemas de inicialização se a partição
do boot PPC PReP está em outro lugar que não a partição primária 1.
Embora todos os sistemas Motorola PPCBUG possam ter um boot PPC PReP
localizado em qualquer partição primária, é uma prática generalizada
colocar a partição de boot PPC PReP em sda1 ou hda1.
</p>
<p>
Após a partição de boot PPC PReP ter sido criada, siga as convenções
normais do Linux para particionamento. Ao menos uma partição root e uma
swap devem ser criadas e elas podem estar tanto nas partições primárias
como nas lógicas.
</p>

<li> Instalar discos de recuperação e drivers
<p>
A instalação de disquetes é direta e simples, insira os discos de
recuperação e drivers na medida em que forem demandados.
</p>
<p>
Para uma instalação baseada em rede, escolha a opção NFS e você será
demandado(a) a configurar a rede. Quando pedir pelo servidor NFS a
ser utilizado, entre com o servidor NFS e diretório que você configurou
antes. Escolha as opções padrões para o resto das questões.
</p>

<li> Configurar os módulos de driver
<p>
 Por agora, os módulos importantes são construídos no kernel, então somente
 escolha "Exit".
</p>

<li> Instalar o sistema de base
<p>
Se uma instalação por disquete está selecionada, simplesmente insira os
disquetes de base como requisitados. Se uma instalação NFS está selecionada,
entre com o servidor NFS e o diretório nos quais o sistema de base está
localizado, e escolha as opções padrão para instalar o sistema de base
do Debian.
</p>

<li> Faça o sistema inicializável a partir do disco rígido
<p>
Faça o sistema inicializável da partição de boot PPC PReP que foi
criada anteriormente. Se essa partição estiver faltando, um erro será
exibido.
</p>

<li> Faça um disquete de boot
<p>
Grava uma imagem inicializável em um disquete.
</p>

</ol>

<h2>O boot do sistema de base do Debian</h2>
<p>
 Se você configurou uma partição de boot type 41 PReP e fez com que o
 programa de instalação deixasse o sistema inicializável do disco rígido,
 então você simplesmente pode enviar um comando de boot de firmware para
 fazê-lo inicializar do disco rígido (tanto o PPCBUG quanto o OF têm
 opções de autoboot que talvez você queira ligar).
</p>
<p>
Nota: para forçar o kernel a fazer root da partição correta, pode ser
necessário registrar a variável de ambiente global PReP, "bootargs".
No caso de uma instalação Motorola PReP, que por padrão fará o root de
sda1 e onde o sistema de arquivos do root está atualmente em sda2, deve-se
definir
<tt>bootargs=/dev/sda2</tt> .
</p>

<p>PPCBUG</p>

<dl>
<dt>Defina o parâmetro bootargs
<dd>PPCBUG&gt;GEVEDIT bootargs
<dd>PPCBUG&gt;bootargs=root=/dev/sda2
<dt>Faça um boot (assume disco SCSI no controller 0, SCSI ID 0):
<dd>PPCBUG&gt;PBOOT 0
<dt>Faça um boot (assume disco SCSI no controller 0, SCSI ID x):
<dd>PPCBUG&gt;PBOOT 0 x0
</dl>
<dl>
<dt>Openfirmware
<dt>Faça um boot (para um disco IDE/SCSI instalado com alias para disk0)
<dd>&gt;boot disk0
<dt>Faça um boot (para um disco IDE/SCSI instalado com alias para hdisk0)
<dd>&gt;boot hdisk0
</dl>
<p>
 Agora o kernel incializa a partir do disco rígido.
</p>

<hr>
Por favor, mande um e-mail com qualquer sugestão/reclamação/problema com a
instalação em PReP e com esta documentação para
<a href="mailto:porter@debian.org">Matt Porter</a>.
