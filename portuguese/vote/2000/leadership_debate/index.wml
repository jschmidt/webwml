#use wml::debian::template title="Debate para liderança do Debian"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::votebar
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h3>Data</h3>

<p>O debate foi realizado na terça-feira, 15 de fevereiro de 2000 às 19h
UTC.</p>

<h3>Onde</h3>

<p><code>irc.debian.org</code> no canal <code>#debian-debate</code></p>

<h3>Formato</h3>

<p>24 horas antes do debate, cada um dos candidatos enviou seu <q>discurso de
abertura</q> ao organizador do debate, Jason Gunthorpe. Eles foram então
colocados nesta página. Tudo foi adicionado ao mesmo tempo para garantir a
equidade.</p>

<p>O debate real tem duas partes. Primeiro, um debate tradicional fortemente
moderado:</p>
<ol>
<li>O moderador faz uma pergunta ao candidato. O candidato
    então tem um tempo razoável para responder.</li>
<li>Após a resposta, cada um dos outros candidatos responde em sequência.</li>
<li>O primeiro candidato faz as considerações finais sobre a questão.</li>
<li>A ordem dos candidatos é alternada para cada pergunta.</li>
</ol>

<p>A segunda parte do debate é mais estilo livre. São feitas as perguntas
enviadas pelo público e desenvolvedores(as). Cada candidato tem um tempo curto
para responder.</p>

<h3>Posicionamentos dos candidatos</h3>

<p>Os candidatos estão listados na ordem das nomeações. Na maioria dos casos
não há uma divisão clara entre o anúncio e a declaração da plataforma,
portanto eles estão simplesmente apresentados como uma lista numerada. </p>

<dl>
<dt>Ben Collins</dt>
<dd>Anúncio e plataforma:
   <a href="https://lists.debian.org/debian-vote-0001/msg00000.html">1</a>
   <a href="https://lists.debian.org/debian-vote-0001/msg00004.html">2</a></dd>
<dd><a href="ben-speech.html">Discurso de abertura para o debate</a></dd>
<dt>Wichert Akkerman</dt>
<dd>Anúncio e plataforma:
   <a href="https://lists.debian.org/debian-vote-0001/msg00001.html">1</a></dd>
<dd><a href="wichert-speech.html">Discurso de abertura para o debate</a></dd>
<dt>Joel Klecker</dt>
<dd>Anúncio e plataforma:
   <a href="https://lists.debian.org/debian-vote-0001/msg00009.html">1</a></dd>
<dd><a href="joel-speech.html">Discurso de abertura para o debate</a></dd>
<dt>Matthew Vernon</dt>
<dd>Anúncio e plataforma:
   <a href="https://lists.debian.org/debian-vote-0001/msg00003.html">1</a>
   <a href="https://lists.debian.org/debian-vote-0001/msg00008.html">2</a></dd>
<dd><a href="mat-speech.html">Discurso de abertura para o debate</a></dd>
</dl>

<h3>Log do debate entre os candidatos</h3>

<p>A <a href="transcript.txt">transcrição bruta</a> e a
<a href="transcript.html">versão formatada</a> estão disponíveis. E também
os <a href="topics.html">tópicos</a> brutos e as <a href="informal.txt">questões
informais</a> para o debate. Nem todos os tópicos tiveram tempo de serem
discutidos.</p>
