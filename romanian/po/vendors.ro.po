msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:00+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Comerciant"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Permite contribuții"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arhitecturi"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Livrări internaționale"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Contact"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Site-ul Comerciantului"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "pagină"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "e-mail"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "în Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "În unele zone"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "sursă"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "și"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Permite contribu&#355;ii la Debian:"

#~ msgid "Architectures:"
#~ msgstr "Arhitecturi:"

#~ msgid "CD Type:"
#~ msgstr "Tip de CD:"

#~ msgid "Country:"
#~ msgstr "&#354;ar&#259;:"

#~ msgid "Custom Release"
#~ msgstr "Versiune special&#259; pentru client"

#~ msgid "DVD Type:"
#~ msgstr "Tip de DVD"

#~ msgid "Development Snapshot"
#~ msgstr "Versiunea &#238;n dezvoltare"

#~ msgid "Multiple Distribution"
#~ msgstr "Distribu&#355;ie multipl&#259;"

#~ msgid "Official CD"
#~ msgstr "CD oficial"

#, fuzzy
#~ msgid "Official DVD"
#~ msgstr "CD oficial"

#~ msgid "Ship International:"
#~ msgstr "Livreaz&#259; interna&#355;ional:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL-ul pentru pagina Debian:"

#~ msgid "Vendor Release"
#~ msgstr "Versiune specific&#259; distribuitorului"

#~ msgid "Vendor:"
#~ msgstr "Distribuitor:"

#~ msgid "contrib included"
#~ msgstr "contribu&#355;ii incluse"

#~ msgid "email:"
#~ msgstr "e-mail:"

#~ msgid "non-US included"
#~ msgstr "non-US inclus"

#~ msgid "non-free included"
#~ msgstr "non-free inclus"

#~ msgid "reseller"
#~ msgstr "re-v&#226;nz&#259;tor"

#~ msgid "reseller of $var"
#~ msgstr "re-v&#226;nz&#259;tor de $var"

#~ msgid "updated monthly"
#~ msgstr "actualizat&#259; lunar"

#~ msgid "updated twice weekly"
#~ msgstr "actualizat&#259; de dou&#259; ori pe s&#259;pt&#259;m&#226;n&#259;"

#~ msgid "updated weekly"
#~ msgstr "actualizat&#259; s&#259;pt&#259;m&#226;nal"

#~ msgid "vendor additions"
#~ msgstr "adaosurile distribuitorului"
