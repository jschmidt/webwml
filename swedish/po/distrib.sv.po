# translation of distrib.po to Swedish
# Martin Ågren <martin.agren@gmail.com>, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: distrib\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-09-20 18:39+0200\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Nyckelord"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Visa"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "sökvägar som slutar med nyckelordet"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "paket som innehåller filer med namnet"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "paket som innehåller filer med nyckelordet i namnet"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Sök i"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "experimental"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "unstable"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "testing"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "stable"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "gammal stable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Arkitektur"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:40
msgid "any"
msgstr "alla"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:44
msgid "Search"
msgstr "Sök"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:45
msgid "Reset"
msgstr "Nollställ"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Sök på"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Endast paketnamn"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Beskrivningar"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Namn på källkodspaket"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Endast exakta träffar"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Avdelning"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/distrib/search_packages-form.inc:39
msgid "non-free-firmware"
msgstr "non-free-firmware"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64-bitars PC (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "64-bitars PC (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "Hard Float ABI ARM (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd 32-bitars PC (i386)"

#: ../../english/releases/arches.data:16
msgid "Hurd 64-bit PC (amd64)"
msgstr "Hurd 64-bitars PC (amd64)"

#: ../../english/releases/arches.data:17
msgid "32-bit PC (i386)"
msgstr "32-bitars PC (i386)"

#: ../../english/releases/arches.data:18
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD 32-bitars PC (i386)"

#: ../../english/releases/arches.data:20
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD 64-bitars PC (amd64)"

#: ../../english/releases/arches.data:21
msgid "64-bit LoongArch"
msgstr "64-bitars LoongArch"

#: ../../english/releases/arches.data:22
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:23
msgid "MIPS (big endian)"
msgstr "MIPS (stor endian)"

#: ../../english/releases/arches.data:24
msgid "64-bit MIPS (little endian)"
msgstr "64-bit MIPS (liten endian)"

#: ../../english/releases/arches.data:25
msgid "MIPS (little endian)"
msgstr "MIPS (liten endian)"

#: ../../english/releases/arches.data:26
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:27
msgid "POWER Processors"
msgstr "POWER-processorer"

#: ../../english/releases/arches.data:28
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "RISC-V 64-bitars liten endian (riscv64)"

#: ../../english/releases/arches.data:29
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:30
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:31
msgid "SPARC"
msgstr "SPARC"

#~ msgid "AMD64"
#~ msgstr "AMD64"

#~ msgid "HP PA/RISC"
#~ msgstr "HP PA/RISC"

#~ msgid "Hurd (i386)"
#~ msgstr "Hurd (i386)"

#~ msgid "Intel IA-64"
#~ msgstr "Intel IA-64"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"

#~ msgid "kFreeBSD (AMD64)"
#~ msgstr "kFreeBSD (AMD64)"

#~ msgid "kFreeBSD (Intel x86)"
#~ msgstr "kFreeBSD (Intel x86)"
